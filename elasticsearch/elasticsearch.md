## 普通aip

### **创建索引**

```html
PUT /liuli2
{
  "mappings": {
    "properties": {
      "name":{
        "type": "text"
      },
      "age": {
        "type": "long"
      },
      "birthday": {
        "type": "date"
      }
    }
  }
}
```

### **查看索引**

```html
GET liuli2
```

### **增删改查**

#### 创建数据PUT

```html
// 创建第一条数据
PUT /liuli/_doc/1
{
  "name": "琉璃呀",
  "age": "18",
  "desc": "一看操作猛如虎，一看工资二千五",
  "tag": ["高大","宅","帅"]
}

```

#### 更新数据

```html
// PUT 更新，本质就是重新赋值，所以可能会丢失原来的所有数值，如下面更新第一条数据后age、desc、tag会全部丢失
PUT /liuli/_doc/1
{
  "name": "琉璃"
}

// POST 更新数据
POST /liuli/_doc/1/_update
{
  "doc": {
    "name": "琉璃哇"
  }
}
```

#### 删除数据

```html
// 删除索引
DELETE liuli
// 删除数据，删除索引liuli的第一条数据
DELETE /liuli/_doc/1
```

#### 普通查询数据

```html
GET /liuli/_doc/1
```

#### 条件查询

```html
GET /liuli/_doc/_search?q=name:琉璃
```

#### 构建查询

```html
GET /liuli/_doc/_search
{
  "query": {
    "match": {
      "name": "琉璃"
    }
  }
}
```

#### 查询全部

```html
GET /liuli/_doc/_search
{
  "query": {
    "match_all": {}
  }
}
```

#### 仅查看部分属性

```html
GET /liuli/_doc/_search
{
  "query": {
    "match_all": {}
  },
  "_source": ["name", "age"]
}
```

#### 查询排序（只能对数字、日期、ID排序）

```html
GET /liuliy/_doc/_search
{
  "query": {
    "match_all": {}
  },
  "sort": [
    {
      "age": {
        "order": "desc"
      }
    }
  ]
}
```

#### 分页查询

```html
GET /liuliy/_doc/_search
{
  "query": {
    "match_all": {}
  },
  "sort": [
    {
      "age": {
        "order": "desc"
      }
    }
  ],
  "from": 0,	// 从第几页开始
  "size": 1		// 返回几条数据
}
```

#### 布尔查询

##### must

```html
GET /liuliy/_doc/_search
{
  "query": {
    "bool": {
      "must": [
        {
          "match": {
            "name": "琉璃"
          }
        },
        {
          "match": {
            "age": 3
          }
        }
      ]
    }
  }
}
```

##### should

```
GET /liuliy/_doc/_search
{
  "query": {
    "bool": {
      "should": [
        {
          "match": {
            "name": "琉璃哇"
          }
        },
        {
          "match": {
            "age": 3
          }
        }
      ]
    }
  }
}
```

##### must_not

```
GET /liuliy/_doc/_search
{
  "query": {
    "bool": {
      "must_not": [
        {
          "match": {
            "age": 3
          }
        }
      ]
    }
  }
}
```

##### Fitter

```
GET /liuliy/_doc/_search
{
  "query":{
    "bool": {
      "must": [
        {
          "match": {
            "name": "琉璃"
          }
        }
      ],
      "filter": {
        "range": {
          "age": {
            "gt": 10
          }
        }
      }
    }
  }
}
```

#### 短语检索

```
GET /liuliy/_doc/_search
{
  "query": {
    "match": {
      "tag": "高 帅"
    }
  }
}
```

#### term查询精确查询

term和match的区别:
match是经过分析(analyer)的，也就是说，文档是先被分析器处理了，根据不同的分析器，分析出
的结果也会不同，在会根据分词 结果进行匹配。
term是不经过分词的，直接去倒排索引查找精确的值。

```
GET testdb/_doc/_search
{
  "query": {
    "terms": {
      "t1": ["22", "33"]
    }
  } 
}
```

#### 高亮显示

```
GET /liuliy/_search
{
  "query": {
    "match": {
      "name": "琉璃"
    }
  },
  "highlight": {
    "fields": {
      "name": {}
    }
  }
}
```

```
自定义高连前后缀
GET /liuliy/_search
{
  "query": {
    "match": {
      "name": "琉璃"
    }
  },
  "highlight": {
    "pre_tags": "<b class='key' style='color:red'>",
    "post_tags": "</b>",
    "fields": {
      "name": {}
    }
  }
}
```

## java api

### 索引相关

#### 创建索引

```java
// 测试创建索引
@Test
void contextLoads() throws IOException {
    CreateIndexRequest request = new CreateIndexRequest("hang_index");
    CreateIndexResponse createIndexResponse = client.indices().create(request, RequestOptions.DEFAULT);
    System.out.println(createIndexResponse);
}
```

#### 删除索引

```java
// 测试删除索引
@Test
void deleteIndex() throws IOException {
    DeleteIndexRequest request = new DeleteIndexRequest("hang_index");
    AcknowledgedResponse response = client.indices().delete(request, RequestOptions.DEFAULT);
    System.out.println(response.isAcknowledged());
}
```

#### 判断索引是否存在

```java
// 判断索引是否存在
@Test
public void existsIndex() throws IOException {
    GetIndexRequest request = new GetIndexRequest("hang_index");
    boolean exists = client.indices().exists(request, RequestOptions.DEFAULT);
    System.out.println(exists);
}
```

#### 判断某索引下文档id是否存在

```java
// 判断某索引下文档id是否存在
@Test
void docExists() throws IOException {
    GetRequest getRequest = new GetRequest("liuli","1");
    //只判断索引是否存在不需要获取_source
    getRequest.fetchSourceContext(new FetchSourceContext(false));
    getRequest.storedFields("_none_");
    boolean exists = client.exists(getRequest, RequestOptions.DEFAULT);
    System.out.println(exists);
}
```

### 文档相关

#### 添加文档记录

```java
// 添加文档记录
@Test
void addDoc() throws IOException {
    User user = new User();
    user.setName("张三");
    user.setAge(18);
    IndexRequest request = new IndexRequest("hang_index");
    request.timeout("1s");
    request.source(JSON.toJSONString(user), XContentType.JSON);
    IndexResponse indexResponse = client.index(request, RequestOptions.DEFAULT);
    RestStatus status = indexResponse.status();
    System.out.println(status);
}
```

#### 根据id来获取记录

```java
// 根据id来获取记录
@Test
void getDoc() throws IOException {
    GetRequest request = new GetRequest("hang_index", "2AH3OXsBuKS5MmAb-e6H");
    GetResponse response = client.get(request, RequestOptions.DEFAULT);
    System.out.println(response);
}
```

#### 批量添加文档记录

```java
// 批量添加文档记录
@Test
void bulkAdd() throws IOException {
    ArrayList<User> userList = new ArrayList<>();
    userList.add(new User("张三1", 18));
    userList.add(new User("张三2", 18));
    userList.add(new User("张三3", 18));
    userList.add(new User("李四1", 28));
    userList.add(new User("李四2", 28));
    userList.add(new User("李四3", 28));


    BulkRequest bulkRequest = new BulkRequest();
    bulkRequest.timeout("10s");
    for (int i = 0; i < userList.size(); i++) {
        bulkRequest.add(new IndexRequest("hang_index")
                        .id("" + (i+1))
                        .source(JSON.toJSONString(userList.get(i)), XContentType.JSON));
    }
    BulkResponse bulk = client.bulk(bulkRequest, RequestOptions.DEFAULT);
    System.out.println(bulk.hasFailures());
}
```

#### 更新文档记录

```java
// 更新文档记录
@Test
void updateDoc() throws IOException {
    UpdateRequest request = new UpdateRequest("hang_index", "1");
    request.doc(JSON.toJSONString(new User("王五", 111)), XContentType.JSON);
    request.timeout(TimeValue.timeValueSeconds(1));
    request.timeout("1s");
    UpdateResponse update = client.update(request, RequestOptions.DEFAULT);
    System.out.println(update.status());
}
```

#### 删除文档记录

```java
// 删除文档记录
@Test
void deleteDoc() throws IOException {
    DeleteRequest request = new DeleteRequest("hang_index", "1");
    DeleteResponse deleteResponse = client.delete(request, RequestOptions.DEFAULT);
    System.out.println(deleteResponse.status());
}
```

#### 查询

```java
// 根据某字段来搜索
/**
     * 使用QueryBuilder
     * termQuery("key", obj) 完全匹配
     * termsQuery("key", obj1, obj2..)   一次匹配多个值
     * matchQuery("key", Obj) 单个匹配, field不支持通配符, 前缀具高级特性
     * multiMatchQuery("text", "field1", "field2"..);  匹配多个字段, field有通配符忒行
     * matchAllQuery();         匹配所有文件
     */
@Test
void search() throws IOException {
    SearchRequest request = new SearchRequest("hang_index");
    // 构造搜索条件
    SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
    // 查询条件，我们可以使用QueryBuilders 工具实现
    // QueryBuilders.termQuery  精确
    // QueryBuilders.matchAllQuery()    匹配所有
    TermQueryBuilder queryBuilder = QueryBuilders.termQuery("age", "18");
    searchSourceBuilder.query(queryBuilder);
    searchSourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
    request.source(searchSourceBuilder);
    SearchResponse searchResponse = client.search(request, RequestOptions.DEFAULT);
    System.out.println(JSON.toJSONString(searchResponse.getHits()));
    System.out.println("=============================================");
    for (SearchHit hit : searchResponse.getHits().getHits()) {
        System.out.println(hit.getSourceAsMap());
    }
}
```