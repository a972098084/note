docker搭建es集群

前提准备

docker环境

虚拟机至少两台

本文采用192.168.88.100、192.168.88.103，虚拟机需要提前开启9200和9300端口或者关闭防火墙

开启端口命令为systemctl-cmd --add-port=xxx

关闭防火墙命令为systemctl stop firewalld

一、创建挂载目录与配置文件

192.168.88.100节点进行如下操作

mkdir -p /es/config/

vi elasticsearch.yaml

```yaml
cluster.name: es
node.name: node-1
node.master: true
node.data: true
network.host: 0.0.0.0
http.port: 9200
transport.port: 9300
network.publish_host: 192.168.88.100
discovery.seed_hosts: ["192.168.88.100:9300", "192.168.88.103:9300"]
cluster.initial_master_nodes: ["node-1"]
```

192.168.88.103进行如下操作

mkdir -p /es/config/

vi elasticsearch.yaml

```yaml
cluster.name: es
node.name: node-2
node.master: false
node.data: true
network.host: 0.0.0.0
http.port: 9200
transport.port: 9300
network.publish_host: 192.168.88.103
discovery.seed_hosts: ["192.168.88.100:9300", "192.168.88.103:9300"]

```

二、拉取镜像

docker pull elasticsearch:7.16.3

三、启动

docker run -e ES_JAVA_OPTS="-Xms256m -Xmx256m" -d -p 9200:9200 -p 9300:9300 -v /es/config/elasticsearch.yaml:/usr/share/elasticsearch/config/elasticsearch.yml  --name ES docker镜像id



问题

发生如下报错bootstrap check failure [2] of [2]: max virtual memory areas vm.max_map_count [65530] is too low, increase to at least [262144]

解决方案

```
[root@chenxi elasticsearch]# vim /etc/sysctl.conf
在文件末尾追加：vm.max_map_count=655360
保存后执行
[root@chenxi elasticsearch]# sysctl -p
```

http://service-4b9cwk5j-1251130579.sh.apigw.tencentcs.com/api/v1/client/subscribe?token=8e7f3808bb15ccab73808ff885c2fdf4

http://service-4b9cwk5j-1251130579.sh.apigw.tencentcs.com/api/v1/client/subscribe?token=652e053d71d2808222b96869f0b5056c

