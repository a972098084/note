Linux上搭建es集群

在单机版搭建的基础上继续修改文件vi ../config/elasticsearch.yml

```
cluster.name: es
node.name: node-1
node.master: false
node.data: true
network.host: 0.0.0.0
discovery.seed_hosts: ["192.168.88.100:9300", "192.168.88.103:9300"]
cluster.initial_master_nodes: ["node-1"]
```

除node.name应该不重名，master是否可以当主节点，data是否为数据节点

运行时会有如下错误

ERROR: [2] bootstrap checks failed. You must address the points described in the following [2] lines before starting Elasticsearch.
bootstrap check failure [1] of [2]: max file descriptors [4096] for elasticsearch process is too low, increase to at least [65535]
bootstrap check failure [2] of [2]: max virtual memory areas vm.max_map_count [65530] is too low, increase to at least [262144]

![image-20220118164150482](https://edu-hanghang.oss-cn-beijing.aliyuncs.com/elasticsearch/2022/image-20220118164150482.png)

第一个报错解决方案

```
[root@chenxi elasticsearch]# vim /etc/security/limits.conf

## 65535修改为65536

* soft nofile 65536
* hard nofile 65536

## 文件末尾追加

* soft nproc 4096
* hard nproc 4096
```

第二个报错解决方案

```
[root@chenxi elasticsearch]# vim /etc/sysctl.conf
在文件末尾追加：vm.max_map_count=655360
保存后执行
[root@chenxi elasticsearch]# sysctl -p
```



![image-20220118170546680](https://edu-hanghang.oss-cn-beijing.aliyuncs.com/elasticsearch/2022/image-20220118170546680.png)

出现这样的报错原因：从节点无法加入主节点

解决办法：删除data文件夹