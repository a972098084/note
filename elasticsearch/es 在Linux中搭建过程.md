es 在Linux中搭建集群过程以及遇到的问题

一、官网下载安装包

<img src="https://edu-hanghang.oss-cn-beijing.aliyuncs.com/elasticsearch/2022/image-20220118161147701.png" alt="image-20220118161147701" style="zoom: 33%;" />

如遇下载失败或速度过慢，附带网盘地址

链接: https://pan.baidu.com/s/1Agu59AHNj8sLMSDCO-zTOw 

提取码: v4zj 

二、上传到服务器后进行解压

<img src="https://edu-hanghang.oss-cn-beijing.aliyuncs.com/elasticsearch/2022/image-20220118161630412.png" alt="image-20220118161630412" style="zoom:50%;" />

通过tar -zxvf elasticsearch-7.16.3-linux-x86_64.tar.gz 进行解压

三、打开文件后修改config/jvm.options中启动内存大小

<img src="https://edu-hanghang.oss-cn-beijing.aliyuncs.com/elasticsearch/2022/image-20220118161841374.png" alt="image-20220118161841374" style="zoom:33%;" />

四、因为es不能使用root用户直接运行，需要创建用户

创建用户语句		adduser es

为es用户增加权限：  chown -R es elasticsearch-7.16.3  （需要在该目录下运行）

切换用户		su es

在/opt/elasticsearch-7.16.3/bin目录下执行./elasticsearch

五、使用curl 127.0.0.1:9200访问

<img src="https://edu-hanghang.oss-cn-beijing.aliyuncs.com/elasticsearch/2022/image-20220118162417208.png" alt="image-20220118162417208" style="zoom:50%;" />