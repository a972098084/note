## elasticsearch之Snapshot备份与恢复

本文主要内容为基于本地存储，同样你也可以采用nfs创建网络共享盘，原理一致；也可以通过插件完成在不同地方进行存储，如：

```
repository-s3 支持S3 仓库
repository-hdfs 支持HDFS 仓库
repository-azure 支持Azure 仓库
repository-gcs 支持 Google Cloud 仓库
```

### 简介

elasticsearch备份有两种形式，一种是将数据全部导出为文件形式，另一种为对elasticsearch 的data目录中的文件进行快照形式存储。如果数据量较小可以通过第一种方式，当数据量较为大时则第二种方式更为试用。本文着重讲解第二种备份方式，即基于Snapshot形式进行备份。

### Snapshot

首先我们需要明白Snapshot是增量备份的，每次Snapshot，es都会对index进行分析，只对增量部分进行备份。它具有以下特点：

- 增量备份，如index基本没发生变化，则几乎没有资源损耗
- 删除一个Snapshot不会对其他的Snapshot有任何影响

### 建仓

在创建快照之前，首先需要创建仓库，一个仓库中可以存在多个快照，配置仓库需要在`elasticsearch.yam`中配置仓库地址，如在docker或k8s中，则可以通过环境变量形式添加如下配置后，重启即可。

```
在elasticsearch.yml 中添加：
path.repo: ["/usr/share/elasticsearch/data"]


# 可以填多个仓库地址，后面添加快照时保存地址必须在下面配置地址之中
path.repo: ["/usr/share/elasticsearch/data1", "/usr/share/elasticsearch/data2"]
```

#### 创建仓库

```js
# 创建一个名为snapshot20220322的仓库
POST /_snapshot/snapshot20220322
{
  "type": "fs",
  "settings": {
    "location": "/usr/share/elasticsearch/data"
  }
}
```

创建仓库时可以设置如下参数：

```
location：仓库地址
compress：是否压缩，默认true,
chunk_size：是否将大文件分解成块，分解成块的单位，例子: 1GB, 10MB, 5KB, 500B，默认：null(不限制)
max_restore_bytes_per_sec：快照恢复的速度，默认：无限制
max_snapshot_bytes_per_sec：节点出入站的速率，默认：40mb/s
readonly：仓库快照是否时只读，默认：false
```

#### 查看仓库

```js
GET /_snapshot/_all
```

#### 删除仓库

```js
DELETE /_snapshot/snapshot20220322
```

#### 检测仓库是否可用

```js
POST /_snapshot/snapshot20220322/_verify
```

### 备份

创建好仓库后就可以开始备份，Snapshot是增量的，在创建Snapshot的时候，elasticsearch会分析已经存在的Snapshot，只对上次备份后创建或修改的文件进行备份，那些没有更改的文件会直接引用到上次的Snapshot中，因此频繁的备份，Snapshot文件会越来越小。特有以下特性：

- Snapshot是某个时间点的集群状态，在备份过程中不会对ES做出的任何更改都不会出现在Snapshot中
- 创建Snapshot的这个过程中，集群中的全部工作依旧正常，但是同一时间只会有一个Snapshot创建

#### 拍摄快照

```js
保存集群下的所有索引快照
# 格式：/_snapshot/仓库名称/快照名称
PUT /_snapshot/snapshot20220322/snapshot_1
这个指令返回关于snapshot的基本信息，包括开始和结束时间、创建快照的elasticsearch版本、包含的索引列表、快照的当前状态和快照期间发生的故障列表。

保存集群下的指定索引快照
指定索引，添加wait_for_completion参数，等于true时，将等待快照保存结束时才会返回结果。反之异步返回结果。添加wait_for_completion参数等于true时，表示将等待快照结束时才会返回结果。
PUT /_snapshot/snapshot20220322/snapshot_1?wait_for_completion=true
{
    "indices": "kibana_sample_data_flights"
}
快照是增量的，有可能很多快照依赖于过去的段，在仓库下的文件要注意了不要手动删除
```

#### 创建日期表达式的快照

```js
# 创建格式为：snapshot-2020-09-28 转为表达式：<snapshot-{now/d}>   转为URI: %3Csnapshot-%7Bnow%2Fd%7D%3E
# 创建以当前日期保存所有索引的快照
PUT /_snapshot/snapshot20220322/%3Csnapshot-%7Bnow%2Fd%7D%3E
```



#### 获取所有快照信息

```js
# 格式：/_snapshot/仓库名称/_all
GET /_snapshot/snapshot20220322/_all

响应：
{
  "snapshots" : [
    {
      "snapshot" : "snapshot_1",
      "uuid" : "2AHaddfOSMWrmEnoyHW3ng",
      "version_id" : 6080899,
      "version" : "6.8.8",
      "indices" : [
        "school"
      ],
      "include_global_state" : true,
      "state" : "SUCCESS",
      "start_time" : "2022-03-22T08:44:31.711Z",
      "start_time_in_millis" : 1647938671711,
      "end_time" : "2022-03-22T08:44:31.763Z",
      "end_time_in_millis" : 1647938671763,
      "duration_in_millis" : 52,
      "failures" : [ ],
      "shards" : {
        "total" : 5,
        "failed" : 0,
        "successful" : 5
      }
    }
  ]
}
```

#### 获取指定快照信息

```js
# 格式：/_snapshot/仓库名称/快照名称
GET /_snapshot/snapshot20220322/snapshot_1

响应：
{
  "snapshots" : [
    {
      "snapshot" : "snapshot_1",
      "uuid" : "VLTDp640SSOjAx5sVXZOMg",
      "version_id" : 6080899,
      "version" : "6.8.8",
      "indices" : [
        "school"
      ],
      "include_global_state" : true,
      "state" : "SUCCESS",
      "start_time" : "2022-03-22T08:52:26.973Z",
      "start_time_in_millis" : 1647939146973,
      "end_time" : "2022-03-22T08:52:27.012Z",
      "end_time_in_millis" : 1647939147012,
      "duration_in_millis" : 39,
      "failures" : [ ],
      "shards" : {
        "total" : 5,
        "failed" : 0,
        "successful" : 5
      }
    }
  ]
}
```

#### 获取指定快照状态

除了上边的`wait_for_completion`参数知道快照是否完成之外，可以使用如下：

```js
GET /_snapshot/snapshot20220322/snapshot_1/_status

响应：
{
  "snapshots" : [
    {
      "snapshot" : "snapshot_1",
      "repository" : "snapshot20220322",
      "uuid" : "VLTDp640SSOjAx5sVXZOMg",
      "state" : "SUCCESS",
      "include_global_state" : true,
      "shards_stats" : {
        "initializing" : 0,
        "started" : 0,
        "finalizing" : 0,
        "done" : 5,
        "failed" : 0,
        "total" : 5
      },
      "stats" : {
        "incremental" : {
          "file_count" : 8,
          "size_in_bytes" : 5198
        },
        "total" : {
          "file_count" : 8,
          "size_in_bytes" : 5198
        },
        "start_time_in_millis" : 1647939146985,
        "time_in_millis" : 21,
        "number_of_files" : 8,
        "processed_files" : 8,
        "total_size_in_bytes" : 5198,
        "processed_size_in_bytes" : 5198
      },
      "indices" : {
        "school" : {
          "shards_stats" : {
            "initializing" : 0,
            "started" : 0,
            "finalizing" : 0,
            "done" : 5,
            "failed" : 0,
            "total" : 5
          },
          "stats" : {
            "incremental" : {
              "file_count" : 8,
              "size_in_bytes" : 5198
            },
            "total" : {
              "file_count" : 8,
              "size_in_bytes" : 5198
            },
            "start_time_in_millis" : 1647939146985,
            "time_in_millis" : 21,
            "number_of_files" : 8,
            "processed_files" : 8,
            "total_size_in_bytes" : 5198,
            "processed_size_in_bytes" : 5198
          },
          "shards" : {
            "0" : {
              "stage" : "DONE",
              "stats" : {
                "incremental" : {
                  "file_count" : 1,
                  "size_in_bytes" : 261
                },
                "total" : {
                  "file_count" : 1,
                  "size_in_bytes" : 261
                },
                "start_time_in_millis" : 1647939147004,
                "time_in_millis" : 2,
                "number_of_files" : 1,
                "processed_files" : 1,
                "total_size_in_bytes" : 261,
                "processed_size_in_bytes" : 261
              }
            },
            "1" : {
              "stage" : "DONE",
              "stats" : {
                "incremental" : {
                  "file_count" : 1,
                  "size_in_bytes" : 261
                },
                "total" : {
                  "file_count" : 1,
                  "size_in_bytes" : 261
                },
                "start_time_in_millis" : 1647939146993,
                "time_in_millis" : 2,
                "number_of_files" : 1,
                "processed_files" : 1,
                "total_size_in_bytes" : 261,
                "processed_size_in_bytes" : 261
              }
            },
            "2" : {
              "stage" : "DONE",
              "stats" : {
                "incremental" : {
                  "file_count" : 4,
                  "size_in_bytes" : 4154
                },
                "total" : {
                  "file_count" : 4,
                  "size_in_bytes" : 4154
                },
                "start_time_in_millis" : 1647939146997,
                "time_in_millis" : 5,
                "number_of_files" : 4,
                "processed_files" : 4,
                "total_size_in_bytes" : 4154,
                "processed_size_in_bytes" : 4154
              }
            },
            "3" : {
              "stage" : "DONE",
              "stats" : {
                "incremental" : {
                  "file_count" : 1,
                  "size_in_bytes" : 261
                },
                "total" : {
                  "file_count" : 1,
                  "size_in_bytes" : 261
                },
                "start_time_in_millis" : 1647939146985,
                "time_in_millis" : 1,
                "number_of_files" : 1,
                "processed_files" : 1,
                "total_size_in_bytes" : 261,
                "processed_size_in_bytes" : 261
              }
            },
            "4" : {
              "stage" : "DONE",
              "stats" : {
                "incremental" : {
                  "file_count" : 1,
                  "size_in_bytes" : 261
                },
                "total" : {
                  "file_count" : 1,
                  "size_in_bytes" : 261
                },
                "start_time_in_millis" : 1647939146988,
                "time_in_millis" : 3,
                "number_of_files" : 1,
                "processed_files" : 1,
                "total_size_in_bytes" : 261,
                "processed_size_in_bytes" : 261
              }
            }
          }
        }
      }
    }
  ]
}
```

分片状态有

```
INITIALIZING：分片在检查集群状态看看自己是否可以被快照。这个一般是非常快的
STARTED：数据正在被传输到仓库
FINALIZING：数据传输完成；分片现在在发送快照元数据
DONE：快照完成
FAILED：快照处理的时候碰到了错误，这个分片/索引/快照不可能完成了。检查你的日志获取更多信息
```

#### 获取通配符快照信息

```js
获取模糊快照信息，多个用逗号隔开
GET /_snapshot/my_backup/snap*
GET /_snapshot/my_backup/snap*,test*
GET /_snapshot/my_backup/snapshot_2,snapshot_1
```

#### 删除快照

```js
DELETE /_snapshot/snapshot20220322/snapshot_1

使用以上指令删除snapshot的时候：
    *会将repository中所有和这个快照相关的文件都删除
    *会保留下来那些被其他还存在的快照使用的文件（因为快照是增量的）
```

### 恢复

一旦你备份过了数据，恢复它就简单了：只要在你希望恢复回集群的快照ID后面加上`_restore`即可：

```js
POST /_snapshot/snapshot20220322/snapshot_1/_restore
```

如上，就会恢复快照中的`snapshot20220322`索引与数据，如果集群中已有快照的索引那就会报索引已存在的错误。如果索引再快照之前有别名，那么恢复快照时，会一同恢复别名。

如果你想在不替换现有数据的前提下，恢复老数据来验证内容，或者做其他处理。可以从快照里恢复单个索引并提供一个替换的名称：

```js
POST /_snapshot/es_bak_20210722/snapshot_kibana_sample_data_flights/_restore
{
  "indices": "school",
  "ignore_unavailable": true,
  "include_global_state": false,              
  "rename_pattern": "kibana_sample_data_flights",
  "rename_replacement": "restored_kibana_sample_data_flights",
  "include_aliases": false
}

备注：school在恢复之前是有一个别名flights的，那么在恢复快照时，会排除别名恢复。
恢复后索引名：restored_kibana_sample_data_flights
```

恢复完成后，当前集群与快照同名的索引、模板会被覆盖。在集群中存在，但快照中不存在的索引、索引别名、模板不会被删除。因此恢复并非同步成与快照一致。

```js
ignore_unavailable:false，false表示当缺少kibana_sample_data_flights索引时报错
include_global_state:false 将还原快照中的所有数据流和索引，但不还原群集状态。
rename_pattern 与rename_replacement 支持正则表达式也可以像上面直接写死。
include_aliases 是否需要恢复别名，true恢复，false不恢复别名
partial:false可选，默认：false，如果快照包含一个或多个索引没有所有主碎片可用，则整个还原操作将失败。 如果为true，则允许恢复具有不可用碎片的索引的部分快照。将只恢复快照中成功包含的碎片。所有丢失的碎片将重新创建为空。

```

使用正则表达式：

```js
POST /_snapshot/my_backup/snapshot_1/_restore
{
  "indices": "test-01",
  "ignore_unavailable": true,
  "include_global_state": false,              
  "rename_pattern": "test-0(.+)",
  "rename_replacement": "restored_test_$1",
  "include_aliases": false
}
```

还原数据的时候，修改索引配置：

```js
POST /_snapshot/my_backup/snapshot_1/_restore
{
  "indices": "index_1",
  "ignore_unavailable": true,
  "index_settings": {
    "index.number_of_replicas": 0
  },
  "ignore_index_settings": [
    "index.refresh_interval"：1s
  ]
}
```



本文参考：

[Elasticsearch使用：Snapshot备份与恢复 - 云+社区 - 腾讯云 (tencent.com)](https://cloud.tencent.com/developer/article/1851297)

https://www.elastic.co/guide/en/elasticsearch/reference/6.8/modules-snapshots.html#_snapshot_status