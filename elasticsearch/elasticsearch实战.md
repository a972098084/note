# 项目实战

## 项目初始化

1、启动es服务和客户端

2、使用springboot快速构建服务

<img src="https://edu-hanghang.oss-cn-beijing.aliyuncs.com/elasticsearch/es-jd/tu1.png" style="zoom: 67%;" />

3、修改版本依赖！

```xml
<properties>
    <java.version>1.8</java.version>
    <!-- 这里SpringBoot默认配置的版本不匹配，我们需要自己配置版本！ -->
    <elasticsearch.version>7.14.0</elasticsearch.version>
</properties>
```

4、配置 `application.properties `文件

```properties
server.port=9090
# 关闭thymeleaf缓存
spring.thymeleaf.cache=false
```

5、导入前端的素材！修改为Thymeleaf支持的格式！

```html
<html xmlns:th="http://www.thymeleaf.org">
```

6、编写IndexController进行跳转测试

<img src="https://edu-hanghang.oss-cn-beijing.aliyuncs.com/elasticsearch/es-jd/tu2.png" style="zoom:80%;" />

## jsoup讲解

1、导入jsoup的依赖

```xml
<!--解析网页-->
<dependency>
    <groupId>org.jsoup</groupId>
    <artifactId>jsoup</artifactId>
    <version>1.10.2</version>
</dependency>
```

2、编写一个工具类 HtmlParseUtil

```java
public class HtmlParseUtil {
    public static void main(String[] args) throws IOException {
        // jsoup不能抓取ajax的请求，除非自己模拟浏览器进行请求！
        // 1、https://search.jd.com/Search?keyword=java
        String url = "https://search.jd.com/Search?keyword=java";
        // 2、解析网页（需要联网）
        Document document = Jsoup.parse(new URL(url), 30000);
        // 3、抓取搜索到的数据！
        // Document 就是我们JS的Document对象，你可以看到很多JS语法
        Element element = document.getElementById("J_goodsList");
        // 4、找到所有的li元素
        Elements elements = element.getElementsByTag("li");
        // 获取京东的商品信息
        for (Element el : elements) {
            //  这种网站，一般为了保证效率，一般会延时加载图片
            //  String img = el.getElementsByTag("img").eq(0).attr("src");
            String img = el.getElementsByTag("img").eq(0).attr("source-data-
lazy-img");
            String price = el.getElementsByClass("p-price").eq(0).text();
            String title = el.getElementsByClass("p-name").eq(0).text();
            System.out.println(img);
            System.out.println(price);
            System.out.println(title);
            System.out.println("================================");
        }
    }
}
```

3、封装一个实体类保存爬取下来的数据

```java
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Content {
    private String img;
    private String title;
    private String price;

}
```

4、封装为工具使用！

```java
public class HtmlParseUtil {
    // public static void main(String[] args) throws Exception {
    //     new HtmlParseUtil().parseJD("vue").forEach(System.out::println);
    // }

    public List<Content> parseJD(String keywords) throws Exception {
        // jsoup不能抓取ajax的请求，除非自己模拟浏览器进行请求！
        // 1、https://search.jd.com/Search?keyword=java
        String url = "https://search.jd.com/Search?keyword=" + keywords;
        // 2、解析网页（需要联网）
        Document document = Jsoup.parse(new URL(url), 30000);
        // 3、抓取搜索到的数据！
        // Document 就是我们JS的Document对象，你可以看到很多JS语法
        Element element = document.getElementById("J_goodsList");
        // 4、找到所有的li元素
        Elements elements = element.getElementsByTag("li");
        // 获取京东的商品信息
        ArrayList<Content> goodsList = new ArrayList<>();
        for (Element el : elements) {
            //  这种网站，一般为了保证效率，一般会延时加载图片
            String img = el.getElementsByTag("img").eq(0).attr("data-lazy-img");
            String price = el.getElementsByClass("p-price").eq(0).text();
            String title = el.getElementsByClass("p-name").eq(0).text();
            Content content = new Content();
            content.setImg(img);
            content.setPrice(price);
            content.setTitle(title);
            goodsList.add(content);
        }
        return goodsList;
    }
}
```

5、测试工具类的使用！

```java
public static void main(String[] args) throws Exception {
    new HtmlParseUtil().parseJD("vue").forEach(System.out::println);
}
```

## 业务编写 

1、导入ElasticsearchClientConfig 配置类

```java
@Configuration
public class ElasticSearchClientConfig {

    @Bean
    public RestHighLevelClient restHighLevelClient(){
        RestHighLevelClient client = new RestHighLevelClient(RestClient.builder(new HttpHost("127.0.0.1", 9200, "http")));
        return client;
    }
}
```

2、编写业务

```java
@Service
public class ContentService {

    @Autowired
    private RestHighLevelClient restHighLevelClient;

    // 解析数据放入es索引中
    public Boolean parseContent(String keywords) throws Exception {
        List<Content> contents = new HtmlParseUtil().parseJD(keywords);
        BulkRequest bulkRequest = new BulkRequest();
        bulkRequest.timeout("2m");
        for (int i = 0; i < contents.size(); i++) {
            bulkRequest.add(
                    new IndexRequest("jd_goods")
                    .source(JSON.toJSONString(contents.get(i)), XContentType.JSON));
        }
        BulkResponse bulk = restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT);
        return !bulk.hasFailures();
    }

    // 获取数据实现基本搜索功能
    public List<Map<String, Object>> searchPage(String keyword, int pageNo, int pageSize) throws IOException 	{
        if (pageNo <=1){
            pageNo = 1;
        }

        // 条件搜索
        SearchRequest searchRequest = new SearchRequest("jd_goods");
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        // 分页
        sourceBuilder.from(pageNo);
        sourceBuilder.size(pageSize);

        // 精准匹配
        TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("title", keyword);
        sourceBuilder.query(termQueryBuilder);
        sourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));

        // 执行搜索
        searchRequest.source(sourceBuilder);
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        // 解析结果
        ArrayList<Map<String, Object>> list = new ArrayList<>();
        for (SearchHit documentFields : searchResponse.getHits().getHits()) {
            list.add(documentFields.getSourceAsMap());
        }
        return list;
    }
}
```

3、controller

```java
@RestController
public class ContentController {

    @Autowired
    private ContentService contentService;

    @GetMapping("/parse/{keywords}")
    public Boolean parse(@PathVariable("keywords") String keywords) throws Exception {
        return contentService.parseContent(keywords);
    }

    @GetMapping("/search/{keyword}/{pageNo}/{pageSize}")
    public List<Map<String, Object>> search(@PathVariable("keyword") String keyword,
                                            @PathVariable("pageNo")  int pageNo,
                                            @PathVariable("pageSize")  int pageSize) throws IOException {
        return contentService.searchPage(keyword, pageNo, pageSize);
    }
}
```

## 搜索高亮

1、编写业务类，处理高亮字段

```java
// 获取数据实现基本搜索高亮功能
public List<Map<String, Object>> searchPageHighlightBuilder(String keyword, int pageNo, int pageSize) throws IOException {
    if (pageNo <=1){
        pageNo = 1;
    }

    // 条件搜索
    SearchRequest searchRequest = new SearchRequest("jd_goods");
    SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
    // 分页
    sourceBuilder.from(pageNo);
    sourceBuilder.size(pageSize);

    // 精准匹配
    TermQueryBuilder termQueryBuilder = QueryBuilders.termQuery("title", keyword);
    sourceBuilder.query(termQueryBuilder);
    sourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));

    // 高亮
    HighlightBuilder highlightBuilder = new HighlightBuilder();
    highlightBuilder.field("title");
    highlightBuilder.requireFieldMatch(false);  // 多个高亮显示
    highlightBuilder.preTags("<span style='color:red'>");
    highlightBuilder.postTags("</span>");
    sourceBuilder.highlighter(highlightBuilder);

    // 执行搜索
    searchRequest.source(sourceBuilder);
    SearchResponse searchResponse = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
    // 解析结果
    ArrayList<Map<String, Object>> list = new ArrayList<>();
    for (SearchHit hit : searchResponse.getHits().getHits()) {

        Map<String, HighlightField> highlightFields = hit.getHighlightFields();
        HighlightField title = highlightFields.get("title");
        Map<String, Object> sourceAsMap = hit.getSourceAsMap(); // 原来的结果
        // 解析高亮的字段，将原来的字段换为我们高亮的字段即可
        if (title != null){
            Text[] fragments = title.fragments();
            String n_title = "";
            for (Text text : fragments){
                n_title += text;
            }
            sourceAsMap.put("title", n_title);  // 高亮的字段替换掉原来的内容即可
        }
        list.add(sourceAsMap);
    }
    return list;
}
```

2、controller层调用新的高亮业务！

```java
@RestController
public class ContentController {

    @Autowired
    private ContentService contentService;

    @GetMapping("/parse/{keywords}")
    public Boolean parse(@PathVariable("keywords") String keywords) throws Exception {
        return contentService.parseContent(keywords);
    }

    @GetMapping("/search/{keyword}/{pageNo}/{pageSize}")
    public List<Map<String, Object>> search(@PathVariable("keyword") String keyword,
                                            @PathVariable("pageNo")  int pageNo,
                                            @PathVariable("pageSize")  int pageSize) throws IOException {
        return contentService.searchPageHighlightBuilder(keyword, pageNo, pageSize);
    }
}
```

