k8s CSI分析——容器存储接口分析

## 概述

kubernetes为了利于扩展功能，支持可插拔式架构，故提供了三个特定功能的接口，分别以下三种：

- 容器网络接口CNI
- 容器运行时接口CRI
- 容器存储接口CSI

kubernetes通过调用这几个接口就可以实现。

本文主要对容器存储接口CSI进行介绍与分析。

关键字：CSI、CSI系统架构、K8S、存储、操作流程



## CSI是什么？

尽管在 CSI Kubernetes 提供强大的卷插件系统之前，向 Kubernetes 添加对新卷插件的支持是一项挑战：卷插件是“in-tree”的，这意味着它们的代码是核心 Kubernetes 代码的一部分，并与核心 Kubernetes 二进制文件一起提供——想要为 Kubernetes 添加对存储系统的支持（甚至修复现有卷插件中的错误）的供应商被迫与 Kubernetes 发布过程保持一致。此外，第三方存储代码会导致核心 Kubernetes 二进制文件的可靠性和安全性问题，并且这些代码对于 Kubernetes 维护人员来说通常很难（并且在某些情况下是不可能的）测试和维护。

CSI是`Container Storage Interface`（容器存储接口）的简写，CSI 是作为一种标准而开发的，用于将任意块和文件存储存储系统暴露给 Kubernetes 等容器编排系统 (CO) 上的容器化工作负载。随着容器存储接口的采用，Kubernetes 卷层变得真正可扩展。使用 CSI，第三方存储提供商可以编写和部署插件，在 Kubernetes 中公开新的存储系统，而无需接触核心 Kubernetes 代码。这为 Kubernetes 用户提供了更多的存储选择，并使系统更加安全可靠。



## CSI有什么用？

尽管在CSI Kubernetes之前就提供过强大的卷插件系统，这种卷插件系统被称为“in-tree”，这就意味着它的代码与Kubernetes代码为一个整体，需要随着Kubernetes二进制文件一起提供，这就带来了如下问题：

- 如果第三方存储上发现问题需要进行修复，即便修复了也不能单独发布，需要与Kubernetes共同发布，这样两者就形成了相互依赖状态，Kubernetes不但要考虑自身版本迭代，同时还需要考虑第三方存储方的迭代版本
- 将kubernetes代码与第三方存储方代码耦合在一起，容易引起安全性、可靠性问题，代码变得复杂，增加后期维护成本

基于以上问题，kubernetes将存储系统抽离出了CSI，通过grpc接口与第三方存储商的卷插件系统进行通信。

这样kubernetes就将第三方存储代码与自身代码分隔开，提高自身代码可读性与安全性的同时，第三方存储商也可以单独发布版本和部署自己的存储插件。



## 涉及k8s对象

### 1、PersistentVolume

持久存储卷，集群级别资源，代表了存储卷资源，记录了该存储卷资源的相关信息

**回收策略**

1. tetain：保留策略
2. delete：删除策略
3. Recycle：回收策略（弃用）

**状态变迁**

available => bound => released

### 2、PersistentVolumeClaim

持久存储卷声明，namespace级别资源，代表了用户对于存储卷的使用需求声明

示例：

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: test
  namespace: test
spec:
  accessModes:
  - ReadWriteMany
  resources:
    requests:
      storage: 10Gi
  storageClassName: csi-cephfs-sc
  volumeMode: Filesystem
```

**pvc状态迁移**

pending => bound

### 3、StorageClass

定义了创建pv的模板信息，集群级别资源，用于动态创建pv

示例：

```yaml
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: csi-rbd-sc
parameters:
  clusterID: ceph01
  imageFeatures: layering
  imageFormat: "2"
  mounter: rbd
  pool: kubernetes
provisioner: rbd.csi.ceph.com
reclaimPolicy: Delete
volumeBindingMode: Immediate
```

### 4、VolumeAttachment

VolumeAttachment 记录了pv的相关挂载信息，如挂载到哪个node节点，由哪个volume plugin来挂载等。

AD Controller 创建一个 VolumeAttachment，而 External-attacher 则通过观察该 VolumeAttachment，根据其状态属性来进行存储的挂载和卸载操作。

示例：

```yaml
apiVersion: storage.k8s.io/v1
kind: VolumeAttachment
metadata:
  name: csi-123456
spec:
  attacher: cephfs.csi.ceph.com
  nodeName: 192.168.1.10
  source:
    persistentVolumeName: pvc-123456
status:
  attached: true
```

### 5、CSINode

CSINode 记录了csi plugin的相关信息（如nodeId、driverName、拓扑信息等）。

当Node Driver Registrar向kubelet注册一个csi plugin后，会创建（或更新）一个CSINode对象，记录csi plugin的相关信息。

示例：

```yaml
apiVersion: storage.k8s.io/v1
kind: CSINode
metadata:
  name: 192.168.1.10
spec:
  drivers:
  - name: cephfs.csi.ceph.com
    nodeID: 192.168.1.10
    topologyKeys: null
  - name: rbd.csi.ceph.com
    nodeID: 192.168.1.10
    topologyKeys: null
```



## 涉及组件与作用

![img](https://work-hang.oss-cn-beijing.aliyuncs.com/Kubernetes/CSI/2393007-20210724163109887-321971934.png)

### 1、volume plugin

扩展各种存储类型的卷的管理能力，实现第三方存储的各种操作能力与k8s存储系统的结合。调用第三方存储的接口或命令，从而提供数据卷的创建/删除、attach/detach、mount/umount的具体操作实现，可以认为是第三方存储的代理人。

volume plugin分为in-tree与out-of-tree

**in-tree**：在k8s源码内部实现，和k8s一起发布、管理，更新迭代慢、灵活性差。

**out-of-tree**：代码独立于k8s，由存储厂商实现，有csi、flexvolume两种实现

#### csi plugin

csi plugin分为ControllerServer与NodeServer，各负责不同的存储操作

#### external plugin

external plugin包括了external-provisioner、external-attacher、external-resizer、external-snapshotter等，external plugin辅助csi plugin组件，共同完成了存储相关操作。external plugin负责watch pvc、volumeAttachment等对象，然后调用volume plugin来完成存储的相关操作。如external-provisioner watch pvc对象，然后调用csi plugin来创建存储，最后创建pv对象；external-attacher watch volumeAttachment对象，然后调用csi plugin来做attach/dettach操作；external-resizer watch pvc对象，然后调用csi plugin来做存储的扩容操作等

#### Node-Driver-Registrar

Node-Driver-Registrar组件负责实现csi plugin（NodeServer）的注册，让kubelet感知csi plugin的存在

### 2、kube-controller-manager

#### PV controller

负责pv、pvc的绑定与生命周期管理（如创建/删除底层存储，创建/删除pv对象，pv与pvc对象的状态变更）。

（1）in-tree：创建/删除底层存储、创建/删除pv对象的操作，由PV controller调用volume plugin（in-tree）来完成。

（2）out-tree CSI：创建/删除底层存储、创建/删除pv对象的操作由external-provisioner与csi plugin共同来完成。

#### AD controller

AD Cotroller全称Attachment/Detachment 控制器，主要负责创建、删除VolumeAttachment对象，并调用volume plugin来做存储设备的Attach/Detach操作（将数据卷挂载到特定node节点上/从特定node节点上解除挂载），以及更新node.Status.VolumesAttached等。

不同的volume plugin的Attach/Detach操作逻辑有所不同，对于csi plugin（out-tree volume plugin）来说，AD controller的Attach/Detach操作只是修改VolumeAttachment对象的状态，而不会真正的将数据卷挂载到节点/从节点上解除挂载，真正的节点存储挂载/解除挂载操作由kubelet中volume manager调用csi plugin来完成

### 3. kubelet

#### volume manager

主要是管理卷的Attach/Detach（与AD controller作用相同，通过kubelet启动参数控制哪个组件来做该操作）、mount/umount等操作。

对于csi来说，volume manager的Attach/Detach操作只创建/删除VolumeAttachment对象，而不会真正的将数据卷挂载到节点/从节点上解除挂载；csi-attacer组件也不会做挂载/解除挂载操作，只是更新VolumeAttachment对象，真正的节点存储挂载/解除挂载操作由kubelet中volume manager调用调用csi plugin来完成。

## kubernetes创建与挂载volume

### in-tree volume plugin

<img src="https://work-hang.oss-cn-beijing.aliyuncs.com/Kubernetes/CSI/2393007-20210724163149524-1202333214.png" alt="img" style="zoom:33%;" />

​	（1）用户创建 `PVc` 

​	（2）`PV Controller` watch到有新的 `PVC` 创建，寻找合适的 `PV` 进行绑定

​	（3）（4）当找不到合适和 `PV` 与之绑定时，调用 `volume plugin` 来创建volume，并创建 `pv` 对象与 `pvc` 对象绑定

​	（5）用户创建关于挂载 `pvc` 的 `pod` 

​	（6）`kube-scheduler` watch到 `pod` 的创建，为其寻找合适的node调度

​	（7）（8）`pod`调度完成，`AD controller` / `volume manager` watch到 `pod` 申明的volume没有进行 `attach` 操作，调用 `volume plugin`进行 `attach` 操作

​	（9）`volume plugin` 进行 `attach` 操作，将 volume 挂载到 `pod` 所在的node节点，成为如 `/dev/ vdb/` 设备

​	（10）（11）`volume manager watch`到刚才 `attach` 完 `pod` 的 volume并没有进行 `mount` 操作，调用 `volume plugin` 来做 `mount` 操作

​	（12）`volume plugin` 进行 `mount` 操作，将node节点上的第（9）步得到的 `/dev/vdb/` 设备挂载到指定目录

### out-of-tree volume plugin

<img src="https://work-hang.oss-cn-beijing.aliyuncs.com/Kubernetes/CSI/2393007-20210724163202137-82826657.png" alt="img" style="zoom: 33%;" />

​	（1）用户创建 `pvc`

​	（2）`PV controller` watch到 `pvc` 的创建，寻找合适的 `pv` 与之绑定。当寻找不到合适的 `pv` 时，将更新 `pvc` 对象，添加 `volume.beta.kubernetes.io/storage-provisioner={csi driver name}` 注解，让 `external-provisioner` 组件开始创建存储 `pv` 对象操作

​	（3）`external-provisioner` 组件watch到 `pvc` 的创建/更新事件，判断 `volume.beta.kubernetes.io/storage-provisioner={csi driver name}` 注解，即判断是否由自己负责创建，如果是就调用 `csi-plugin ControllerServer` 来创建存储， 并创建 `pv `对象（这里的 `pv` 对象使用了提前绑定特性，将 `pvc` 信息填入 `pv` 对象的 `spec.claimRef` 属性）

​	（4）`PV controller` 将上一步创建的 `pv` 与 `pvc` 进行绑定

​	（5）用户创建关于挂载 `pvc` 的 `pod` 

​	（6）`kube-scheduler` watch到 `pod` 的创建，为其寻找合适的node调度

​	（7）（8）`pod` 调度完成后，`AD controller` / `volume manager` watch到 `pod` 声明的volume没有进行 `attach` 操作，将调用 `csi-attacher` 来做 `attach` 操作（实际上只是创建 `volumeAttachement` 对象）

​	（9）`external-attacher` 组件 watch 到 `volumeAttachment` 对象的新建，调用  `csi-plugin`  进行 `attach` 操作（如果 `volume plugin` 是 `ceph-csi` ，`external-attacher` 组件watch到 `volumeAttachment` 对象的新建后，只是修改该对象的状态属性，不会做`attach`操作，真正的`attach`操作由 `kubelet` 中的 `volume manager` 调用 `volume plugin`  `ceph-csi` 来完成）

​	（10）`csi-plugin ControllerServer` 进行  `attach`  操作，将volume挂载到 `pod` 所在 `node` 节点，成为如 `/dev/vdb` 的设备。
​	（11）（12）`attach `操作完成后，`volume manager`  watch到 `pod` 声明的volume没有进行 `mount` 操作，将调用 `csi-mounter` 来做  `mount` 操作。
​	（13）`csi-mounter` 调用 `csi-plugin NodeServer` 进行`mount`操作，将node节点上的第（10）步得到的`/dev/vdb`设备挂载到指定目录

## kubernetes存储相关操作流程具体分析

下面来看下`kubernetes`通过`ceph-csi volume plugin`来创建/删除、挂载/解除挂载ceph存储的流程

### 1、存储创建

![img](https://work-hang.oss-cn-beijing.aliyuncs.com/Kubernetes/CSI/2393007-20210724163218607-1092113440.png)

流程分析

1. 用户创建 `pvc`
2. `pv contriller` 组件 watch到 `pvc`对象创建，寻找合适的 `pv` 对 象与之匹配，当找不到合适的 `pv` 对象时，将更新 `pvc`对象，添加`volume.beta.kubernetes.io/storage-provisioner={csi driver name}` 注解。
3. `external-provisioner` 组件监听到 `pvc` 的新增事件后，判断 `pvc` 的注解的值，即判断是否由自己负责创建操作，如果是则调用`ceph-csi` 组件进行存储的创建
4. `ceph-csi` 组件调用完成底层存储创建
5. 底层存储创建完成后，`external-provisioner` 根据存储信息，拼接 `pv` 对象，创建 `pv` 对象（ `pv` 对象采用了提前绑定特性，`pv` 对象将 `pvc` 信息填入到 `spec.claimRef` 属性）
6. `pv controller` 组件监 `pvc` 对象，与第5步创建的 `pv` 对象进行绑定

### 2、存储扩容

![img](https://work-hang.oss-cn-beijing.aliyuncs.com/Kubernetes/CSI/2393007-20210724163248988-418612698.png)

流程分析

1. 修改 pvc 对象，修改申请存储大小(pvc.spec.resources.requests.storage)
2. 修改成功后，external-resizer监听到该pvc的update事件，发现`pvc.Spec.Resources.Requests.storgage`比`pvc.Status.Capacity.storgage`大，便调用ceph-csi组件进行controller端的扩容
3. ceph-csi组件调用ceph存储，完成底层存储扩容
4. 底层完成扩容后，ceph-csi 组件更新 pv对象的.Spec.Capacity.storgage值为扩容后的大小
5. kubelet的volume manager在reconcile()调谐过程中发现pv.Spec.Capacity.storgage大于pv.Status.Caoacity.storage，于是调用ceph-csi完成Node端的扩容
6. ceph-csi组件对node上存储对应的文件系统进行扩容
7. 扩容后，kubelet更新pvc.Status.Capacity.storage的值为扩容后的值

### 3、存储挂载

kubelet启动参数 `--enable-controller-attach-detach`，该启动参数设置为 `true` 表示启用 `Attach/Detach controller ` 进行 `Attach/Detach`  操作，同时禁用  `kubelet`  执行  `Attach/Detach`  操作（默认值为 `true`）。实际上 `Attach/Detach`  操作就是创建/删除 `VolumeAttachment` 对象。

（1）`kubelet` 启动参数 `--enable-controller-attach-detach=true`，`Attach/Detach controller` 进行 `Attach/Detach` 操作

![img](https://work-hang.oss-cn-beijing.aliyuncs.com/Kubernetes/CSI/2393007-20210724163307969-1618265390.png)



（2）`kubelet`启动参数 `--enable-controller-attach-detach=false`，`kubelet `端 `volume manager` 进行 `Attach/Detach`  操作

![img](https://work-hang.oss-cn-beijing.aliyuncs.com/Kubernetes/CSI/2393007-20210724163327249-925473553.png)

流程分析

1. 用户创建挂载pvc 的pod
2. AD controller / volume manager 中的 reconcile() 发现由 volume 未执行 attach 操作，于是创建VolumeAttachment对象
3. external-Attacher 组件 list / watch VolumeAttachment对象，更新 VolumeAttachment.status.attached=true
4. AD controller组件更新node对象的 .Status.VolumesAttached 属性值，将volume标记为attached
5. kubelet 中的volume manager 获取 node.Status.VolumeAttached 属性值，发现 volume 已经被标为attached
6. volume manager 中的 reconcile() 调用ceph-csi中的NodeStageVolume和NodePublishVolume完成存储的挂载

### 4、解除存储挂载

（1）`kubelet` 启动参数 `--enable-controller-attach-detach=true`，`Attach/Detach controller` 进行 `Attach/Detach`  操作

![img](https://work-hang.oss-cn-beijing.aliyuncs.com/Kubernetes/CSI/2393007-20210724163346622-133040314.png)

（2）`kubelet`启动参数`--enable-controller-attach-detach=false`，`kubelet`端`volume manager`进行`Attach/Detach` 操作

![img](https://work-hang.oss-cn-beijing.aliyuncs.com/Kubernetes/CSI/2393007-20210724163359740-1984174337.png)

流程分析

1. 用户删除申明了pvc的pod
2. AD controller/volume manage 中的 reconcile 发现volume未执行dettach操作，便执行dettach操作，即删除VolumeAttachment对象
3. AD controller/volume manage 等待VolumeAttachment对象删除成功
4. AD controller 组件更新node 的 .Status.VolumesAttached属性值，将标记attach的该volume从属性值中去除
5. kubelet中的volume manage 获取node.Status.VolumesAttached属性值，找不到相关的volume信息
6. volume manage 中的 reconcile()调用ceph-csi 组件中NodeUnpublishVolume和NodeUnstageVolume完成解除挂载存储操作

### 5、删除存储

![img](https://work-hang.oss-cn-beijing.aliyuncs.com/Kubernetes/CSI/2393007-20210724163411879-2030932565.png)

流程分析

1. 用户删除pvc对象
2. pv controller 发现pv绑定的pvc被删除，于是更新pv状态未released
3. external-provisioner组件watch到pv更新事件，检查pv的状态是否为released，删除策略是否为delete
4. 确认pv对象状态以及回收策略后external-provisioner调用ceph-csi的DeleteVolume来删除存储
5. ceph-csi组件的DeleteVolume方法，调用ceph集群命令，删除底层存储
6. 删除底层存储后external-provisioner组件删除pv对象















参考

https://kubernetes.io/blog/2019/01/15/container-storage-interface-ga/