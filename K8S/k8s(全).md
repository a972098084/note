# Kubernetes集群管理工具kubectl

## 概述

kubectl是Kubernetes集群的命令行工具，通过kubectl能够对集群本身进行管理，并能够在集群上进行容器化应用的安装和部署

## 命令格式

命令格式如下

```bash
kubectl [command] [type] [name] [flags]
```

参数

- command：指定要对资源执行的操作，例如create、get、describe、delete
- type：指定资源类型，资源类型是大小写敏感的，开发者能够以单数 、复数 和 缩略的形式

例如：

```bash
# k8s支持缩写即单复数等，no、node、nodes代表一致
kubectl get pod node
kubectl get pods pod
```

![image-20211118150752680](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211118150752680.png)

- name：指定资源的名称，名称也是大小写敏感的，如果省略名称，则会显示所有的资源，例如

```bash
kubectl get pods
```

- flags：指定可选的参数，例如，可用 -s 或者 -server参数指定Kubernetes API server的地址和端口

## 常见命令

### kubectl help 获取更多信息

通过 help命令，能够获取帮助信息

```bash
# 获取kubectl的命令
kubectl --help

# 获取某个命令的介绍和使用
kubectl get --help
```

### 基础命令

常见的基础命令

| 命令    | 介绍                                           |
| ------- | ---------------------------------------------- |
| create  | 通过文件名或标准输入创建资源                   |
| expose  | 将一个资源公开为一个新的Service                |
| run     | 在集群中运行一个特定的镜像                     |
| set     | 在对象上设置特定的功能                         |
| get     | 显示一个或多个资源                             |
| explain | 文档参考资料                                   |
| edit    | 使用默认的编辑器编辑一个资源                   |
| delete  | 通过文件名，标准输入，资源名称或标签来删除资源 |

### 部署命令

| 命令           | 介绍                                               |
| -------------- | -------------------------------------------------- |
| rollout        | 管理资源的发布                                     |
| rolling-update | 对给定的复制控制器滚动更新                         |
| scale          | 扩容或缩容Pod数量，Deployment、ReplicaSet、RC或Job |
| autoscale      | 创建一个自动选择扩容或缩容并设置Pod数量            |

### 集群管理命令

| 命令         | 介绍                           |
| ------------ | ------------------------------ |
| certificate  | 修改证书资源                   |
| cluster-info | 显示集群信息                   |
| top          | 显示资源(CPU/M)                |
| cordon       | 标记节点不可调度               |
| uncordon     | 标记节点可被调度               |
| drain        | 驱逐节点上的应用，准备下线维护 |
| taint        | 修改节点taint标记              |

### 故障和调试命令

| 命令         | 介绍                                                         |
| ------------ | ------------------------------------------------------------ |
| describe     | 显示特定资源或资源组的详细信息                               |
| logs         | 在一个Pod中打印一个容器日志，如果Pod只有一个容器，容器名称是可选的 |
| attach       | 附加到一个运行的容器                                         |
| exec         | 执行命令到容器                                               |
| port-forward | 转发一个或多个                                               |
| proxy        | 运行一个proxy到Kubernetes API Server                         |
| cp           | 拷贝文件或目录到容器中                                       |
| auth         | 检查授权                                                     |

### 其它命令

| 命令         | 介绍                                                |
| ------------ | --------------------------------------------------- |
| apply        | 通过文件名或标准输入对资源应用配置                  |
| patch        | 使用补丁修改、更新资源的字段                        |
| replace      | 通过文件名或标准输入替换一个资源                    |
| convert      | 不同的API版本之间转换配置文件                       |
| label        | 更新资源上的标签                                    |
| annotate     | 更新资源上的注释                                    |
| completion   | 用于实现kubectl工具自动补全                         |
| api-versions | 打印受支持的API版本                                 |
| config       | 修改kubeconfig文件（用于访问API，比如配置认证信息） |
| help         | 所有命令帮助                                        |
| plugin       | 运行一个命令行插件                                  |
| version      | 打印客户端和服务版本信息                            |

### 目前使用的命令

```bash
# 创建一个nginx镜像
kubectl create deployment nginx --image=nginx

# 对外暴露端口
kubectl expose deployment nginx --port=80 --type=NodePort

# 查看资源
kubectl get pod, svc

# 对资源应用进行配置
kubectl apply -f

# 如果删除pod一直等待可以是强制删除，使用以下命令
kubectl delete pod xxx -n namespace --force --grace-period=0
```

# Kubernetes集群YAML文件详解

## 概述

k8s 集群中对资源管理和资源对象编排部署都可以通过声明样式（YAML）文件来解决，也就是可以把需要对资源对象操作编辑到YAML 格式文件中，我们把这种文件叫做资源清单文件，通过kubectl 命令直接使用资源清单文件就可以实现对大量的资源对象进行编排部署了。一般在我们开发的时候，都是通过配置YAML文件来部署集群的。

YAML文件：就是资源清单文件，用于资源编排

## YAML文件介绍

### YAML概述

YAML ：仍是一种标记语言。为了强调这种语言以数据做为中心，而不是以标记语言为重点。

YAML 是一个可读性高，用来表达数据序列的格式。

### YAML 基本语法

- 使用空格做为缩进
- 缩进的空格数目不重要，只要相同层级的元素左侧对齐即可
- 低版本缩进时不允许使用Tab 键，只允许使用空格
- 使用#标识注释，从这个字符一直到行尾，都会被解释器忽略
- 使用 --- 表示新的yaml文件开始

### YAML 支持的数据结构

#### 对象

键值对的集合，又称为映射(mapping) / 哈希（hashes） / 字典（dictionary）

```yaml
# 对象类型：对象的一组键值对，使用冒号结构表示
name: Tom
age: 18

# yaml 也允许另一种写法，将所有键值对写成一个行内对象
hash: {name: Tom, age: 18}
```

#### 数组

```bash
# 数组类型：一组连词线开头的行，构成一个数组
People
- Tom
- Jack

# 数组也可以采用行内表示法
People: [Tom, Jack]
```

## YAML文件组成部分

主要分为了两部分，一个是控制器的定义 和 被控制的对象

### 控制器的定义

![92caa374b48844f5aa68dc1c7f55fd68](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/92caa374b48844f5aa68dc1c7f55fd68.png)

### 被控制的对象

包含一些 镜像，版本、端口等

![128d7a2f160741c1891abbf984825dcc](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/128d7a2f160741c1891abbf984825dcc.png)

### 属性说明

在一个YAML文件的控制器定义中，有很多属性名称

| 属性名称   | 介绍       |
| ---------- | ---------- |
| apiVersion | API版本    |
| kind       | 资源类型   |
| metadata   | 资源元数据 |
| spec       | 资源规格   |
| replicas   | 副本数量   |
| selector   | 标签选择器 |
| template   | Pod模板    |
| metadata   | Pod元数据  |
| spec       | Pod规格    |
| containers | 容器配置   |

## 如何快速编写YAML文件

一般来说，我们很少自己手写YAML文件，因为这里面涉及到了很多内容，我们一般都会借助工具来创建

### 使用kubectl create命令

这种方式一般用于资源没有部署的时候，我们可以直接创建一个YAML配置文件

```bash
# 尝试运行,并不会真正的创建镜像
kubectl create deployment web --image=nginx -o yaml --dry-run
```

或者我们可以输出到一个文件中

```bash
kubectl create deployment web --image=nginx -o yaml --dry-run > hello.yaml
```

然后我们就在文件中直接修改即可

### 使用kubectl get命令导出yaml文件

可以首先查看一个目前已经部署的镜像

```bash
kubectl get deploy
```

![image-20211118155118133](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211118155118133.png)

然后我们导出 nginx的配置

```bash
kubectl get deploy nginx -o=yaml > nginx_.yaml
```

然后会生成一个 `nginx_.yaml` 的配置文件

![image-20211118155522036](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211118155522036.png)

# Kubernetes核心技术Pod

## Pod概述

Pod是K8S系统中可以创建和管理的最小单元，是资源对象模型中由用户创建或部署的最小资源对象模型，也是在K8S上运行容器化应用的资源对象，其它的资源对象都是用来支撑或者扩展Pod对象功能的，比如控制器对象是用来管控Pod对象的，Service或者Ingress资源对象是用来暴露Pod引用对象的，PersistentVolume资源对象是用来为Pod提供存储等等，K8S不会直接处理容器，而是Pod，Pod是由一个或多个container组成。

Pod是Kubernetes的最重要概念，每一个Pod都有一个特殊的被称为 “根容器”的Pause容器。Pause容器对应的镜像属于Kubernetes平台的一部分，除了Pause容器，每个Pod还包含一个或多个紧密相关的用户业务容器。

![d51f0c45a2a0439fb603a24198151021](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/d51f0c45a2a0439fb603a24198151021.png)

### Pod基本概念

- 最小部署的单元
- Pod里面是由一个或多个容器组成【一组容器的集合】
- 一个pod中的容器是共享网络命名空间
- Pod是短暂的
- 每个Pod包含一个或多个紧密相关的用户业务容器

### Pod存在的意义

- 创建容器使用docker，一个docker对应一个容器，一个容器运行一个应用进程
- Pod是多进程设计，运用多个应用程序，也就是一个Pod里面有多个容器，而一个容器里面运行一个应用程序

![19da32824bf14c739ac1dd8f18054e22](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/19da32824bf14c739ac1dd8f18054e22.png)

- Pod的存在是为了亲密性应用
  - 两个应用之间进行交互
  - 网络之间的调用【通过127.0.0.1 或 socket】
  - 两个应用之间需要频繁调用

## Pod实现机制

主要有以下两大机制

- 共享网络
- 共享存储

### 共享网络

容器本身之间相互隔离的，一般是通过 namespace 和 group进行隔离，那么Pod里面的容器如何实现通信？

- 首先需要满足前提条件，也就是容器都在同一个namespace之间

关于Pod实现原理，首先会在Pod会创建一个根容器： `pause容器`，然后我们在创建业务容器 【nginx，redis 等】，在我们创建业务容器的时候，会把它添加到 `info容器` 中

而在 `info容器` 中会独立出 ip地址，mac地址，port 等信息，然后实现网络的共享

![750256d23053436db670b99595ce76dd](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/750256d23053436db670b99595ce76dd.png)

完整步骤如下

- 通过 Pause 容器，把其它业务容器加入到Pause容器里，让所有业务容器

### 共享存储

Pod持久化数据，专门存储到某个地方中

![865aba102b8a4b399b8d290779abc1db](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/865aba102b8a4b399b8d290779abc1db.png)

使用 Volumn数据卷进行共享存储，案例如下所示

![7da36757eed44807967262ca5738c65f](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/7da36757eed44807967262ca5738c65f.png)

## Pod镜像拉取策略

我们以具体实例来说，拉取策略就是 `imagePullPolicy`

![13aca554647d4ae58f8704e772fb2762](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/13aca554647d4ae58f8704e772fb2762.png)

拉取策略主要分为了以下几种

- IfNotPresent：默认值，镜像在宿主机上不存在才拉取
- Always：每次创建Pod都会重新拉取一次镜像
- Never：Pod永远不会主动拉取这个镜像

## Pod资源限制

也就是我们Pod在进行调度的时候，可以对调度的资源进行限制，例如我们限制 Pod调度是使用的资源是 2c4g，那么在调度对应的node节点时，只会占用对应的资源，对于不满足资源的节点，将不会进行调度

![f088c353b2854b58b04b515a3e60b023](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/f088c353b2854b58b04b515a3e60b023.png)

### 示例

我们在下面的地方进行资源的限制

![4202455b180d4c7eb8363fb7db69448d](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/4202455b180d4c7eb8363fb7db69448d.png)

这里分了两个部分

- request：表示调度所需的资源
- limits：表示最大所占用的资源

## Pod重启机制

因为Pod中包含了很多个容器，假设某个容器初选问题了，那么就会触发Pod重启机制

![912f0c6492f248d4b7313f0eb957497e](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/912f0c6492f248d4b7313f0eb957497e.png)

重启策略主要分为以下三种

- Always：当容器终止退出后，总是重启容器，默认策略 【nginx等，需要不断提供服务】
- OnFailure：当容器异常退出（退出状态码非0）时，才重启容器。
- Never：当容器终止退出，从不重启容器 【批量任务】

## Pod健康检查

通过容器检查，原来我们使用下面的命令来检查

```bash
kubectl get pod
```

但是有的时候，程序可能出现了java堆内存溢出，程序还在运行，但是不能对外提供服务了，这个时候就不能通过 容器检查来判断服务是否可用了

这个时候就可以使用应用层面的检查

```bash
# 存活检查，如果检查失败，将杀死容器，根据Pod的restartPolicy【重启策略】来操作
livenessProbe

# 就绪检查，如果检查失败，Kubernetes会把Pod从Service endpoints中剔除
readinessProbe
```

![052b341e62904978a073cca3a5af307c](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/052b341e62904978a073cca3a5af307c.png)

Probe支持以下三种检查方式

- http Get：发送HTTP请求，返回200 - 400 范围状态码为成功
- exec：执行Shell命令返回状态码是0为成功
- tcpSocket：发起TCP socket建立成功

initialDelaySeconds: 表示容器启动后多长时间后进行检测

periodseconds: 表示每个多长时间检测一次

## Pod调度策略

### 创建Pod流程

- 首先创建一个pod，然后创建一个API Server 和 Etcd【把创建出来的信息存储在etcd中】
- 然后创建 Scheduler，监控API Server是否有新的Pod，如果有的话，会通过调度算法，把pod调度某个node上
- 在node节点，会通过 `kubelet -- apiserver` 读取etcd 拿到分配在当前node节点上的pod，然后通过docker创建容器

![07f5c3e3d98c4605a35edddcbde14108](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/07f5c3e3d98c4605a35edddcbde14108.png)

### 影响Pod调度的属性

Pod资源限制对Pod的调度会有影响

#### 根据request找到足够node节点进行调度

![4202455b180d4c7eb8363fb7db69448d](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/4202455b180d4c7eb8363fb7db69448d.png)

#### 节点选择器标签影响Pod调度

#### ![e4466ae3e3c74db8aa7f3eff9df4ee0a](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/e4466ae3e3c74db8aa7f3eff9df4ee0a.png)

关于节点选择器，其实就是有两个环境，然后环境之间所用的资源配置不同

![4640ccafeeb74b1ea83f5a88ebe4bb31](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/4640ccafeeb74b1ea83f5a88ebe4bb31.png)

我们可以通过以下命令，给我们的节点新增标签，然后节点选择器就会进行调度了

```bash
kubectl label node node1 env_role=prod
```

#### 节点亲和性

节点亲和性 **nodeAffinity** 和 之前nodeSelector 基本一样的，根据节点上标签约束来决定Pod调度到哪些节点上

- 硬亲和性：约束条件必须满足

- 软亲和性：尝试满足，不保证

  ![9752ab54ae434b23863f9c7f8532dbae](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/9752ab54ae434b23863f9c7f8532dbae.png)

支持常用操作符：in、NotIn、Exists、Gt、Lt、DoesNotExists

反亲和性：就是和亲和性刚刚相反，如 NotIn、DoesNotExists等

## 污点和污点容忍

### 概述

nodeSelector 和 NodeAffinity，都是Prod调度到某些节点上，属于Pod的属性，是在调度的时候实现的。

Taint 污点：节点不做普通分配调度，是节点属性

### 场景

- 专用节点【限制ip】
- 配置特定硬件的节点【固态硬盘】
- 基于Taint驱逐【在node1不放，在node2放】

### 查看污点情况

```bash
kubectl describe node k8smaster | grep Taint
```

![image-20211118162529189](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211118162529189.png)

污点值有三个

- NoSchedule：一定不被调度
- PreferNoSchedule：尽量不被调度【也有被调度的几率】
- NoExecute：不会调度，并且还会驱逐Node已有Pod

### 未节点添加污点

```bash
kubectl taint node [node] key=value:污点的三个值
```

举例：

```bash
kubectl taint node k8snode1 env_role=yes:NoSchedule
```

### 删除污点

```bash
kubectl taint node k8snode1 env_role:NoSchedule-
```

![image-20211118163556128](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211118163556128.png)

### 演示

我们现在创建多个Pod，查看最后分配到Node上的情况

首先我们创建一个 nginx 的pod

```bash
kubectl create deployment nginx --image=nginx
```

然后使用命令查看

```bash
kubectl get pods -o wide
```

![image-20211118163815456](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211118163815456.png)

我们可以非常明显的看到，这个Pod已经被分配到 k8snode2 节点上了

下面我们把pod复制5份，在查看情况pod情况

```bash
kubectl scale deployment web --replicas=5
```

我们可以发现，因为master节点存在污点的情况，所以节点都被分配到了 node1 和 node2节点上

![image-20211118164015951](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211118164015951.png)

我们可以使用下面命令，把刚刚我们创建的pod都删除

```bash
kubectl delete deployment nginx
```

现在给了更好的演示污点的用法，我们现在给 node1节点打上污点

```bash
kubectl taint node k8snode1 env_role=yes:NoSchedule
```

然后我们查看污点是否成功添加

```bash
kubectl describe node k8snode1 | grep Taint
```

![image-20211118164300332](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211118164300332.png)

然后我们在创建一个 pod

```bash
# 创建nginx pod
kubectl create deployment nginx --image=nginx
# 复制五次
kubectl scale deployment nginx --replicas=5
```

然后我们在进行查看

```bash
kubectl get pods -o wide
```

我们能够看到现在所有的pod都被分配到了 k8snode2上，因为刚刚我们给node1节点设置了污点

![image-20211118164403765](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211118164403765.png)

最后我们可以删除刚刚添加的污点

```bash
kubectl taint node k8snode1 env_test:NoSchedule-
```

### 污点容忍

污点容忍就是某个节点可能被调度，也可能不被调度

![42c14cd353d043b7b468261d5156eb85](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/42c14cd353d043b7b468261d5156eb85.png)

# Kubernetes核心技术-Controller

## 内容

- 什么是Controller
- Pod和Controller的关系
- Deployment控制器应用场景
- yaml文件字段说明
- Deployment控制器部署应用
- 升级回滚
- 弹性伸缩

## 什么是Controller

Controller是在集群上管理和运行容器的对象，Controller是实际存在的，Pod是虚拟机的

## Pod和Controller的关系

Pod是通过Controller实现应用的运维，比如弹性伸缩，滚动升级等

Pod 和 Controller之间是通过label标签来建立关系，同时Controller又被称为控制器工作负载

![c5ba89a0ff6245e78e5996a5753f61dd](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/c5ba89a0ff6245e78e5996a5753f61dd.png)

## Deployment控制器应用

- Deployment控制器可以部署无状态应用
- 管理Pod和ReplicaSet
- 部署，滚动升级等功能
- 应用场景：web服务，微服务

## Deployment部署应用

之前我们也使用Deployment部署过应用，如下代码所示

```bash
kubectrl create deployment web --image=nginx
```

但是上述代码不是很好的进行复用，因为每次我们都需要重新输入代码，所以我们都是通过YAML进行配置

但是我们可以尝试使用上面的代码创建一个镜像【只是尝试，不会创建】

```bash
kubectl create deployment web --image=nginx --dry-run -o yaml > nginx.yaml
```

然后输出一个yaml配置文件 `nginx.yml` ，配置文件如下所示

```bash
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: web
  name: web
spec:
  replicas: 1
  selector:
    matchLabels:
      app: web
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: web
    spec:
      containers:
      - image: nginx
        name: nginx
        resources: {}
status: {}
```

我们看到的 selector 和 label 就是我们Pod 和 Controller之间建立关系的桥梁

![image-20211118165313298](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211118165313298.png)

### 使用YAML创建Pod

通过刚刚的代码，我们已经生成了YAML文件，下面我们就可以使用该配置文件快速创建Pod镜像了

```bash
kubectl apply -f nginx.yaml
```

![image-20211118165535934](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211118165535934.png)

但是因为这个方式创建的，我们只能在集群内部进行访问，所以我们还需要对外暴露端口

```bash
kubectl expose deployment web --port=80 --type=NodePort --target-port=80 --name=web1
```

关于上述命令，有几次参数

- --port：就是我们内部的端口号
- --target-port：就是暴露外面访问的端口号
- --name：名称
- --type：类型

同理，我们一样可以导出对应的配置文件

```bash
kubectl expose deployment web --port=80 --type=NodePort --target-port=80 --name=web1 -o yaml > web1.yaml
```

得到的web1.yaml如下所示

```bash
apiVersion: v1
kind: Service
metadata:
  creationTimestamp: "2020-11-16T02:26:53Z"
  labels:
    app: web
  managedFields:
  - apiVersion: v1
    fieldsType: FieldsV1
    fieldsV1:
      f:metadata:
        f:labels:
          .: {}
          f:app: {}
      f:spec:
        f:externalTrafficPolicy: {}
        f:ports:
          .: {}
          k:{"port":80,"protocol":"TCP"}:
            .: {}
            f:port: {}
            f:protocol: {}
            f:targetPort: {}
        f:selector:
          .: {}
          f:app: {}
        f:sessionAffinity: {}
        f:type: {}
    manager: kubectl
    operation: Update
    time: "2020-11-16T02:26:53Z"
  name: web2
  namespace: default
  resourceVersion: "113693"
  selfLink: /api/v1/namespaces/default/services/web2
  uid: d570437d-a6b4-4456-8dfb-950f09534516
spec:
  clusterIP: 10.104.174.145
  externalTrafficPolicy: Cluster
  ports:
  - nodePort: 32639
    port: 80
    protocol: TCP
    targetPort: 80
  selector:
    app: web
  sessionAffinity: None
  type: NodePort
status:
  loadBalancer: {}
```

然后我们可以通过下面的命令来查看对外暴露的服务

```bash
kubectl get pods,svc
```

![image-20211118170014922](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211118170014922.png)

然后我们访问对应的url，即可看到 nginx了 

![image-20211119104915282](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211119104915282.png)

## 升级回滚和弹性伸缩

- 升级： 假设从版本为1.14 升级到 1.15 ，这就叫应用的升级【升级可以保证服务不中断】
- 回滚：从版本1.15 变成 1.14，这就叫应用的回滚
- 弹性伸缩：我们根据不同的业务场景，来改变Pod的数量对外提供服务，这就是弹性伸缩

### 应用升级和回滚

首先我们先创建一个 1.14版本的Pod

```bash
apiVersion: apps/v1
kind: Deployment
metadata:
  creationTimestamp: null
  labels:
    app: web
  name: web
spec:
  replicas: 1
  selector:
    matchLabels:
      app: web
  strategy: {}
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: web
    spec:
      containers:
      - image: nginx:1.14
        name: nginx
        resources: {}
status: {}
```

我们先指定版本为1.14，然后开始创建我们的Pod

```bash
kubectl apply -f nginx.yaml
```

同时，我们使用docker images命令，就能看到我们成功拉取到了一个 1.14版本的镜像

![image-20211119105744742](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211119105744742.png)

我们使用下面的命令，可以将nginx从 1.14 升级到 1.16

```bash
kubectl set image deployment web nginx=nginx:1.16
```

在我们执行完命令后，能看到升级的过程

![image-20211119110159413](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211119110159413.png)

![image-20211119110215226](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211119110215226.png)

- 首先是开始的nginx 1.14版本的Pod在运行，然后 1.16版本的在创建
- 然后在1.16版本创建完成后，就会暂停1.14版本
- 最后把1.14版本的Pod移除，完成我们的升级

我们在下载 1.16版本，容器就处于ContainerCreating状态，然后下载完成后，就用 1.15版本去替换1.14版本了，这么做的好处就是：升级可以保证服务不中断

![f088c353b2854b58b04b515a3e60b023](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/f088c353b2854b58b04b515a3e60b023.png)

我们到我们的node2节点上，查看我们的 docker images;

![image-20211119110434418](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211119110434418.png)

能够看到，我们已经成功拉取到了 1.15版本的nginx了

#### 查看升级状态

下面可以，查看升级状态

```bash
kubectl rollout status deployment web
```

![image-20211119110834336](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211119110834336.png)

#### 查看历史版本

我们还可以查看历史版本

```bash
kubectl rollout history deployment web
```

#### 应用回滚

我们可以使用下面命令，完成回滚操作，也就是回滚到上一个版本

```bash
kubectl rollout undo deployment web
```

然后我们就可以查看状态

```bash
kubectl rollout status deployment web
```

![image-20211119111028218](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211119111028218.png)

同时我们还可以回滚到指定版本

```bash
kubectl rollout undo deployment web --to-revision=2
```

### 弹性伸缩

弹性伸缩，也就是我们通过命令一下创建多个副本

```bash
kubectl scale deployment web --replicas=5
```

能够清晰看到，我们一下创建了5个副本

![image-20211119111143349](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211119111143349.png)

# Kubernetes核心技术Service

## Service存在的意义

#### 防止Pod失联【服务发现】

因为Pod每次创建都对应一个IP地址，而这个IP地址是短暂的，每次随着Pod的更新都会变化，假设当我们的前端页面有多个Pod时候，同时后端也多个Pod，这个时候，他们之间的相互访问，就需要通过注册中心，拿到Pod的IP地址，然后去访问对应的Pod

![0e27c06374ec409485af171ef20c6400](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/0e27c06374ec409485af171ef20c6400.png)

#### 定义Pod访问策略【负载均衡】

页面前端的Pod访问到后端的Pod，中间会通过Service一层，而Service在这里还能做负载均衡，负载均衡的策略有很多种实现策略，例如：

- 随机
- 轮询
- 响应比

![cfa1e498f72349a9a8bec725841188db](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/cfa1e498f72349a9a8bec725841188db.png)

## Pod和Service的关系

这里Pod 和 Service之间还是根据 label 和 selector 建立关联的 【和Controller一样】

![f3f68ee2bcba4c658c60bf41560454ec](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/f3f68ee2bcba4c658c60bf41560454ec.png)

我们在访问service的时候，其实也是需要有一个ip地址，这个ip肯定不是pod的ip地址，而是 虚拟IP `vip`

## Service常用类型

Service常用类型有三种

- ClusterIp：集群内部访问
- NodePort：对外访问应用使用
- LoadBalancer：对外访问应用使用，公有云

### 举例

我们可以导出一个文件 包含service的配置信息

```bash
kubectl expose deployment web --port=80 --target-port=80 --dry-run -o yaml > service.yaml
```

service.yaml 如下所示

```yaml
apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  labels:
    app: web
  name: web
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
  selector:
    app: web
status:
  loadBalancer: {}
```

如果我们没有做设置的话，默认使用的是第一种方式 ClusterIp，也就是只能在集群内部使用，我们可以添加一个type字段，用来设置我们的service类型

```yaml
apiVersion: v1
kind: Service
metadata:
  creationTimestamp: null
  labels:
    app: web
  name: web
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
  selector:
    app: web
  type: NodePort
status:
  loadBalancer: {}
```

修改完命令后，我们使用创建一个pod

```bash
kubectl apply -f service.yaml
```

然后能够看到，已经成功修改为 NodePort类型了，最后剩下的一种方式就是LoadBalanced：对外访问应用使用公有云

node一般是在内网进行部署，而外网一般是不能访问到的，那么如何访问的呢？

- 找到一台可以通过外网访问机器，安装nginx，反向代理
- 手动把可以访问的节点添加到nginx中

如果我们使用LoadBalancer，就会有负载均衡的控制器，类似于nginx的功能，就不需要自己添加到nginx上

# Kubernetes控制器Controller详解

## Statefulset

Statefulset主要是用来部署有状态应用

对于StatefulSet中的Pod，每个Pod挂载自己独立的存储，如果一个Pod出现故障，从其他节点启动一个同样名字的Pod，要挂载上原来Pod的存储继续以它的状态提供服务。

### 无状态应用

我们原来使用 deployment，部署的都是无状态的应用，那什么是无状态应用？

- 认为Pod都是一样的
- 没有顺序要求
- 不考虑应用在哪个node上运行
- 能够进行随意伸缩和扩展

### 有状态应用

上述的因素都需要考虑到

- 让每个Pod独立的
- 让每个Pod独立的，保持Pod启动顺序和唯一性
- 唯一的网络标识符，持久存储
- 有序，比如mysql中的主从

适合StatefulSet的业务包括数据库服务MySQL 和 PostgreSQL，集群化管理服务Zookeeper、etcd等有状态服务

StatefulSet的另一种典型应用场景是作为一种比普通容器更稳定可靠的模拟虚拟机的机制。传统的虚拟机正是一种有状态的宠物，运维人员需要不断地维护它，容器刚开始流行时，我们用容器来模拟虚拟机使用，所有状态都保存在容器里，而这已被证明是非常不安全、不可靠的。

使用StatefulSet，Pod仍然可以通过漂移到不同节点提供高可用，而存储也可以通过外挂的存储来提供 高可靠性，StatefulSet做的只是将确定的Pod与确定的存储关联起来保证状态的连续性。

### 部署有状态应用

无头service， ClusterIp：none

这里就需要使用 StatefulSet部署有状态应用

![1ea6c566ec2f4872a27bd119604b84a9](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/1ea6c566ec2f4872a27bd119604b84a9.png)

![4546947a18904394b35f2a9b91ed9c4c](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/4546947a18904394b35f2a9b91ed9c4c.png)

```yaml
apiVersion: v1
kind: Service
metadata:
  name: nginx
  labels:
    app: nginx
  namespace: dev
spec:
  ports:
  - port: 80
    name: web
  clusterIP: None
  selector:
    app: nginx

---

apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: nginx-statefulset
  namespace: dev
spec:
  serviceName: nginx
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:latest
        ports:
        - containerPort: 80

```

然后通过查看pod，能否发现每个pod都有唯一的名称

![image-20211119114231619](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211119114231619.png)

然后我们在查看service，发现是无头的service

![image-20211119114301088](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211119114301088.png)

这里有状态的约定，肯定不是简简单单通过名称来进行约定，而是更加复杂的操作

- deployment：是有身份的，有唯一标识
- statefulset：根据主机名 + 按照一定规则生成域名

每个pod有唯一的主机名，并且有唯一的域名

- 格式：主机名称.service名称.名称空间.svc.cluster.local
- 举例：nginx-statefulset-0.default.svc.cluster.local

## DaemonSet

DaemonSet 即后台支撑型服务，主要是用来部署守护进程

长期伺服型和批处理型的核心在业务应用，可能有些节点运行多个同类业务的Pod，有些节点上又没有这类的Pod运行；而后台支撑型服务的核心关注点在K8S集群中的节点(物理机或虚拟机)，要保证每个节点上都有一个此类Pod运行。节点可能是所有集群节点，也可能是通过 nodeSelector选定的一些特定节点。典型的后台支撑型服务包括：存储、日志和监控等。在每个节点上支撑K8S集群运行的服务。

守护进程在我们每个节点上，运行的是同一个pod，新加入的节点也同样运行在同一个pod里面

- 例子：在每个node节点安装数据采集工具

  ```yaml
  apiVersion: apps/v1
  kind: DaemonSet
  metadata:
    name: ds-test 
    labels:
      app: filebeat
  spec:
    selector:
      matchLabels:
        app: filebeat
    template:
      metadata:
        labels:
          app: filebeat
      spec:
        containers:
        - name: logs
          image: nginx
          ports:
          - containerPort: 80
          volumeMounts:
          - name: varlog
            mountPath: /tmp/log
        volumes:
        - name: varlog
          hostPath:
            path: /var/log
  
  ```

  这里采用不是一个FileBeat镜像，采用什么镜像也不重要，只要证明每个node节点都会创建一个该任务，并且同通过挂在目录模拟实现了数据采集工作。

进入某个 Pod里面，进入

```bash
kubectl exec -it ds-test-cbk6v bash
```

通过该命令后，我们就能看到我们内部收集的日志信息了

![image-20211119165412774](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211119165412774.png)

## Job和CronJob

一次性任务 和 定时任务

- 一次性任务：一次性执行完就结束
- 定时任务：周期性执行

Job是K8S中用来控制批处理型任务的API对象。批处理业务与长期伺服业务的主要区别就是批处理业务的运行有头有尾，而长期伺服业务在用户不停止的情况下永远运行。Job管理的Pod根据用户的设置把任务成功完成就自动退出了。成功完成的标志根据不同的 spec.completions 策略而不同：单Pod型任务有一个Pod成功就标志完成；定数成功行任务保证有N个任务全部成功；工作队列性任务根据应用确定的全局成功而标志成功。

### Job

Job也即一次性任务

```yaml
apiVersion: batch/v1
kind: Job
metadata:
  name: pi
spec:
  template:
    spec:
      containers:
      - name: pi
        image: perl
        command: ["perl",  "-Mbignum=bpi", "-wle", "print bpi(2000)"]
      restartPolicy: Never
  backoffLimit: 4

```

使用下面命令，能够看到目前已经存在的Job

```bash
kubectl get jobs
```

![image-20211119170857812](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211119170857812.png)

在计算完成后，通过命令查看，能够发现该任务已经完成

![image-20211119170932801](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211119170932801.png)

我们可以通过查看日志，查看到一次性任务的结果

```bash
kubectl logs pi-cjprs
```

![image-20211119171031204](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211119171031204.png)

### CronJob

定时任务，cronjob.yaml如下所示

```bash
apiVersion: batch/v1beta1
kind: CronJob
metadata:
  name: hello
spec:
  schedule: "*/1 * * * *"
  jobTemplate:
    spec:
      template:
        spec:
          containers:
          - name: hello
            image: busybox
            args:
            - /bin/sh
            - -c
            - date; echo Hello from the Kubernetes cluster
          restartPolicy: OnFailure


```

这里面的命令就是每个一段时间，这里是通过 cron 表达式配置的，通过 schedule字段

然后下面命令就是每个一段时间输出

我们首先用上述的配置文件，创建一个定时任务

```bash
kubectl apply -f cronjob.yaml
```

创建完成后，我们就可以通过下面命令查看定时任务

```bash
kubectl get cronjobs
```

![image-20211119172625799](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211119172625799.png)

我们可以通过日志进行查看

```bash
kubectl logs -n dev hello-1637313360-8qdl8
```

![image-20211119172707883](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211119172707883.png)

然后每次执行，就会多出一个 pod

![image-20211119174512483](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211119174512483.png)

## 删除svc 和 statefulset

使用下面命令，可以删除我们添加的svc 和 statefulset

```bash
kubectl delete svc web

kubectl delete statefulset --all
```

## Replication Controller

Replication Controller 简称 **RC**，是K8S中的复制控制器。RC是K8S集群中最早的保证Pod高可用的API对象。通过监控运行中的Pod来保证集群中运行指定数目的Pod副本。指定的数目可以是多个也可以是1个；少于指定数目，RC就会启动新的Pod副本；多于指定数目，RC就会杀死多余的Pod副本。

即使在指定数目为1的情况下，通过RC运行Pod也比直接运行Pod更明智，因为RC也可以发挥它高可用的能力，保证永远有一个Pod在运行。RC是K8S中较早期的技术概念，只适用于长期伺服型的业务类型，比如控制Pod提供高可用的Web服务。

### Replica Set

Replica Set 检查 RS，也就是副本集。RS是新一代的RC，提供同样高可用能力，区别主要在于RS后来居上，能够支持更多种类的匹配模式。副本集对象一般不单独使用，而是作为Deployment的理想状态参数来使用

# Kubernetes配置管理

## Secret

Secret的主要作用就是加密数据，然后存在etcd里面，让Pod容器以挂载Volume方式进行访问

场景：用户名 和 密码进行加密

一般场景的是对某个字符串进行base64编码 进行加密

```bash
echo -n 'admin' | base64
```

![image-20211119174117455](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211119174117455.png)

### 变量形式挂载到Pod

- 创建secret加密数据的yaml文件 secret.yaml

  ```yaml
  apiVersion: v1
  kind: Secret
  metadata:
    name: mysecret
  type: Opaque
  data:
    username: YWRtaW4=
    password: MWYyZDFlMmU2N2Rm
  ```

  通过下面命令创建

  ```bash
  kubectl create -f secret.yaml
  ```

- 创建对应挂在的pod，mypod.yaml文件如下

  ```yaml
  apiVersion: v1
  kind: Pod
  metadata:
    name: mypod
  spec:
    containers:
    - name: nginx
      image: nginx
      env:
        - name: SECRET_USERNAME
          valueFrom:
            secretKeyRef:
              name: mysecret
              key: username
        - name: SECRET_PASSWORD
          valueFrom:
            secretKeyRef:
              name: mysecret
              key: password
  
  ```

  然后创建一个pod

  ```bash
  kubectl create -f mypod.yaml
  ```

  通过get命令查看

  ```bash
  kubectl get pods
  ```

  ![image-20211119180842669](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211119180842669.png)

  然后我们通过下面的命令，进入到我们的容器内部

  ```bash
  kubectl exec -it mypod bash
  ```

  然后我们就可以输出我们的值，这就是以变量的形式挂载到我们的容器中

  ```bash
  # 输出用户
  echo $SECRET_USERNAME
  # 输出密码
  echo $SECRET_PASSWORD
  ```

  ![image-20211119181026876](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211119181026876.png)

  最后如果我们要删除这个Pod和 secret，就可以使用这个命令

  ```bash
  kubectl delete -f mypod.yaml
  kubectl delete -f secret.yaml
  ```

### 数据卷形式挂载

- 首先我们创建一个 secret-val.yaml 文件

  ```yaml
  apiVersion: v1
  kind: Pod
  metadata:
    name: mypod
  spec:
    containers:
    - name: nginx
      image: nginx
      volumeMounts:
      - name: foo
        mountPath: "/etc/foo"
        readOnly: true
    volumes:
    - name: foo
      secret:
        secretName: mysecret
  ```

- 然后创建我们的 Pod

  ```yaml
  # 根据配置创建容器
  kubectl apply -f secret-val.yaml
  # 进入容器
  kubectl exec -it mypod bash
  # 查看
  ls /etc/foo
  ```

  ![image-20211122103318761](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211122103318761.png)

## ConfigMap

ConfigMap作用是存储不加密的数据到etcd中，让Pod以变量或数据卷Volume挂载到容器中

应用场景：配置文件

### 创建配置文件

首先我们需要创建一个配置文件 `redis.properties`

```bash
redis.port=127.0.0.1
redis.port=6379
redis.password=123456
```

### 创建ConfigMap

我们使用命令创建configmap

```bash
kubectl create configmap redis-config --from-file=redis.properties
```

然后查看详细信息

```bash
kubectl describe cm redis-config
```

![image-20211122104501186](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211122104501186.png)

### Volume数据卷形式挂载

首先我们需要创建一个 `cm.yaml`

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  containers:
    - name: busybox
      image: busybox
      command: [ "/bin/sh","-c","cat /etc/config/redis.properties" ]
      volumeMounts:
      - name: config-volume
        mountPath: /etc/config
  volumes:
    - name: config-volume
      configMap:
        name: redis-config
  restartPolicy: Never
```

然后使用该yaml创建我们的pod

```bash
# 创建
kubectl apply -f cm.yaml
# 查看
kubectl get pods
```

![image-20211122104912967](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211122104912967.png)

最后我们通过命令就可以查看结果输出了

```bash
kubectl logs mypod
```

![image-20211122104934445](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211122104934445.png)

### 以变量的形式挂载Pod

首先我们也有一个 myconfig.yaml文件，声明变量信息，然后以configmap创建

```yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: myconfig
  namespace: default
data:
  special.level: info
  special.type: hello
```

然后我们就可以创建我们的配置文件

```bash
# 创建pod
kubectl apply -f myconfig.yaml
# 获取
kubectl get cm
```

![image-20211122105609539](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211122105609539.png)

然后我们创建完该configmap后，我们就需要在创建一个 config-var.yaml 来使用我们的配置信息

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  containers:
    - name: busybox
      image: busybox
      command: [ "/bin/sh", "-c", "echo $(LEVEL) $(TYPE)" ]
      env:
        - name: LEVEL
          valueFrom:
            configMapKeyRef:
              name: myconfig
              key: special.level
        - name: TYPE
          valueFrom:
            configMapKeyRef:
              name: myconfig
              key: special.type
  restartPolicy: Never
```

最后我们查看输出

```bash
kubectl logs mypod
```

![image-20211122105953943](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211122105953943.png)

# Kubernetes集群安全机制(二进制方便)

## 概述

当我们访问K8S集群时，需要经过三个步骤完成具体操作

- 认证
- 鉴权【授权】
- 准入控制

进行访问的时候，都需要经过 apiserver， apiserver做统一协调，比如门卫

- 访问过程中，需要证书、token、或者用户名和密码
- 如果访问pod需要serviceAccount



# Kubernetes核心技术Ingress

## 前言

原来我们需要将端口号对外暴露，通过 ip + 端口号就可以进行访问

原来是使用Service中的NodePort来实现

- 在每个节点上都会启动端口
- 在访问的时候通过任何节点，通过ip + 端口号就能实现访问

但是NodePort还存在一些缺陷

- 因为端口不能重复，所以每个端口只能使用一次，一个端口对应一个应用
- 实际访问中都是用域名，根据不同域名跳转到不同端口服务中

## Ingress和Pod关系

pod 和 ingress 是通过service进行关联的，而ingress作为统一入口，由service关联一组pod中

![20b4d1b5b25d421cb50588ae853a4a98](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/20b4d1b5b25d421cb50588ae853a4a98.png)

- 首先service就是关联我们的pod
- 然后ingress作为入口，首先需要到service，然后发现一组pod
- 发现pod后，就可以做负载均衡等操作

## Ingress工作流程

在实际的访问中，我们都是需要维护很多域名， a.com 和 b.com

然后不同的域名对应的不同的Service，然后service管理不同的pod

![8b5c86e7bcce4c4fa0cae17612e94ff8](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/8b5c86e7bcce4c4fa0cae17612e94ff8.png)

需要注意，ingress不是内置的组件，需要我们单独的安装

## 使用Ingress

步骤如下所示

- 部署ingress Controller【需要下载官方的】
- 创建ingress规则【对哪个Pod、名称空间配置规则】

### 创建Nginx Pod

创建一个nginx应用，然后对外暴露端口

```bash
# 创建pod
kubectl create deployment web --image=nginx
# 查看
kubectl get pods
```

对外暴露端口

```bash
kubectl expose deployment web --port=80 --target-port=80 --type=NodePort
```

### 部署 ingress controller

下面我们来通过yaml的方式，部署我们的ingress，配置文件如下所示ingress-controller.yaml

```yaml
apiVersion: v1
kind: Namespace
metadata:
  name: ingress-nginx
  labels:
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/part-of: ingress-nginx

---

kind: ConfigMap
apiVersion: v1
metadata:
  name: nginx-configuration
  namespace: ingress-nginx
  labels:
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/part-of: ingress-nginx

---
kind: ConfigMap
apiVersion: v1
metadata:
  name: tcp-services
  namespace: ingress-nginx
  labels:
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/part-of: ingress-nginx

---
kind: ConfigMap
apiVersion: v1
metadata:
  name: udp-services
  namespace: ingress-nginx
  labels:
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/part-of: ingress-nginx

---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: nginx-ingress-serviceaccount
  namespace: ingress-nginx
  labels:
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/part-of: ingress-nginx

---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRole
metadata:
  name: nginx-ingress-clusterrole
  labels:
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/part-of: ingress-nginx
rules:
  - apiGroups:
      - ""
    resources:
      - configmaps
      - endpoints
      - nodes
      - pods
      - secrets
    verbs:
      - list
      - watch
  - apiGroups:
      - ""
    resources:
      - nodes
    verbs:
      - get
  - apiGroups:
      - ""
    resources:
      - services
    verbs:
      - get
      - list
      - watch
  - apiGroups:
      - ""
    resources:
      - events
    verbs:
      - create
      - patch
  - apiGroups:
      - "extensions"
      - "networking.k8s.io"
    resources:
      - ingresses
    verbs:
      - get
      - list
      - watch
  - apiGroups:
      - "extensions"
      - "networking.k8s.io"
    resources:
      - ingresses/status
    verbs:
      - update

---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: Role
metadata:
  name: nginx-ingress-role
  namespace: ingress-nginx
  labels:
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/part-of: ingress-nginx
rules:
  - apiGroups:
      - ""
    resources:
      - configmaps
      - pods
      - secrets
      - namespaces
    verbs:
      - get
  - apiGroups:
      - ""
    resources:
      - configmaps
    resourceNames:
      # Defaults to "<election-id>-<ingress-class>"
      # Here: "<ingress-controller-leader>-<nginx>"
      # This has to be adapted if you change either parameter
      # when launching the nginx-ingress-controller.
      - "ingress-controller-leader-nginx"
    verbs:
      - get
      - update
  - apiGroups:
      - ""
    resources:
      - configmaps
    verbs:
      - create
  - apiGroups:
      - ""
    resources:
      - endpoints
    verbs:
      - get

---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: RoleBinding
metadata:
  name: nginx-ingress-role-nisa-binding
  namespace: ingress-nginx
  labels:
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/part-of: ingress-nginx
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: nginx-ingress-role
subjects:
  - kind: ServiceAccount
    name: nginx-ingress-serviceaccount
    namespace: ingress-nginx

---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: nginx-ingress-clusterrole-nisa-binding
  labels:
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/part-of: ingress-nginx
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: nginx-ingress-clusterrole
subjects:
  - kind: ServiceAccount
    name: nginx-ingress-serviceaccount
    namespace: ingress-nginx

---

apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-ingress-controller
  namespace: ingress-nginx
  labels:
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/part-of: ingress-nginx
spec:
  replicas: 1
  selector:
    matchLabels:
      app.kubernetes.io/name: ingress-nginx
      app.kubernetes.io/part-of: ingress-nginx
  template:
    metadata:
      labels:
        app.kubernetes.io/name: ingress-nginx
        app.kubernetes.io/part-of: ingress-nginx
      annotations:
        prometheus.io/port: "10254"
        prometheus.io/scrape: "true"
    spec:
      hostNetwork: true
      # wait up to five minutes for the drain of connections
      terminationGracePeriodSeconds: 300
      serviceAccountName: nginx-ingress-serviceaccount
      nodeSelector:
        kubernetes.io/os: linux
      containers:
        - name: nginx-ingress-controller
          image: lizhenliang/nginx-ingress-controller:0.30.0
          args:
            - /nginx-ingress-controller
            - --configmap=$(POD_NAMESPACE)/nginx-configuration
            - --tcp-services-configmap=$(POD_NAMESPACE)/tcp-services
            - --udp-services-configmap=$(POD_NAMESPACE)/udp-services
            - --publish-service=$(POD_NAMESPACE)/ingress-nginx
            - --annotations-prefix=nginx.ingress.kubernetes.io
          securityContext:
            allowPrivilegeEscalation: true
            capabilities:
              drop:
                - ALL
              add:
                - NET_BIND_SERVICE
            # www-data -> 101
            runAsUser: 101
          env:
            - name: POD_NAME
              valueFrom:
                fieldRef:
                  fieldPath: metadata.name
            - name: POD_NAMESPACE
              valueFrom:
                fieldRef:
                  fieldPath: metadata.namespace
          ports:
            - name: http
              containerPort: 80
              protocol: TCP
            - name: https
              containerPort: 443
              protocol: TCP
          livenessProbe:
            failureThreshold: 3
            httpGet:
              path: /healthz
              port: 10254
              scheme: HTTP
            initialDelaySeconds: 10
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 10
          readinessProbe:
            failureThreshold: 3
            httpGet:
              path: /healthz
              port: 10254
              scheme: HTTP
            periodSeconds: 10
            successThreshold: 1
            timeoutSeconds: 10
          lifecycle:
            preStop:
              exec:
                command:
                  - /wait-shutdown

---

apiVersion: v1
kind: LimitRange
metadata:
  name: ingress-nginx
  namespace: ingress-nginx
  labels:
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/part-of: ingress-nginx
spec:
  limits:
  - min:
      memory: 90Mi
      cpu: 100m
    type: Container
```

这个文件里面，需要注意的是 hostNetwork: true，改成ture是为了让后面访问到

通过这种方式，其实我们在外面就能访问，这里还需要在外面添加一层

```bash
kubectl apply -f ingress-controller.yaml
```

![image-20211122154317487](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211122154317487.png)

最后通过下面命令，查看是否成功部署 ingress

```bash
kubectl get pods -n ingress-nginx
```

![image-20211122154344504](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211122154344504.png)

### 创建ingress规则文件

创建ingress规则文件，ingress-h.yaml

```yaml
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: example-ingress
spec:
  rules:
  - host: example.ingredemo.com
    http:
      paths:
      - path: /
        backend:
          serviceName: web
          servicePort: 80
```

### 添加域名访问规则

在windows 的 hosts文件，添加域名访问规则

![image-20211122184157759](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211122184157759.png)

![image-20211122184304703](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211122184304703.png)

最后通过域名就能访问

### ingress规则文件之自定义访问路径

以spring boot中的swagger项目为例，docker打包镜像等省略，ingress规则文件如下

```yaml
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: example-ingress
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /$2
spec:
  rules:
  - host: hang.com
    http:
      paths:
      - path: /aaa(/|$)(.*)
        pathType: Prefix
        backend:
          serviceName: swagger
          servicePort: 8081
```

访问结果

![image-20211122184634490](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211122184634490.png)

# Kubernetes核心技术Helm

Helm就是一个包管理工具【类似于npm】

![67e8d576f06b438aa7de6b80a1175b22](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/67e8d576f06b438aa7de6b80a1175b22.png)

## 为什么引入Helm

首先在原来项目中都是基于yaml文件来进行部署发布的，而目前项目大部分微服务化或者模块化，会分成很多个组件来部署，每个组件可能对应一个deployment.yaml,一个service.yaml,一个Ingress.yaml还可能存在各种依赖关系，这样一个项目如果有5个组件，很可能就有15个不同的yaml文件，这些yaml分散存放，如果某天进行项目恢复的话，很难知道部署顺序，依赖关系等，而所有这些包括

- 基于yaml配置的集中存放
- 基于项目的打包
- 组件间的依赖

但是这种方式部署，会有什么问题呢？

- 如果使用之前部署单一应用，少数服务的应用，比较合适
- 但如果部署微服务项目，可能有几十个服务，每个服务都有一套yaml文件，需要维护大量的yaml文件，版本管理特别不方便

Helm的引入，就是为了解决这个问题

- 使用Helm可以把这些YAML文件作为整体管理
- 实现YAML文件高效复用
- 使用helm应用级别的版本管理

## Helm介绍

Helm是一个Kubernetes的包管理工具，就像Linux下的包管理器，如yum/apt等，可以很方便的将之前打包好的yaml文件部署到kubernetes上。

Helm有三个重要概念

- helm：一个命令行客户端工具，主要用于Kubernetes应用chart的创建、打包、发布和管理
- Chart：应用描述，一系列用于描述k8s资源相关文件的集合
- Release：基于Chart的部署实体，一个chart被Helm运行后将会生成对应的release，将在K8S中创建出真实的运行资源对象。也就是应用级别的版本管理
- Repository：用于发布和存储Chart的仓库

## Helm组件及架构

Helm采用客户端/服务端架构，有如下组件组成

- Helm CLI是Helm客户端，可以在本地执行
- Tiller是服务器端组件，在Kubernetes集群上运行，并管理Kubernetes应用程序
- Repository是Chart仓库，Helm客户端通过HTTP协议来访问仓库中Chart索引文件和压缩包

![ef1b59988c1148fa8f2452d803df7e7a](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/ef1b59988c1148fa8f2452d803df7e7a.png)

## Helm v3变化

2019年11月13日，Helm团队发布了Helm v3的第一个稳定版本

该版本主要变化如下

- 架构变化
  - 最明显的变化是Tiller的删除
  - V3版本删除Tiller
  - relesase可以在不同命名空间重用

V3之前

![f3ebe67616c847718bd080c885b06a2e](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/f3ebe67616c847718bd080c885b06a2e.png)

V3版本

![c946b315122c48158f86b8753ea23466](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/c946b315122c48158f86b8753ea23466.png)

## helm配置

首先我们需要去 [官网下载](https://helm.sh/docs/intro/quickstart/)

- 第一步，[下载helm](https://github.com/helm/helm/releases)安装压缩文件，上传到linux系统中
- 第二步，解压helm压缩文件，把解压后的helm目录复制到 usr/bin 目录中
- 使用命令：helm

我们都知道yum需要配置yum源，那么helm就就要配置helm源

## helm仓库

添加仓库

```bash
helm repo add 仓库名  仓库地址 
```

例如

```bash
# 配置微软源
helm repo add stable http://mirror.azure.cn/kubernetes/charts
# 配置阿里源
helm repo add aliyun https://kubernetes.oss-cn-hangzhou.aliyuncs.com/charts

# 更新
helm repo update
```

然后可以查看我们添加的仓库地址

```bash
# 查看全部
helm repo list
# 查看某个
helm search repo stable
```

![image-20211123094953431](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211123094953431.png)

或者可以删除我们添加的源

```bash
helm repo remove stable
```

## helm基本命令

- chart install
- chart upgrade
- chart rollback

## 使用helm快速部署应用

### 使用命令搜索应用

首先我们使用命令，搜索我们需要安装的应用

```bash
# 搜索 weave仓库
helm search repo weave
```

![image-20211123103209363](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211123103209363.png)

### 根据搜索内容选择安装

搜索完成后，使用命令进行安装

```bash
# ui为安装后的名称， aliyun/weave-scope为要安装的名称
helm install ui aliyun/weave-scope
```

安装完成后，通过下面命令即可查看

```bash
helm list
```

同时可以通过下面命令，查看更新具体的信息

```bash
helm status ui
```

但是我们通过查看 svc状态，发现没有对象暴露端口

所以我们需要修改service的yaml文件，添加NodePort

```bash
kubectl edit svc ui-weave-scope
```

![image-20211123111138747](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211123111138747.png)

这样就可以对外暴露端口了

![image-20211123111207450](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211123111207450.png)

然后我们通过 ip + 30503 即可访问

### 如果自己创建Chart

使用命令，自己创建Chart

```bash
helm create mychart
```

创建完成后，我们就能看到在当前文件夹下，创建了一个 mychart目录

![image-20211123112158746](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211123112158746.png)

#### 目录格式

- templates：编写yaml文件存放到这个目录
- values.yaml：存放的是全局的yaml文件
- chart.yaml：当前chart属性配置信息

### 在templates文件夹创建两个文件

我们创建以下两个

- deployment.yaml
- service.yaml

我们可以通过下面命令创建出yaml文件

```bash
# 导出deployment.yaml
kubectl create deployment web1 --image=nginx --dry-run -o yaml > deployment.yaml
# 导出service.yaml 【可能需要创建 deployment，不然会报错】
kubectl expose deployment web1 --port=80 --target-port=80 --type=NodePort --dry-run -o yaml > service.yaml
```

### 安装mychart

执行命令创建

```bash
helm install web1 mychart
```

### 应用升级

当我们修改了mychart中的东西后，就可以进行升级操作

```bash
helm upgrade web1 mychart
```

![image-20211123135519432](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211123135519432.png)

## chart模板使用

通过传递参数，动态渲染模板，yaml内容动态从传入参数生成

![image-20211123135602329](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211123135602329.png)

刚刚我们创建mychart的时候，看到有values.yaml文件，这个文件就是一些全局的变量，然后在templates中能取到变量的值，下面我们可以利用这个，来完成动态模板

- 在values.yaml定义变量和值
- 具体yaml文件，获取定义变量值
- yaml文件中大题有几个地方不同
  - image
  - tag
  - label
  - port
  - replicas

### 定义变量和值

在values.yaml定义变量和值

![image-20211123140416138](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211123140416138.png)

### 获取变量和值

我们通过表达式形式 使用全局变量 `{{.Values.变量名称}}`

例如： `{{.Release.Name}}`

![image-20211123142055524](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211123142055524.png)

安装应用
在我们修改完上述的信息后，就可以尝试的创建应用了

helm install --dry-run web2 mychart

![image-20211123142939533](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211123142939533.png)

# Kubernetes持久化存储

## 前言

之前我们有提到数据卷：`emptydir` ，是本地存储，pod重启，数据就不存在了，需要对数据持久化存储

对于数据持久化存储【pod重启，数据还存在】，有两种方式

- nfs：网络存储【通过一台服务器来存储】

## 步骤

### 持久化服务器上操作

- 找一台新的服务器nfs服务端，安装nfs
- 设置挂载路径

使用命令安装nfs

```bash
yum install -y nfs-utils
```

首先创建存放数据的目录

```bash
mkdir -p /data/nfx
```

设置挂载路径

```bash
# 打开文件
vim /etc/exports
# 添加如下内容
/data/nfs *(rw,no_root_squash)
```

执行完成后，即部署完我们的持久化服务器

### Node节点上操作

然后需要在k8s集群node节点上安装nfs，这里需要在 node1 和 node2节点上安装

```bash
yum install -y nfs-utils
```

执行完成后，会自动帮我们挂载上

### 启动nfs服务端

下面我们回到nfs服务端，启动我们的nfs服务

```bash
systemctl start nfs
```

![image-20211123161528507](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211123161528507.png)

### K8s集群部署应用

最后我们在k8s集群上部署应用，使用nfs持久化存储

```bash
# 创建一个pv文件
mkdir pv
# 进入
cd pv
```

然后创建一个yaml文件 `nfs-nginx.yaml`

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-dep1
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx
        volumeMounts:
        - name: wwwroot
          mountPath: /usr/share/nginx/html
        ports:
        - containerPort: 80
      volumes:
        - name: wwwroot
          nfs:
            server: 192.168.1.104
            path: /data/nfs
```

通过这个方式，就挂载到了刚刚我们的nfs数据节点下的 /data/nfs 目录

最后就变成了： /usr/share/nginx/html -> 192.168.1.104/data/nfs 内容是对应的

我们通过这个 yaml文件，创建一个pod

```bash
kubectl apply -f nfs-nginx.yaml
```

创建完成后，我们也可以查看日志

```bash
kubectl describe pod nginx-dep1
```

![image-20211123161823629](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211123161823629.png)

可以看到，我们的pod已经成功创建出来了，同时下图也是出于Running状态

![image-20211123161853688](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211123161853688.png)

下面我们就可以进行测试了，比如现在nfs服务节点上添加数据，然后在看数据是否存在 pod中

```bash
# 进入pod中查看
kubectl exec -it nginx-dep1 bash
```

![image-20211123161923363](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211123161923363.png)

## PV和PVC

> pv和pvc通过容量和模式来进行绑定

对于上述的方式，我们都知道，我们的ip 和端口是直接放在我们的容器上的，这样管理起来可能不方便

所以这里就需要用到 pv 和 pvc的概念了，方便我们配置和管理我们的 ip 地址等元信息

PV：持久化存储，对存储的资源进行抽象，对外提供可以调用的地方【生产者】

PVC：用于调用，不需要关心内部实现细节【消费者】

### 实现流程

- PVC绑定PV
- 定义PVC
- 定义PV【数据卷定义，指定数据存储服务器的ip、路径、容量和匹配模式】

### 举例

创建一个 pvc.yaml

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-dep1
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx
        volumeMounts:
        - name: wwwroot
          mountPath: /usr/share/nginx/html
        ports:
        - containerPort: 80
      volumes:
      - name: wwwroot
        persistentVolumeClaim:
          claimName: my-pvc

---

apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: my-pvc
spec:
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 5Gi
```

第一部分是定义一个 deployment，做一个部署

- 副本数：3
- 挂载路径
- 调用：是通过pvc的模式

然后定义pvc

然后在创建一个 `pv.yaml`

```yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: my-pv
spec:
  capacity:
    storage: 5Gi
  accessModes:
    - ReadWriteMany
  nfs:
    path: /data/nfs
    server: 192.168.1.104
```

然后就可以创建pod了

```bash
kubectl apply -f pvc.yaml
kubectl apply -f pv.yaml
```

然后我们就可以通过下面命令，查看我们的 pv 和 pvc之间的绑定关系

```bash
kubectl get pv,pvc
```

![image-20211123163720422](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/Kubernetes/new/image-20211123163720422.png)

到这里为止，我们就完成了我们 pv 和 pvc的绑定操作，通过之前的方式，进入pod中查看内容

```bash
kubect exec -it nginx-dep1 bash
```

然后查看 /usr/share/nginx.html

