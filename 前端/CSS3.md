# 1、初识CSS3 

## **1.1、什么是CSS**

<font color=red>Cascading Style Sheet  级联样式表。</font>

**表现**HTML或XHTML文件样式的计算机**语言**。

包括对字体、颜色、边距、高度、宽度、背景图片、网页定位等设定。

![image-20210912223434693](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912223434693.png)

**说明：**

- 首先介绍什么是CSS
- 然后对比讲解使用CSS和没有使用CSS的两个相同的HTML代码页面显示效果，说明CSS的重要性
- 最后根据图说明CSS在网页中的应用

## **1.2、CSS的发展史** 

![image-20210912223504578](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912223504578.png)

CSS1.0   读者可以从其他地方去使用自己喜欢的设计样式去继承性地使用样式；

CSS2.0 融入了DIV+CSS的概念，提出了HTML结构与CSS样式表的分离

CSS2.1 融入了更多高级的用法，如浮动，定位等。

CSS3.0 它包括了CSS2.1下的所有功能，是目前最新的版本，它向着模块化的趋势发展，又加了很多使用的新技术，如字体、多背景、圆角、阴影、动画等高级属性，但是它需要高级浏览器的支持。

由于现在IE 6、IE 7使用比例已经很少，对市场企业进行调研发现使用CSS3的频率大幅增加，学习CSS3已经成为一种趋势



**CSS的优势**

- 内容与表现分离
- 网页的表现统一，容易修改
- 丰富的样式，使得页面布局更加灵活
- 减少网页的代码量，增加网页的浏览速度，节省网络带宽
- 运用独立于页面的CSS，有利于网页被搜索引擎收录

## **1.3、CSS的基本语法**

- 首先讲解CSS的基本语法结构，由选择器和声明构成
- 然后对照具体的样式详细讲解语法，强调声明必须在{ }中
- 最后说明基本W3C的规范，每条声明后的;都要写上

![image-20210912223719681](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912223719681.png)

**<font color=red>Style标签</font>**

- 讲解CSS样式如何在HTML中应用，引入style标签的应用
- 讲解style标签，说明type=“text/css的用法
- 说明style标签在HTML文档中的位置，在与之间

![image-20210912223804207](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912223804207.png)

## 1.4、引入CSS方式 

- <font color=red>行内样式</font>

  使用style属性引入CSS样式

  ```html
  <h1 style="color:red;">style属性的应用</h1>
  <p style="font-size:14px; color:green;">直接在HTML标签中设置的样式</p>
  ```
  
  使用style属性设置CSS样式仅对当前的HTML标签起作为，并且是写在HTML标签中的这种方式不能起到内容与表现相分离，本质上没有体现出CSS的优势，因此不推荐使用。

- <font color=red>内部样式表</font>

  CSS代码写在` <head> `的` <style> `标签中

  ```html
  <style>
      h1{color: green; }
  </style>
  ```

  优点：方便在同页面中修改样式

  缺点：不利于在多页面间共享复用代码及维护，对内容与样式的分离也不够彻底引出外部样式表

- <font color=red>外部样式表</font>

  CSS代码保存在扩展名为.css的样式表中

  HTML文件引用扩展名为.css的样式表，有两种方式
  - **链接式**（使用的最多）

    使用 `<link> `标签链接外部样式表，并讲解各参数的含义，` <link>` 标签必须放在`<head>` 标签中

    ![image-20210912225312329](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912225312329.png)

  - **导入式**

    使用@import导入外部样式表

    ![image-20210912225352245](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912225352245.png)

<font color=red>链接式与导入式的区别</font>

1. `<link/> `标签是属于XHTML范畴的，@import是属于CSS2.1中特有的。
2. 使用 `<link/>` 链接的CSS是客户端浏览网页时先将外部CSS文件加载到网页当中，然后再进行编译显示，所以这种情况下显示出来的网页与用户预期的效果一样，即使网速再慢也一样的效果。
3. 使用@import导入的CSS文件，客户端在浏览网页时是先将HTML结构呈现出来，再把外部CSS文件加载到网页当中，当然最终的效果也与使用` <link/> `链接文件效果一样，只是当网速较慢时会先显示没有CSS统一布局的HTML网页，这样就会给用户很不好的感觉。这个也是现在目前大多少网站采用链接外部样式表的主要原因。
4. 由于@import是属于CSS2.1中特有的，因此对于不兼容CSS2.1的浏览器来说就是无效的。

**<font color=red>CSS样式优先级</font>**

```html
行内样式>内部样式表>外部样式表
就近原则：越接近标签的样式优先级越高
```

## **1.5、CSS基本选择器** 

- <font color=red>标签选择器</font>

  ```html
  HTML标签作为标签选择器的名称
  <h1>...<h6>、<p>、<img/>
  ```

<img src="http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912225649994.png" alt="image-20210912225649994" style="zoom:80%;" />

- <font color=red>类选择器</font>

  一些特殊的实现效果，单纯使用标签选择器不能实现，从而引出类选择器

  ![image-20210912225803140](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912225803140.png)

- <font color=red>ID选择器</font>

  ID选择器的名称就是HTML中标签的ID名称，ID全局唯一

  ![image-20210912225852853](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912225852853.png)

**<font color=red>小结</font>**

- 标签选择器直接应用于HTML标签
- 类选择器可在页面中多次使用
- ID选择器在同一个页面中只能使用一次

**<font color=red>基本选择器的优先级</font>**

```html
ID选择器>类选择器>标签选择器
```

标签选择器是否也遵循“就近原则”？

**不遵循，无论是哪种方式引入CSS样式，一般都遵循ID选择器 > class类选择器 > 标签选择器的优先级**

## 1.6、CSS高级选择器  

### **1、层次选择器** 

![image-20210912230127915](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912230127915.png)

**<font color=red>后代选择器</font>**

```css
body p{  
    background: red;  
}
```

![image-20210912230232925](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912230232925.png)

**后代选择器两个选择符之间必须要以空格隔开，中间不能有任何其他的符号插入**

**<font color=red>子选择器</font>**

```css
body>p{
    background: pink;  
}
```

![image-20210912230344426](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912230344426.png)

**<font color=red>相邻兄弟选择器</font>**

```css
.active+p {
    background: green;
}
```

![image-20210912230426222](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912230426222.png)

**<font color=red>通用兄弟选择器</font>**

```css
.active~p{  
    background: yellow;  
}
```

![image-20210912230521585](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912230521585.png)

### **2、结构伪类选择器**

![image-20210912230546284](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912230546284.png)

```html
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>使用CSS3结构伪类选择器</title>
</head>
<body>
    <p>p1</p>
    <p>p2</p>
    <p>p3</p>
    <ul>
        <li>li1</li>
        <li>li2</li>
        <li>li3</li>
    </ul>
</body>
</html>
```

标红的重点强调下，其他的可以了解

![image-20210912230643292](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912230643292.png)

```css
ul li:first-child{ background: red;}
ul li:last-child{ background: green;}
p:nth-child(1){ background: yellow;}
p:nth-of-type(2){ background: blue;}
```

![image-20210912230725515](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912230725515.png)

**小结**

- 使用E F:nth-child(n)和E F:nth-of-type(n)的 关键点

  - E F:nth-child(n)在父级里从一个元素开始查找，不分类型
  - E F:nth-of-type(n)在父级里先看类型，再看位置

  

### **3、属性选择器**

![image-20210912230829690](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912230829690.png)

**E[attr]属性选择器**

```css
a[id] { 
    background: yellow; 
}
```

![image-20210912230904769](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912230904769.png)

**E[attr=val]属性选择器**

```css
a[id=first] { 
    background: red; 
}
```

![image-20210912230934955](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912230934955.png)

**E[attr*=val]属性选择器**

```css
a[class*=links] { 
    background: red; 
}
```

![image-20210912231006638](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912231006638.png)

**E[attr^=val]属性选择器**

```css
a[href^=http] { 
    background: red; 
}
```

![image-20210912231036102](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912231036102.png)

**E[attr$=val]属性选择器**

```css
a[href$=png] {
    background: red;
}
```

![image-20210912231106136](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912231106136.png)

# 2、美化网页元素

## 2.1、字体样式

![image-20210912231350189](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912231350189.png)

**<font color=red>字体类型 font-family</font>**

```css
p{font-family:Verdana,"楷体";}
body{font-family: Times,"Times New Roman", "楷体";}
```

同时设置中文和英文时，计算机如何识别中英文不同类型

<img src="http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912231441640.png" alt="image-20210912231441640" style="zoom: 67%;" />

**<font color=red>字体大小 font-size</font>**

- 单位
  - px（像素）
  - em、rem、cm、mm、pt、pc

```css
h1{font-size:24px;}
h2{font-size:16px;}
h3{font-size:2em;}
span{font-size:12pt;}
strong{font-size:13pc;}
```

<font color=red>字体风格 font-style</font>

normal、italic和oblique

![image-20210912231631858](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912231631858.png)

<font color=red>字体的粗细 font-weight</font>

![image-20210912231705588](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912231705588.png)

<font color=red>字体属性 font</font>

字体属性的顺序：字体风格→字体粗细→字体大小→字体类型

```css
p span{
    font:oblique bold 12px "楷体";
}
```

## 2.2、文本样式

![image-20210912231834210](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912231834210.png)

<font color=red>文本颜色color</font>

- RGB

  - 十六进制方法表示颜色：前两位表示红色分量，中间两位表示绿色分量，最后两位表示蓝色分量 

    rgb(r,g,b) : 正整数的取值为0～255

- RGBA

  - 在RGB基础上增加了控制alpha透明度的参数，其中这个透明通道值为0～1

```css
color:#A983D8;
color:#EEFF66;
color:rgb(0,255,255);
color:rgba(0,0,255,0.5);
```

<font color=red>排版文本段落</font>

水平对齐方式：text-align属性

![image-20210912232030552](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912232030552.png)

首行缩进：text-indent：em或px 

行高：line-height：px



<font color=red>文本修饰和垂直对齐</font>

文本装饰：text-decoration属性（后面的讲解中会大量用到）

![image-20210912232115700](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912232115700.png)

垂直对齐方式：vertical-align属性：middle、top、bottom

## 2.3、文本阴影 

![image-20210912232150856](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912232150856.png)

text-shadow属性在CSS2.0中出现，但迟迟未被各大浏览器所支持，因此在CSS2.1中被废弃，如今在CSS3中得到了各大浏览器的支持！

## 2.4、超链接伪类

![image-20210912232221736](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912232221736.png)

**使用CSS设置超链接**

![image-20210912232243574](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912232243574.png)

实际网页开发中通常只设置两种状态，一是a{color:#333;}，一是a:hover {  color:#B46210;}

## 2.5、列表样式  

- list-style-type
- list-style-image
- list-style-position
- list-style

![image-20210912232323893](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912232323893.png)

上网时大家都会看到在浏览的网页中用到列表时很少使用CSS自带的列表标记，而是使用设计的图标，那么大家会想使用list-style-image就可以了。可是list-style-position不能准确地定位图像标记的位置，通常，网页中图标的位置都是非常精确的。在实际的网页制作中，通常使用list-style或list-style-type设置项目无标记符号，然后通过背景图像的方式把设计的图标设置成列表项标记。在网页制作中，list-style和list-style-type两个属性是大家经常用到的，而另两个属性则不太常用，因此在这里大家牢记list-style和list-style-type的用法即可！

![image-20210912232344639](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912232344639.png)

## 2.6、背景样式  

**常见的背景样式**

- 背景图像
  - background-image
- 背景颜色
  - background-color

<font color=red>设置背景图像</font>

background-image属性

background-repeat属性

![image-20210912232523986](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912232523986.png)

**<font color=red>背景定位：background-position属性</font>**

![image-20210912232557210](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912232557210.png)

这些小三角，实现定位

<img src="http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912232618128.png" alt="image-20210912232618128" style="zoom: 67%;" />

**<font color=red>设置背景</font>**

背景属性：background属性（背景样式简写）

```css
.title {
    font-size:18px;
    font-weight:bold;
    color:#FFF;
    text-indent:1em;
    line-height:35px;
    background:#C00 url(../image/arrow-down.gif) 205px 10px no-repeat;
}
background: 背景颜色  背景图像  背景定位  背景不重复显示
```

<font color=red>背景尺寸 background-size</font>

![image-20210912232820794](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912232820794.png)

## 2.7、CSS渐变样式

网站推荐：http://color.oulu.me/

- **线性渐变**
  - 颜色沿着一条直线过渡：从左到右、从右到左、从上到下等
- **径向渐变**
  - 圆形或椭圆形渐变，颜色不再沿着一条直线变化，而是从一个起点朝所有方向混合

<font color=red>CSS3渐变兼容</font>

- IE浏览器是Trident内核，加前缀：-ms-
- Chrome浏览器是Webkit内核，加前缀：-webkit-
- Safari浏览器是Webkit内核，加前缀：-webkit-
- Opera浏览器是Blink内核，加前缀：-o-
- Firefox浏览器是Mozilla内核，加前缀：-moz-

<font color=red>线性渐变</font>

![image-20210912233037260](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912233037260.png)

# 3、盒子模型 

## 3.1、什么是盒子模型  

讲解盒子模型及属性，并说明边框、外边框和内边框都是四个边，最后介绍盒子模型的立体结构

![image-20210912233223025](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912233223025.png)

## 3.2、边框

<font color=red>边框颜色 border-color</font>

- 边框颜色设置方式与文本颜色对比讲解，都是使用十六进制
- 强调同时设置4个边框颜色时，顺序为上右下左
- 详细讲解分别上、下、左、右各边框颜色的不同设置方式，及属性值的顺序

![image-20210912233337139](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912233337139.png)

<font color=red>边框粗细 border-width</font>

- thin
- medium
- thick
- 像素值

```css
border-top-width:5px; 
border-right-width:10px; 
border-bottom-width:8px; 
border-left-width:22px; 
border-width:5px ; 
border-width:20px 2px;
border-width:5px 1px 6px;
border-width:1px 3px 5px 2px;
```

<font color=red>边框样式 border-style</font>

- none
- hidden
- dotted
- dashed
- solid
- double

```css
border-top-style:solid; 
border-right-style:solid; 
border-bottom-style:solid; 
border-left-style:solid; 
border-style:solid ; 
border-style:solid dotted;
border-style:solid dotted dashed;
border-style:solid dotted dashed double;
```

<font color=red>border简写</font>

同时设置边框的颜色、粗细和样式

```css
border:1px solid #3a6587;
border: 1px dashed red;
```

![image-20210912235448545](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912235448545.png)

## 3.3、内外边距 

<font color=red>外边距 margin</font>

- margin-top
- margin-right
- margin-bottom
- margin-left

```css
margin-top: 1 px
margin-right : 2 px
margin-bottom : 2 px
margin-left : 1 px
margin :3px 5px 7px 4px;
margin :3px 5px;
margin :3px 5px 7px;
margin :8px;
```

<font color=red>外边距的妙用：网页居中对齐</font>

```css
margin:0px  auto;
```

**网页居中对齐的必要条件**

- 块元素
- 固定宽度

<font color=red>内边距 padding</font>

- padding-left 
- padding-right
- padding-top
- padding-bottom

```css
padding-left:10px; 
padding-right: 5px; 
padding-top: 20px; 
padding-bottom:8px; 
padding:20px 5px 8px 10px ; 
padding:10px 5px; 
padding:30px 8px 10px ; 
padding:10px;
```

## 3.4、盒子型模尺寸

![image-20210912235750409](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912235750409.png)

**<font color=red>box-sizing</font>**

![image-20210912235855986](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912235855986.png)

```html
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>box-sizing</title>
    <style>
        div{
            width: 100px;
            height: 100px;
            padding: 5px;
            margin: 10px;
            border: 1px solid #000000;
            box-sizing: border-box;
            /*box-sizing: content-box;  /!* 默认值*!/*/
        }
    </style>
</head>
<body>
    <div></div>
</body>
</html>
```

## 3.5、圆角边框 

```css
border-radius: 20px 10px 50px 30px;
```

**四个属性值按顺时针排列**

![image-20210912235951177](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210912235951177.png)

**<font color=red>border-radius制作特殊图形：圆形</font>**

利用border-radius属性制作圆形的两个要点

- 元素的宽度和高度必须相同
- 圆角的半径为元素宽度的一半，或者直接设置圆角半径值为50%

```css
div{
    width: 100px;
    height: 100px;
    border: 4px solid red;
    border-radius: 50%;
}
```

![image-20210913000045828](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210913000045828.png)

```html
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>border-radius制作圆形</title>
    <style>
        div{
            width: 100px;
            height: 100px;
            border: 4px solid red;
            border-radius: 50%;
        }
    </style>
</head>
<body>
    <div></div>
</body>
</html>
```

**<font color=red>使用border-radius制作特殊图形：半圆形</font>**

利用border-radius属性制作半圆形的两个要点

- 制作上半圆或下半圆时，元素的宽度是高度的2倍，而且圆角半径为元素的高度值
- 制作左半圆或右半圆时，元素的高度是宽度的2倍，而且圆角半径为元素的宽度值

<img src="http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210913000156934.png" alt="image-20210913000156934" style="zoom:67%;" />

```html
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>border-radius制作半圆形</title>
    <style>
        div{
            background: red;
            margin: 30px;
        }
        div:nth-of-type(1){
            width: 100px;
            height: 50px;
            border-radius: 50px 50px 0 0;
        }
        div:nth-of-type(2){
            width: 100px;
            height: 50px;
            border-radius:0 0 50px 50px;
        }
        div:nth-of-type(3){
            width: 50px;
            height: 100px;
            border-radius:0 50px 50px  0;
        }
        div:nth-of-type(4){
            width: 50px;
            height: 100px;
            border-radius: 50px 0 0 50px;
        }
    </style>
</head>
<body>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
</body>
</html>
```

**<font color=red>使用border-radius制作特殊图形：扇形</font>**

利用border-radius属性制作扇形遵循“三同，一不同”原则

- “三同”是元素宽度、高度、圆角半径相同
- “一不同”是圆角取值位置不同

<img src="C:\Users\航\AppData\Roaming\Typora\typora-user-images\image-20210913000308701.png" alt="image-20210913000308701" style="zoom:67%;" />

```html
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>border-radius制作扇形</title>
    <style>
        div{
            background: red;
            margin: 30px;
        }
        div:nth-of-type(1){
            width: 50px;
            height: 50px;
            border-radius: 50px 0 0 0;
        }
        div:nth-of-type(2){
            width: 50px;
            height: 50px;
            border-radius:0 50px 0 0;
        }
        div:nth-of-type(3){
            width: 50px;
            height: 50px;
            border-radius:0 0 50px 0;
        }
        div:nth-of-type(4){
            width: 50px;
            height: 50px;
            border-radius: 0 0 0 50px;
        }
    </style>
</head>
<body>
    <div></div>
    <div></div>
    <div></div>
    <div></div>
</body>
</html>
```

## 3.6、盒子阴影 

![image-20210913000359226](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210913000359226.png)

```html
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>box-shadow的使用</title>
    <style>
        div{
            width: 100px;
            height: 100px;
            border: 1px solid red;
            border-radius: 8px;
            margin: 20px;
            /*box-shadow: 20px 10px 10px #06c;      /!*内阴影*!/*/
            /*box-shadow: 0px 0px 20px #06c;          /!*只设置模糊半径的阴影*!/*/
            box-shadow: inset 3px 3px 10px #06c;   /*内阴影*/
        }
    </style>
</head>
<body>
<div></div>
</body>
</html>
```

# 4、浮动

## 4.1、标准文档流  

标准文档流：指元素根据块元素或行内元素的特性按从上到下，从左到右的方式自然排列。这也是元素默认的排列方式

**标准文档流组成**

- 块级元素（block）

```css
<h1>...<h6>、<p>、<div>、列表
```

- 内联元素（inline）

```css
<span>、<a>、<img/>、<strong>...
```

内联标签可以包含于块级标签中，成为它的子元素，而反过来则不成立

## 4.2、display 

**<font color=red>display属性</font>**

![image-20210913000633874](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210913000633874.png)

```css
display:block;
```

![image-20210913000657849](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210913000657849.png)

```css
display:inline;
```

```css
display:inline-block;
```

![image-20210913000738714](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210913000738714.png)

```css
display:none;
```

![image-20210913000801615](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210913000801615.png)

<font color=red>display特性</font>

- 块级元素与行级元素的转变（block、inline）
- 控制块元素排到一行（inline-block）
- 控制元素的显示和隐藏（none）

## 4.3、浮动 

<font color=red>float属性</font>

![image-20210913000907498](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210913000907498.png)

```html
<body>
    <div id="father">
        <div class="layer01"><img src="image/photo-1.jpg" alt="日用品" />
</div>
        <div class="layer02"><img src="image/photo-2.jpg" alt="图书" /></div>
        <div class="layer03"><img src="image/photo-3.jpg" alt="鞋子" /></div>
        <div class="layer04">浮动的盒子......</div>
    </div>
</body>
```

<font color=red>左浮动</font>

```css
.layer01 {
    border:1px #F00 dashed;
    float:left;
}
.layer02 {
    border:1px #00F dashed;
    float:left;
}
.layer03 {
    border:1px #060 dashed;
    float:left;
}
```

<font color=red>右浮动</font>

```css
.layer01 {
    border:1px #F00 dashed;
    float:right;
}
.layer02 {
    border:1px #00F dashed;
    float:right;
}
```

## 4.4、边框塌陷  

layer04设置宽度和右浮动后，为什么边框塌陷了？怎么解决？

- 浮动元素脱离标准文档流
- 清除浮动

**<font color=red>clear属性</font>**

![image-20210913001106928](C:\Users\航\AppData\Roaming\Typora\typora-user-images\image-20210913001106928.png)

```css
.layer04 {
    clear:both; #清除两侧浮动
}
.layer04 {
    clear:left; #清除左侧浮动
}
.layer04 {
    clear:right;  #清除右侧浮动
}
```

**<font color=red>解决父级边框塌陷的方法</font>**

clear属性可以清除浮动对其他元素造成的影响，可是依然解决不了父级边框塌陷问题，怎么办？

- 浮动元素后面加空div

```html
<div id="father">
  <div class="layer01"><img src="image/photo-1.jpg" alt="日用品" /></div>
  <div class="layer02"><img src="image/photo-2.jpg" alt="图书" /></div>
  <div class="layer03"><img src="image/photo-3.jpg" alt="鞋子" /></div>
  <div class="layer04">浮动的盒子......</div>
  <div class="clear"></div>
</div>
.clear{  clear: both;  margin: 0; padding: 0;}
```

- 设置父元素的高度

```html
<div id="father">
  <div class="layer01"><img src="image/photo-1.jpg" alt="日用品" /></div>
  <div class="layer02"><img src="image/photo-2.jpg" alt="图书" /></div>
  <div class="layer03"><img src="image/photo-3.jpg" alt="鞋子" /></div>
  <div class="layer04">浮动的盒子......</div>
</div>
#father {height: 400px; border:1px #000 solid; }
```

- 父级添加overflow属性（溢出处理）

![image-20210913001333835](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210913001333835.png)

hidden属性值，这个值在网页中经常使用，通常与< div>宽度结合使用设置< div>自动扩展高度，或者隐藏超出的内容

```html
<div id="father">
  <div class="layer01"><img src="image/photo-1.jpg" alt="日用品" /></div>
  <div class="layer02"><img src="image/photo-2.jpg" alt="图书" /></div>
  <div class="layer03"><img src="image/photo-3.jpg" alt="鞋子" /></div>
  <div class="layer04">浮动的盒子......</div>
</div>
#father {overflow: hidden;border:1px #000 solid; }
```

- 父级添加伪类after

```html
<div id="father" class="clear">
  <div class="layer01"><img src="image/photo-1.jpg" alt="日用品" /></div>
  <div class="layer02"><img src="image/photo-2.jpg" alt="图书" /></div>
  <div class="layer03"><img src="image/photo-3.jpg" alt="鞋子" /></div>
  <div class="layer04">浮动的盒子......</div>
</div>
.clear:after{
    content: '';          /*在clear类后面添加内容为空*/
    display: block;      /*把添加的内容转化为块元素*/
    clear: both;         /*清除这个元素两边的浮动*/
}
```

<font color=red>小结</font>

【清除浮动，防止父级边框塌陷的四种方法】

- 浮动元素后面加空div
  - 简单，空div会造成HTML代码冗余
- 设置父元素的高度
  - 简单，元素固定高会降低扩展性
- 父级添加overflow属性
  - 简单，下拉列表框的场景不能用
- 父级添加伪类after
  - 写法比上面稍微复杂一点，但是没有副作用，推荐使用

## 4.5、inline-block和float区别 

- display:inline-block
  - 可以让元素排在一行，并且支持宽度和高度，代码实现起来方便
  - 位置方向不可控制，会解析空格
  - IE 6、IE 7上不支持
- float 
  - 可以让元素排在一行并且支持宽度和高度，可以决定排列方向
  - float 浮动以后元素脱离文档流，会对周围元素产生影响，必须在它的父级上添加清除浮动的样式

# 5、定位 

## 5.1、定位在网页中的应用 

![image-20210913001639588](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210913001639588.png)

![image-20210913001656853](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210913001656853.png)

![image-20210913001708514](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210913001708514.png)

## 5.2、相对定位

position属性

<font color=red>static：默认值，没有定位</font>

![image-20210913001743435](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210913001743435.png)

<font color=red>relative：相对定位</font>

相对自身原来位置进行偏移，偏移设置：top、left、right、bottom

![image-20210913001818869](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210913001818869.png)

**相对定位元素的规律**

设置相对定位的盒子会相对它原来的位置，通过指定偏移，到达新的位置 

设置相对定位的盒子仍在标准文档流中，它对父级盒子和相邻的盒子都没有任何影响 

设置相对定位的盒子原来的位置会被保留下来

**设置第二个盒子右浮动，再设置第一、第二盒子相对定位**

![image-20210913001850757](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210913001850757.png)

```css
#first {
    background-color:#FC9;
    border:1px #B55A00 dashed;
    position:relative;
    right:20px;
    bottom:20px;
}
#second {
    background-color:#CCF;
    border:1px #0000A8 dashed;
    float:right;
    position:relative;
    left:20px;
    top:-20px;
}
```

![image-20210913001916710](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210913001916710.png)

## 5.3、绝对定位  

absolute属性值：偏移设置： left、right、top、bottom 

绝对定位：

- 使用了绝对定位的元素以它最近的一个“已经定位”的“祖先元素” 为基准进行偏移
- 如果没有已经定位的祖先元素，会以浏览器窗口为基准进行定位
- 绝对定位的元素从标准文档流中脱离，这意味着它们对其他元素的定位不会造成影响
- 元素位置发生偏移后，它原来的位置不会被保留下来

![image-20210913001949493](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210913001949493.png)

> 设置了绝对定位但没有设置偏移量的元素将保持在原来的位置。
> 在网页制作中可以用于需要使某个元素脱离标准流，而仍然希望它保持在原来的位置的情况

## 5.4、固定定位  

**fixed属性值**

偏移设置： left、right、top、bottom

类似绝对定位，不过区别在于定位的基准不是祖先元素，而是浏览器窗口

![image-20210913002051879](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210913002051879.png)

![image-20210913002100036](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210913002100036.png)

## 5.5、定位小结  

**相对定位**

- 相对定位的特性
  - 相对于自己的初始位置来定位
  - 元素位置发生偏移后，它原来的位置会被保留下来
  - 层级提高，可以把标准文档流中的元素及浮动元素盖在下边
- 相对定位的使用场景
  - 相对定位一般情况下很少自己单独使用，都是配合绝对定位使用，为绝对定位创造定位父级而又不设置偏移量

**绝对定位**

- 绝对定位的特性
  - 绝对定位是相对于它的定位父级的位置来定位，如果没有设置定位父级，则相对浏览器窗口来定位
  - 元素位置发生偏移后，原来的位置不会被保留
  - 层级提高，可以把标准文档流中的元素及浮动元素盖在下边
  - 设置绝对定位的元素脱离文档流
- 绝对定位的使用场景
  - 一般情况下，绝对定位用在下拉菜单、焦点图轮播、弹出数字气泡、特别花边等场景

固定定位

- 固定定位的特性
  - 相对浏览器窗口来定位
  - 偏移量不会随滚动条的移动而移动
- 固定定位的使用场景
  - 一般在网页中被用在窗口左右两边的固定广告、返回顶部图标、吸顶导航栏等

5.6、z-index属性  

- 调整元素定位时重叠层的上下位置
  - z-index属性值：整数，默认值为0
  - 设置了positon属性时，z-index属性可以设置各元素之间的重叠高低关系
  - z-index值大的层位于其值小的层上方

![image-20210913002335872](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210913002335872.png)

**网页元素透明度**

![image-20210913002359253](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/HTML/image-20210913002359253.png)

<font color=red>小结</font>

网页中的元素都含有两个堆叠层级

未设置绝对定位时所处的环境，z-index是0

设置绝对定位时所处的堆叠环境，此时层的位置由z-index的值确定

改变设置绝对定位和没有设置绝对定位的层的上下堆叠顺序，只需调整绝对定位层的z-index值即可