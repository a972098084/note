# 邮件任务 

邮件发送，在我们的日常开发中，也非常的多，Springboot也帮我们做了支持

- 邮件发送需要引入spring-boot-start-mail
- SpringBoot 自动配置MailSenderAutoConfiguration
- 定义MailProperties内容，配置在application.yml中
- 自动装配JavaMailSender
- 测试邮件发送

演示

1. 引入pom依赖

   ```xml
   <dependency>
       <groupId>org.springframework.boot</groupId>
       <artifactId>spring-boot-starter-mail</artifactId>
   </dependency>
   ```

   看它引入的依赖，可以看到 jakarta.mail

   ```xml
   <dependency>
     <groupId>com.sun.mail</groupId>
     <artifactId>jakarta.mail</artifactId>
     <version>1.6.7</version>
     <scope>compile</scope>
   </dependency>
   ```

2. 查看自动配置类：MailSenderAutoConfiguration

   ![image-20210824164907984](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/SpringBoot/image-20210824164907984.png)

   这个类中存在bean，JavaMailSenderImpl

   ![image-20210824165008662](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/SpringBoot/image-20210824165008662.png)

   然后我们去看下配置文件

   ```java
   @ConfigurationProperties(
       prefix = "spring.mail"
   )
   public class MailProperties {
       private static final Charset DEFAULT_CHARSET;
       private String host;
       private Integer port;
       private String username;
       private String password;
       private String protocol = "smtp";
       private Charset defaultEncoding;
       private Map<String, String> properties;
       private String jndiName;
   }
   ```

3. 配置文件：

   ```properties
   spring.mail.username=972098084@qq.com
   spring.mail.password= **************
   spring.mail.host=smtp.qq.com
   # qq需要配置ssl
   spring.mail.properties.mail.smtp.ssl.enable=true
   ```

4. Spring单元测试

   ```java
   @SpringBootTest
   class SwaggerApplicationTests {
   
       @Autowired
       JavaMailSenderImpl mailSender;
       @Test
       public void contextLoads() {
           //邮件设置1：一个简单的邮件
           SimpleMailMessage message = new SimpleMailMessage();
           message.setSubject("通知");
           message.setText("你好呀");
           message.setTo("972098084@qq.com");
           message.setFrom("972098084@qq.com");
           mailSender.send(message);
       }
       @Test
       public void contextLoads2() throws MessagingException {
           //邮件设置2：一个复杂的邮件
           MimeMessage mimeMessage = mailSender.createMimeMessage();
           MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
           helper.setSubject("通知");
           helper.setText("<b style='color:red'>你好呀</b>",true);
           //发送附件
           helper.addAttachment("1.jpg",new File(""));
           helper.addAttachment("2.jpg",new File(""));
           helper.setTo("972098084@qq.com");
           helper.setFrom("972098084@qq.com");
           mailSender.send(mimeMessage);
       }
   }
   ```

