# 定时任务

项目开发中经常需要执行一些定时任务，比如需要在每天凌晨的时候，分析一次前一天的日志信息，Spring为我们提供了异步执行任务调

度的方式，提供了两个接口。

- TaskExecutor接口
- TaskScheduler接口

两个注解：

- @EnableScheduling
- @Scheduled

**cron表达式**

![image-20210824163221442](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/SpringBoot/image-20210824163221442.png) 

![image-20210824163241343](http://edu-hanghang.oss-cn-beijing.aliyuncs.com/SpringBoot/image-20210824163241343.png) 

1. 创建一个ScheduledService

   我们里面存在一个hello方法，他需要定时执行，怎么处理呢？

   ```java
   @Service
   public class ScheduledService {
   
       //秒    分    时     日    月    周几
       //0 * * * * MON-FRI
       //注意cron表达式的用法；
       @Scheduled(cron = "0 * * * * 0-7")
       public void hello(){
           System.out.println("hello.....");
       }
   }
   ```

2. 这里写完定时任务之后，我们需要在主程序上增加@EnableScheduling 开启定时任务功能

   ```java
   @SpringBootApplication
   @EnableAsync //开启异步注解功能
   @EnableScheduling //开启基于注解的定时任务
   public class SwaggerApplication {
   
       public static void main(String[] args) {
           SpringApplication.run(SwaggerApplication.class, args);
       }
   
   }
   ```

3. 我们来详细了解下cron表达式；
   http://www.bejson.com/othertools/cron/