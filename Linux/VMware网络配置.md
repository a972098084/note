VMware网络配置

方案一

1、设置你需要的网段

<img src="C:\Users\航\AppData\Roaming\Typora\typora-user-images\image-20211108183706905.png" alt="image-20211108183706905" style="zoom:67%;" />

2、设置网关地址

![image-20211108183855719](C:\Users\航\AppData\Roaming\Typora\typora-user-images\image-20211108183855719.png)

3、在Linux中修改参数/etc/sysconfig/network-scripts/ifcfg-ens33，lo为本地为本地回环，相当于127.0.0.1，可以忽略

![image-20211108184059392](C:\Users\航\AppData\Roaming\Typora\typora-user-images\image-20211108184059392.png)

<img src="C:\Users\航\AppData\Roaming\Typora\typora-user-images\image-20211108184435650.png" alt="image-20211108184435650" style="zoom:67%;" />

修改红色框中内容（注：如果配置了DNS1，则会自动同步到/etc/resolv.conf文件中，但是配置了GATEWAY网关配置后，不会同步到/etc/sysconfig/network中）

![image-20211108184816901](C:\Users\航\AppData\Roaming\Typora\typora-user-images\image-20211108184816901.png)

在适配器中更改和虚拟器相同ip等内容





修改网关 vi /etc/sysconfig/network，如果/etc/sysconfig/network-scripts/ifcfg-ens33中配置了，以/etc/sysconfig/network-scripts/ifcfg-ens33为准，如果没有配置，则以他为准。

修改DNS  vi /etc/resolv.conf，如果/etc/sysconfig/network-scripts/ifcfg-ens33中配置过，/etc/sysconfig/network-scripts/ifcfg-ens33中配置的内容会自动同步过来。DNS可以选择公用的解析服务，如8.8.8.8等，也可以设置为网关地址



方案二（最简单）

修改网卡配置文件 vi /etc/sysconfig/network-scripts/ifcfg-ens32  (最后一个为网卡名称)

（1）bootproto=static

（2）onboot=yes

（3）在最后加上几行，IP地址、子网掩码、网关、dns服务器

```
IPADDR=192.168.88.100
NETMASK=255.255.255.0
GATEWAY=192.168.88.2
DNS1=192.168.88.2
```

（4）重启网络服务

```
systemctl restart network
```
