本文主要介绍Filebeat7.16版本中Log相关配置以及实际应用场景

一般情况下，我们使用log input配置如下，只需要填写一系列paths即可

```yaml
filebeat.inputs:
- type: log
  paths:
    - /var/log/messages
    - /var/log/*.log
```

但其实除了基本的paths配置外，我们还需要大概十几个配置都需要我们关注。

这些配置或多或少影响Filebeat的使用方式以及性能，虽然使用默认值以及基本满足日常所需，但是只有在深刻理解相关配置的真正含义，才能在不同使用环境下做到完全把控。

### **log input 配置**

**paths**

指定一系列的paths作为信息输入来源，在指定path时需要注意以下规则：

1. 指定的路径必须是文件，不能是目录。
2. 支持Glob模式。
3. 默认支持递归路径，如`/**/`形式，Filebeat将会展开8层嵌套目录。

**Glob模式**

Glob模式支持通配符匹配，目前支持的语法有：

| 通配符 | 解释                   | 实例      | 匹配                                |
| ------ | ---------------------- | --------- | ----------------------------------- |
| *      | 匹配任意数目的字符     | Ag*       | Agree, Against, .....               |
| ？     | 匹配任意的单个字符     | Agre?     | Agrea,Agreb,Agrec,Agred,Agree,..... |
| [abc]  | 匹配一个在括号中的字符 | Agre[abc] | Agrea, Agreb, Agrec                 |
| [a-z]  | 匹配一个指定范围的字符 | Agre[a-z] | Agrea,Agreb,......,Agrez            |

**递归的Glob模式**

filebeat对传统的Glob模式进行了扩展，支持用户指定`/**/`模式的路径，filebeat可以将其展开为8层的Glob路径。

例如，假如指定了`/home/data/**/*.log`, filebeat将会把`/**/`翻译成8层的子目录，如下：

```
/home/data/*.log
/home/data/*/*.log
/home/data/*/*/*.log
/home/data/*/*/*/*.log
/home/data/*/*/*/*/*.log
/home/data/*/*/*/*/*/*.log
/home/data/*/*/*/*/*/*/*.log
/home/data/*/*/*/*/*/*/*/*.log
/home/data/*/*/*/*/*/*/*/*/*.log
```

加上不带子目录的Glob路径，一共会有8条Glob路径。这些路径都会作为input的输入源路径进行搜索。

在使用中需要注意以下几点：

1. filebeat展开为成8层子目录的规则，是直接hardcode在代码中的，无法通过配置修改匹配层数
2. 只支持单纯的`/**/`模式，对于`/data**/`模式不支持
3. 递归模式默认开启，可通过`recursive_glob.enabled`配置项关闭

**recursive_glob.enabled**

是否开启递归的Glob模式，默认是true。

**fields**

向输出的每一条日志添加额外的信息，比如“level:debug”，方便后续对日志进行分组统计。默认情况下，会在输出信息的fields子目录下以指定的新增fields建立子目录，例如fields.level

```yaml
    fields: 
      level: debug
```

![image-20220228170329568](https://edu-hanghang.oss-cn-beijing.aliyuncs.com/filebeat/setting/image-20220228170329568.png)

**fields_under_root**

如果该选项设置为true，则新增fields成为顶级目录，而不是将其放在fields目录下。自定义的field会覆盖filebeat默认的field。例如添加如下配置：

```yaml
fields: 
  level: debug 
fields_under_root: true
```
![image-20220228170352106](https://edu-hanghang.oss-cn-beijing.aliyuncs.com/filebeat/setting/image-20220228170352106.png)

**encoding**

指定被监控的文件的编码类型，使用plain和utf-8都是可以处理中文日志的。默认是plain。即ASCII模式。

**exclude_lines**

可指定多个正则表达式，来去除某些不需要上报的行。例如：

```yaml
filebeat.inputs:
- type: log
  ...
  exclude_lines: ['^DBG']
```

该配置将会排除以DBG开头的行

**include_lines**

可指定多个项正则表达式，来仅上报匹配的行。例如：

```yaml
filebeat.inputs:
- type: log
  ...
  include_lines: ['^ERR', '^WARN']
```

该配置将会仅上报以`ERR`和`WARN`开头的行。

问题来了，如果同时指定了exclude_lines和include_lines会怎么处理？

对于这种情况，Filebeat将会先校验include_lines，再校验exclude_lines，其代码实现如下：

```go
func (h *Harvester) shouldExportLine(line string) bool {
    if len(h.config.IncludeLines) > 0 {
        if !harvester.MatchAny(h.config.IncludeLines, line) {
            // drop line
            return false
        }
    }
    if len(h.config.ExcludeLines) > 0 {
        if harvester.MatchAny(h.config.ExcludeLines, line) {
            return false
        }
    }

    return true
}
```

**exclude_files**

可指定多个正则表达式，匹配到的文件名将不会被处理。

**harvester_buffer_size**

读文件时的buffer大小，最终会应用在golang的`File.Read`函数上面。

```go
func (f *File) Read(b []byte) (n int, err error)
```

默认是16384。即16k。

**max_bytes**

表示一条log消息的最大bytes数目。超过这个大小，剩余就会被截断。 默认值为10485760(即10MB)。

**multiline**

multiline是为了解决需要多行聚合在一起发送的情况，例如Java Stack Traces信息等。 虽然filebeat默认不开启multiline，但是官方的配置文件给了一个例子，可以支持Java Stack Traces或者是C语言式的换行连续符`\`, 可在[filebeat.reference.yml](https://link.zhihu.com/?target=https%3A//github.com/elastic/beats/blob/7.5/filebeat/filebeat.reference.yml%23L494)中查看。

由于大部分场景不涉及multiline，本文不再进行深入讨论。关于multiline配置的详细资料可查看官方文档： [Manage multiline messages](https://www.elastic.co/guide/en/beats/filebeat/7.16/multiline-examples.html)

**ignore_older**

ignore_older表示对于最近修改时间距离当前时间已经超过某个时长的文件，就暂时不进行处理。默认值为0，表示禁用该功能。

注意：ignore_older只是暂时不处理该文件，并不会在Registrar中改变该文件的状态。

其代码实现如下：

```go
func (p *Input) isIgnoreOlder(state file.State) bool {
    // ignore_older is disable
    if p.config.IgnoreOlder == 0 {
        return false
    }

    modTime := state.Fileinfo.ModTime()
    if time.Since(modTime) > p.config.IgnoreOlder {
        return true
    }

    return false
}
```

**close_* 系列**

log input中有一系列以close_开头配置，这些配置决定了Harvester何时结束对文件的读取。

1. close_eof： 如果读取到了EOF(即文件末尾)，是否要结束读取。如果为true，则读取到文件末尾就结束读取，否则Harvester将会继续工作。默认只为false。
2. close_inactive： 如果配置了close_eof为false，则Harvester即使读取到了文件末尾也不会终止。close_inactive决定了最长没有读到新消息的时长，默认为5m(即五分钟)。如果超过了close_inactive规定的时间依然没有新消息，则Harvester退出。
3. close_timeout<sup>[1]</sup>： 决定了一个Harvester的最长工作时间，如果Harvester工作了一段时间后依然没有停止，则强行停止Harvester。默认为0，表示不强行停止Harvester。
4. close_renamed： 文件更名时是否退出，默认为false。文件更名一般发生在日志轮替的场景下。
5. close_removed： 表示当文件被删除时Harvester是否要继续。默认为true。

不过即使Harvester关闭了也关系不大。因为根据filebeat会定时扫描文件，如果关闭后又有了新增内容，filebeat依然是可以检查出来的。

[1]：可能导致数据丢失，原因有以下两点：

1. 如将close_timeout大于等于ignore_older时，大概率会导致丢失数据，因为当close_timeout时间到时，ignore_older时间也到了，如果数据没有采集完成，且后面该文件没有更新时，这个文件里的剩余数据就会丢失，永远不会采集，如果要设置这个值，一定要保证他的值不大于ignore_older+scan_frequency。
2. 如果进行多行匹配模式时，如时间到达后还没有匹配到结束点，则会导致只会发送部分数据，如果再次开启 harvester时文件还存在，继续发送剩余部分内容。

**clean_* 系列**

clean_开头的一系列配置用来清理Registrar中的文件状态，同时也可以起到减小Registrar文件大小、防止inode复用等作用。

1. clean_inactive<sup>[1]</sup>： 表示一个时间段。用于移除已经一长段时间没有新产生内容的日志文件，默认为0，表示禁用该功能。

2. clean_removed：  在Registrar中移除那些已经不存在的文件。默认为true。

注：

[1]：此值必须大于ignore_older + scan_frequency

**scan_frequency**

代表input的扫描频率，默认为10s。 input会按照此频率，启动定时器定时扫描路径，以发现新文件和文件的改动情况。

**scan.sort和scan.order**

这两个配置项需要放在一起讲。 `scan.sort`可取的值为: modtime和filename。默认值为空，不进行排序。 `scan.order`可取的值为：asc和desc。默认值为asc。`scan.order`仅在`scan.sort`非空时生效。

需要注意的是：该功能目前为实验功能，可能会在以后版本移除。

**tail_files**

默认情况下，Harvester处理文件时，会文件头开始读取文件。开启此功能后，filebeat将直接会把文件的offset置到末尾，从文件末尾监听消息。默认值是false。

注意： 开启了tail_files, 则所有文件中的当前内容将不会被上报，只有新产生消息时才会上报。

在真实的实现中，tail_files被当做`ignore_older=1ns`处理。因此，在启动的时候，只要是新文件，里面的内容都会被忽略，直接把offset置为文件末尾。

所以使用该配置项时千万要谨慎！

**harvester_limit**

harvester_limit决定了一个input最多同时有多少个harvester启动。默认为0，代表不对harvester个数进行限制。 在使用时要注意两点：

1. 如果一个文件对应的harvester在本轮扫描时没能启动，那会在下次扫描时，有其他文件的harvester完全退出时，该文件的harvester才能启动。

2. harvester_limit仅对针对配置的input进行了限制，多个input之间的harvester_limit互不影响。

**symlinks**

代表是否要对符号链接进行处理，默认值为false，代表不处理。

**backoff相关配置**

我们上文讲到`close_eof`选项，当读取到eof时，且close_eof为false，则Harvester还会一直尝试读取文件。

在这种情况下，Harvester继续读取之前，其实filebeat还会等待一段时间。等待的时长就是由`backoff`、`backoff_factor`和`max_backoff`三个配置项共同决定。

对应的代码实现为：

```go
func (f *Log) wait() {
    // Wait before trying to read file again. File reached EOF.
    select {
    case <-f.done:
        return
    case <-time.After(f.backoff):
    }

    // Increment backoff up to maxBackoff
    if f.backoff < f.config.MaxBackoff {
        f.backoff = f.backoff * time.Duration(f.config.BackoffFactor)
        if f.backoff > f.config.MaxBackoff {
            f.backoff = f.config.MaxBackoff
        }
    }
}
```

其中，`backoff`默认值为1s, `backoff_factor`默认值为2，`max_backoff`默认值为10s。

该配置项意味着，如果读到EOF，则filebeat将会等待一段时间再去读文件。 等待时间开始为1s，如果一直是EOF，则会逐渐增大等待时间，每次的等待时间是前一次的两倍，且一次最长等待10s。

再结合`close_inactive`选项，如果等待时间超过了默认值5分钟，则Harvester结束。

此外，如果等待的时候文件又追加了新的数据，则backoff将会重新置为初始值。

### **全局配置**

除了log input相关的属性外，有一些全局属性也需要我们注意。

**queue相关配置**

filebeat会将event暂时存放在queue里面。filebeat的queue目前有mem和spool两种实现，默认是mem。 本文只介绍下mem的相关配置项。

```yaml
queue:
  mem:
    events: 4096
    flush.min_events: 2048
    flush.timeout: 1s
```

events代表queue最多能够承载的event的个数。如果个数达到最大值，则input将不能再向queue中插入数据，直至output将数据消费。

`flush.min_events`代表只有queue里面的数据到达了指定个数，才将数据发送给output。设为0代表直接发送给output，不进行等待。

`flush.timeout`代表定时刷新event到output中，即使其个数没有达到`flush.min_events`。该配置项只会在`flush.min_events`大于0时生效。

**registry相关配置**

1. filebeat.registry.path 定制registry文件的目录，默认值是`registry`。

   注意，这里指定的只是registry的目录，最终的registry文件的路径会是这样:

   ```
   ${filebeat.registry.path}/filebeat/data.json
   ```

2. filebeat.registry.flush 将registry文件内容定时刷新到磁盘中。默认为0s，代表每次更新时直接写文件。 配置了该选项可以提高些filebeat的性能，避免频繁写磁盘，但是也增加了一定数据丢失的风险。

**日志相关配置**

filebeat可以对输出日志的进行相关配置，filebeat提供了如下日志相关的配置:

```yaml
logging.level: info # 日志输出的最小级别
logging.selectors: [] # 过滤器，用户可在logp.NewLogger时指定
logging.to_stderr: false # 将日志输出到stderr
logging.to_syslog: false # 将日志输出到syslog (主要用于unix)
logging.to_eventlog: false # 将日志输出到windows的event log
logging.to_files: true # 将日志输出到文件中
logging.files:
	path: ${filebeat_bin_path}/logs/ # 日志目录
	name: filebeat  # 文件名 filebeat filebeat.1 filebeat.2
	rotateonstartup: true # 在filebeat启动时进行日志轮替
	rotateeverybytes: 10485760 # = 10MB 日志轮替的默认值
	keepfiles: 7 # 日志保留个数
	permissions: 0600 # 日志权限
	interval: 0 # 日志轮替
logging.metrics.enabled: true 
logging.metrics.period: 30s
```

filebeat可以选择将日志输出到许多地方，在线上运营时我们常常会将日志输出到文件, 所以接下来讲下文件相关的配置。

我们可以配置日志文件的所在目录以及文件名，分别对应logging.files.path和logging.files.name。
默认情况下，日志的输出目录是在filebeat的bin文件所在目录下的logs文件。

filebeat会进行日志轮替，一般情况下，常见的日志轮替规则有按大小和按时间，filebeat两种规则均支持。
其中:

1. rotateeverybytes决定了日志文件的最大值，如果日志文件超过了该值，将发生日志轮替，默认值为10MB。
2. rotateonstartup是说明是否在每次启动时都进行一次日志轮替，这样的话，每次启动的日志都会从一个新文件开始。默认为true

按文件大小进行轮替后，日志文件名将会变成filebeat、filebeat.1、filebeat.2这种格式，后缀越大文件越旧。

filebeat也支持按时间进行轮替，可以配置logging.files下的interval属性，支持按照秒、分钟、小时、周、月、年进行轮替，对应值为1s,1m, 1h, 24h, 7*24h, 30*24h, 和365*24h。当然，最小值是1s。

按照时间进行轮替时，时间将会以连字符进行分割, 例如：按照1小时进行轮替的话，文件格式为：filebeat-2019-11-28-15。filebeat目前还不支持日期格式的自定义。

同时，我们也可以指定日志的保留策略，目前只能通过设置keepfiles来决定保留日志的个数。

在日志里面还有logging.metrics相关配置，filebeat会定时输出一些当前的运行指标，例如输出下当前ack成功的数目、当前的内存占用情况等：
\- logging.metrics.enabled决定是否开启指标搜集
\- logging.metrics.period决定指标输出的间隔

