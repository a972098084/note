# 剑指offer

## 数组与矩阵

### [数组中重复的数字](https://cyc2018.xyz/算法/剑指 Offer 题解/3. 数组中重复的数字.html)

**题目描述**

在一个长度为 n 的数组里的所有数字都在 0 到 n-1 的范围内。数组中某些数字是重复的，但不知道有几个数字是重复的，也不知道每个数字重复几次。请找出数组中任意一个重复的数字。

```html
Input:
{2, 3, 1, 0, 2, 5}

Output:
2
```

**解题思路**

> 方法一	遍历数组

   * 创建一个空的set集合， 重复数字repeat = -1

   * 遍历数组中的每一个元素

        * 将集合加入到数组中，判断是否添加成功
        * 如果添加失败，则证明中有该元素，因此该元素重复，将该元素赋值给repeat，结束遍历

   * 返回repeat

     ```java
     public static int findRepeatNumber(int[] nums) {
             HashSet<Integer> set = new HashSet<>();
             int repeat = -1;
             for (int num : nums) {
                 if(!set.add(num)){
                     repeat = num;
                     break;
                 }
             }
             return repeat;
         }
     ```

> 方法二

* 数组中的元素在[0, n-1]范围内，可以把元素i放到第i个位置上，在调整中如果第i个位置已经有元素，则证明i值重复

* 例如(1, 4, 3, 4)，遍历到第四个位置是已经有值，则证明4重复

  ```java
  public int duplicate (int[] numbers) {
      // write code here
      int i = 0;
      while(i < numbers.length){
          if(numbers[i] == i){
              i++;
              continue;
          }
          if(numbers[numbers[i]] == numbers[i]) return numbers[i];
          int temp = numbers[i];
          numbers[i] = numbers[temp];
          numbers[temp] = temp;
      }
      return -1;
  }
  ```

### [二维数组中的查找](https://cyc2018.xyz/算法/剑指 Offer 题解/4. 二维数组中的查找.html)

**题目描述**

给定一个二维数组，其每一行从左到右递增排序，从上到下也是递增排序。给定一个数，判断这个数是否在该二维数组中。

```html
Consider the following matrix:
[
  [1,   4,  7, 11, 15],
  [2,   5,  8, 12, 19],
  [3,   6,  9, 16, 22],
  [10, 13, 14, 17, 24],
  [18, 21, 23, 26, 30]
]

Given target = 5, return true.
Given target = 20, return false.
```

**解题思路**

根据数组规律，小于他的一定在其左边，大于他的一定在其下边，每次就可以排除一行或者一列。

```java
public boolean Find(int target, int[][] matrix) {
    if (matrix == null || matrix.length == 0 || matrix[0].length == 0)
        return false;
    int rows = matrix.length, cols = matrix[0].length;
    int r = 0, c = cols - 1; // 从右上角开始
    while (r <= rows - 1 && c >= 0) {
        if (target == matrix[r][c])
            return true;
        else if (target > matrix[r][c])
            r++;
        else
            c--;
    }
    return false;
}
```

### [替换空格](https://cyc2018.xyz/算法/剑指 Offer 题解/5. 替换空格.html)

**题目描述**

将一个字符串中的空格替换成 "%20"。

```text
Input:
"A B"

Output:
"A%20B"
```

**解题思路**

> 方法一	调用api

```java
public String replaceSpace (String s) {
    if (s == null || "".equals(s))
        return s;
   return s.replaceAll(" ", "%20");
}
```

> 方法二	数组

申请临时数组，遍历字符串，如果不是空格把字符添加到数组中，如果是空格就添加三个字符"%", "2", "0"，最后把数组替换为字符串

```java
public String replaceSpace(String s) {
    int length = s.length();
    char[] array = new char[length * 3];
    int index = 0;
    for (int i = 0; i < length; i++) {
        char c = s.charAt(i);
        if (c == ' ') {
            array[index++] = '%';
            array[index++] = '2';
            array[index++] = '0';
        } else {
            array[index++] = c;
        }
    }
    String newStr = new String(array, 0, index);
    return newStr;
}
```

> 方法三	调用StringBuilder

    public String replaceSpace(String s) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == ' ')
                stringBuilder.append("%20");
            else
                stringBuilder.append(s.charAt(i));
        }
        return stringBuilder.toString();
    }

### [顺时针打印矩阵](https://cyc2018.xyz/算法/剑指 Offer 题解/29. 顺时针打印矩阵.html)

**题目描述**

按顺时针的方向，从外到里打印矩阵的值。下图的矩阵打印结果为：1, 2, 3, 4, 8, 12, 16, 15, 14, 13, 9, 5, 6, 7, 11, 10

![img](https://cs-notes-1256109796.cos.ap-guangzhou.myqcloud.com/image-20201104010349296.png)

**解题思路**

从里到外打印处理步骤相同，唯一不同的是上下左右的边界值。定义r1,r2,c1,c2分别代表上下左右的边界值，打印顺序为从左到右打印最上一行->从上到下打印最右一行->从右到左打印最下一行->从下到上打印最左一行。应当注意只有在 r1 != r2 时才打印最下一行，也就是在当前最外层的行数大于 1 时才打印最下一行，这是因为当前最外层只有一行时，继续打印最下一行，会导致重复打印。打印最左一行也要做同样处理。

```java
public ArrayList<Integer> printMatrix(int[][] matrix) {
    ArrayList<Integer> ret = new ArrayList<>();
    int r1 = 0, r2 = matrix.length - 1, c1 = 0, c2 = matrix[0].length - 1;
    while (r1 <= r2 && c1 <= c2) {
        // 上
        for (int i = c1; i <= c2; i++)
            ret.add(matrix[r1][i]);
        // 右
        for (int i = r1 + 1; i <= r2; i++)
            ret.add(matrix[i][c2]);
        if (r1 != r2)
            // 下
            for (int i = c2 - 1; i >= c1; i--)
                ret.add(matrix[r2][i]);
        if (c1 != c2)
            // 左
            for (int i = r2 - 1; i > r1; i--)
                ret.add(matrix[i][c1]);
        r1++; r2--; c1++; c2--;
    }
    return ret;
}
```

### [ 第一个只出现一次的字符位置](https://cyc2018.xyz/算法/剑指 Offer 题解/50. 第一个只出现一次的字符位置.html)

**题目描述**

在一个字符串中找到第一个只出现一次的字符，并返回它的位置。字符串只包含 ASCII 码字符。

```text
Input: abacc
Output: b
```

**解题思路**

最直观的解法是使用 HashMap 对出现次数进行统计：字符做为 key，出现次数作为 value，遍历字符串每次都将 key 对应的 value 加 1。最后再遍历这个 HashMap 就可以找出出现次数为 1 的字符。考虑到要统计的字符范围有限，也可以使用整型数组代替 HashMap。ASCII 码只有 128 个字符，因此可以使用长度为 128 的整型数组来存储每个字符出现的次数。

```java
public int FirstNotRepeatingChar(String str) {
    int[] cnts = new int[128];
    for (int i = 0; i < str.length(); i++)
        cnts[str.charAt(i)]++;
    for (int i = 0; i < str.length(); i++)
        if (cnts[str.charAt(i)] == 1)
            return i;
    return -1;
}
```

## 栈队列堆

### **[用两个栈实现队列](https://cyc2018.xyz/算法/剑指 Offer 题解/9. 用两个栈实现队列.html)**

**题目描述**

用两个栈来实现一个队列，完成队列的 Push 和 Pop 操作

**解题思路**

栈的入栈和出栈的数据是反向的，而队列的进入与出去顺序是正向的，如果先将数据入栈A，然后再将数据出栈入栈B，那么栈B的数据出栈后就是正向的。

```java
Stack<Integer> in = new Stack<Integer>();
Stack<Integer> out = new Stack<Integer>();

public void push(int node) {
    in.push(node);
}

public int pop() throws Exception {
    if (out.isEmpty())
        while (!in.isEmpty())
            out.push(in.pop());

    if (out.isEmpty())
        throw new Exception("queue is empty");

    return out.pop();
}
```

### [包含 min 函数的栈](https://cyc2018.xyz/算法/剑指 Offer 题解/30. 包含 min 函数的栈.html)

**题目描述**

实现一个包含 min() 函数的栈，该方法返回当前栈中最小的值

**解题思路**

使用一个额外的minStack栈，栈顶元素为当前栈的最小值。对dataStack进行入栈和出栈操作时，同时需要对minStack进行入栈出栈操作，从而使minStack栈顶元素为当前栈的最小值。进行push操作时，需要入栈当前元素和当前栈中的最小值，将值最小的元素入栈到minStack中

```java
private Stack<Integer> dataStack = new Stack<>();
private Stack<Integer> minStack = new Stack<>();

public void push(int node) {
    dataStack.push(node);
    minStack.push(minStack.isEmpty() ? node : Math.min(minStack.peek(), node));
}

public void pop() {
    dataStack.pop();
    minStack.pop();
}

public int top() {
    return dataStack.peek();
}

public int min() {
    return minStack.peek();
}
```

### [栈的压入、弹出序列](https://cyc2018.xyz/算法/剑指 Offer 题解/31. 栈的压入、弹出序列.html)

**题目描述**

输入两个整数序列，第一个序列表示栈的压入顺序，请判断第二个序列是否为该栈的弹出顺序。假设压入栈的所有数字均不相等。

例如序列 1,2,3,4,5 是某栈的压入顺序，序列 4,5,3,2,1 是该压栈序列对应的一个弹出序列，但 4,3,5,1,2 就不可能是该压栈序列的弹出序列

**解题思路**

每次入栈后，都要判断栈顶元素是不是为出栈序列popSequence的下标元素，如果是是的话进行出栈并将popSequence的下标后移一位，继续进行判断。

```java
public boolean IsPopOrder(int[] pushSequence, int[] popSequence) {
    int n = pushSequence.length;
    Stack<Integer> stack = new Stack<>();
    for (int pushIndex = 0, popIndex = 0; pushIndex < n; pushIndex++) {
        stack.push(pushSequence[pushIndex]);
        while (popIndex < n && !stack.isEmpty() 
                && stack.peek() == popSequence[popIndex]) {
            stack.pop();
            popIndex++;
        }
    }
    return stack.isEmpty();
}
```

### [最小的 K 个数](https://cyc2018.xyz/算法/剑指 Offer 题解/40. 最小的 K 个数.html)

**题目描述**

给定一个数组，找出其中最小的K个数。例如数组元素是4,5,1,6,2,7,3,8这8个数字，则最小的4个数字是1,2,3,4。

- 0 <= k <= input.length <= 10000
- 0 <= input[i] <= 10000

**解题思路**

对原数组从小到大排序后取出前 k*k* 个数即可。

```java
class Solution {
    public int[] getLeastNumbers(int[] arr, int k) {
        int[] vec = new int[k];
        Arrays.sort(arr);
        for (int i = 0; i < k; ++i) {
            vec[i] = arr[i];
        }
        return vec;
    }
}
```

### [数据流中的中位数](https://cyc2018.xyz/算法/剑指 Offer 题解/41.1 数据流中的中位数.html)

**题目描述**

如何得到一个数据流中的中位数？如果从数据流中读出奇数个数值，那么中位数就是所有数值排序之后位于中间的数值。如果从数据流中读出偶数个数值，那么中位数就是所有数值排序之后中间两个数的平均值

**解题思路**

```java
/* 大顶堆，存储左半边元素 */
private PriorityQueue<Integer> left = new PriorityQueue<>((o1, o2) -> o2 - o1);
/* 小顶堆，存储右半边元素，并且右半边元素都大于左半边 */
private PriorityQueue<Integer> right = new PriorityQueue<>();
/* 当前数据流读入的元素个数 */
private int N = 0;

public void Insert(Integer val) {
    /* 插入要保证两个堆存于平衡状态 */
    if (N % 2 == 0) {
        /* N 为偶数的情况下插入到右半边。
         * 因为右半边元素都要大于左半边，但是新插入的元素不一定比左半边元素来的大，
         * 因此需要先将元素插入左半边，然后利用左半边为大顶堆的特点，取出堆顶元素即为最大元素，此时插入右半边 */
        left.add(val);
        right.add(left.poll());
    } else {
        right.add(val);
        left.add(right.poll());
    }
    N++;
}

public Double GetMedian() {
    if (N % 2 == 0)
        return (left.peek() + right.peek()) / 2.0;
    else
        return (double) right.peek();
}
```

### [字符流中第一个不重复的字符](https://cyc2018.xyz/算法/剑指 Offer 题解/41.2 字符流中第一个不重复的字符.html)

**题目描述**

请实现一个函数用来找出字符流中第一个只出现一次的字符。例如，当从字符流中只读出前两个字符 "go" 时，第一个只出现一次的字符是 "g"。当从该字符流中读出前六个字符“google" 时，第一个只出现一次的字符是 "l"。

**解题思路**

使用统计数组来统计每个字符出现的次数，本题涉及到的字符为都为 ASCII 码，因此使用一个大小为 128 的整型数组就能完成次数统计任务。

使用队列来存储到达的字符，并在每次有新的字符从字符流到达时移除队列头部那些出现次数不再是一次的元素。因为队列是先进先出顺序，因此队列头部的元素为第一次只出现一次的字符。

```java
int[] nums = new int[128];
Queue<Character> queue = new LinkedList<>();
public void Insert(char ch)
{
    nums[ch]++;
    queue.add(ch);
    while(!queue.isEmpty() && nums[queue.peek()]>1)
        queue.poll();

}
//return the first appearence once char in current stringstream
public char FirstAppearingOnce()
{
    return queue.isEmpty() ? '#' : queue.peek();
}
```

### [滑动窗口的最大值](https://cyc2018.xyz/算法/剑指 Offer 题解/59. 滑动窗口的最大值.html)

**题目描述**

给定一个数组和滑动窗口的大小，找出所有滑动窗口里数值的最大值。

例如，如果输入数组 {2, 3, 4, 2, 6, 2, 5, 1} 及滑动窗口的大小 3，那么一共存在 6 个滑动窗口，他们的最大值分别为 {4, 4, 6, 6, 6, 5}。

![img](https://cs-notes-1256109796.cos.ap-guangzhou.myqcloud.com/image-20201104020702453.png)

**解题思路**

维护一个大小为窗口大小的大顶堆，顶堆元素则为当前窗口的最大值。

假设窗口的大小为 M，数组的长度为 N。在窗口向右移动时，需要先在堆中删除离开窗口的元素，并将新到达的元素添加到堆中，这两个操作的时间复杂度都为 log2M，因此算法的时间复杂度为 O(Nlog2M)，空间复杂度为 O(M)。

```java
 public ArrayList<Integer> maxInWindows(int [] num, int size) {
        ArrayList<Integer> lists = new ArrayList<>();
        if(size > num.length || size < 1)
            return lists;
        PriorityQueue<Integer> heap = new PriorityQueue<>((o1, o2) -> o2 -o1);
        for(int i=0; i<size; i++){
            heap.add(num[i]);
        }
        lists.add(heap.peek());
        for(int i = 0, j = i + size; j < num.length; i++, j++){
            heap.remove(num[i]);
            heap.add(num[j]);
            lists.add(heap.peek());
        }
        return lists;
    }
```

## 双指针

### [和为 S 的两个数字](https://cyc2018.xyz/算法/剑指 Offer 题解/57.1 和为 S 的两个数字.html)

**题目描述**

在有序数组中找出两个数，使得和为给定的数 S。如果有多对数字的和等于 S，输出两个数的乘积最小的。

**解题思路**

使用双指针，一个指针指向元素较小的值，一个指针指向元素较大的值。指向较小元素的指针从头向尾遍历，指向较大元素的指针从尾向头遍历。

- 如果两个指针指向元素的和 sum == target，那么这两个元素即为所求。
- 如果 sum > target，移动较大的元素，使 sum 变小一些；
- 如果 sum < target，移动较小的元素，使 sum 变大一些。

```java
public ArrayList<Integer> FindNumbersWithSum(int[] nums, int target) {
    int i = 0, j = nums.length - 1;
    while (i < j) {
        int cur = nums[i] + array[j];
        if (cur == target)
            return new ArrayList<>(Arrays.asList(nums[i], nums[j]));
        if (cur < target)
            i++;
        else
            j--;
    }
    return new ArrayList<>();
}
```

### [和为 S 的连续正数序列](https://cyc2018.xyz/算法/剑指 Offer 题解/57.2 和为 S 的连续正数序列.html)

**题目描述**

输出所有和为 S 的连续正数序列。例如和为 100 的连续序列有：

```text
[9, 10, 11, 12, 13, 14, 15, 16]
[18, 19, 20, 21, 22]。
```

**解题思路**

```java
public ArrayList<ArrayList<Integer> > FindContinuousSequence(int sum) {
        ArrayList<ArrayList<Integer>> ret = new ArrayList<>();
        int start = 1, end = 2;
        int curSum = 3;
        while (end < sum) {
            if(curSum > sum){
                curSum -= start;
                start++;
            }else if(curSum < sum){
                end++;
                curSum += end;
            }else {
                ArrayList<Integer> list = new ArrayList<>();
                for (int i = start; i <= end; i++) {
                    list.add(i);
                }
                ret.add(list);
                curSum -= start;
                start++;
                end++;
                curSum += end;
            }
        }
        return  ret;
    }
```



### [翻转单词顺序列](https://cyc2018.xyz/算法/剑指 Offer 题解/58.1 翻转单词顺序列.html)

**题目描述**

```html
Input:
"I am a student."

Output:
"student. a am I"
```

**解题思路**

先翻转每个单词，再翻转整个字符串。

题目应该有一个隐含条件，就是不能用额外的空间。虽然 Java 的题目输入参数为 String 类型，需要先创建一个字符数组使得空间复杂度为 O(N)，但是正确的参数类型应该和原书一样，为字符数组，并且只能使用该字符数组的空间。任何使用了额外空间的解法在面试时都会大打折扣，包括递归解法。

```java
public String ReverseSentence(String str) {
    int n = str.length();
    char[] chars = str.toCharArray();
    int i=0, j=0;
    while (j <= n){
        if(j==n || chars[j] == ' '){
            reverse(chars, i, j-1);
            i = j+1;
        }
        j++;
    }
    reverse(chars, 0, n-1);
    return new String(chars);
}

private void reverse(char[] chars, int i, int j) {
    while (i < j){
        swap(chars, i++, j--);
    }
}

private void swap(char[] chars, int i, int j) {
    char t = chars[i];
    chars[i] = chars[j];
    chars[j] = t;
}
```



### [左旋转字符串](https://cyc2018.xyz/算法/剑指 Offer 题解/58.2 左旋转字符串.html)

**题目描述**

将字符串 S 从第 K 位置分隔成两个子字符串，并交换这两个子字符串的位置。

```html
Input:
S="abcXYZdef"
K=3

Output:
"XYZdefabc"
```

**解题思路**

先将 "abc" 和 "XYZdef" 分别翻转，得到 "cbafedZYX"，然后再把整个字符串翻转得到 "XYZdefabc"。

```java
public String LeftRotateString(String str,int n) {
        if(n >= str.length())
            return str;
        char[] chars = str.toCharArray();
        reverse(chars, 0, n-1);
        reverse(chars, n, chars.length-1);
        reverse(chars, 0, chars.length-1);
        return new String(chars);
    }

    public void reverse(char[] chars, int i, int j) {
        while (i<j)
            swap(chars, i++, j--);
    }

    public void swap(char[] chars, int i, int j) {
        char t = chars[i];
        chars[i] = chars[j];
        chars[j] = t;
    }
}
```

## 链表

### [从尾到头打印链表](https://cyc2018.xyz/算法/剑指 Offer 题解/6. 从尾到头打印链表.html)

**题目描述**

从尾到头反过来打印出每个结点的值。

![img](https://cs-notes-1256109796.cos.ap-guangzhou.myqcloud.com/f5792051-d9b2-4ca4-a234-a4a2de3d5a57.png)

**解题思路**

1.使用递归

要逆序打印链表 1->2->3（3,2,1)，可以先逆序打印链表 2->3(3,2)，最后再打印第一个节点 1。而链表 2->3 可以看成一个新的链表，要逆序打印该链表可以继续使用求解函数，也就是在求解函数中调用自己，这就是递归函数。

```java
public ArrayList<Integer> printListFromTailToHead(ListNode listNode) {
    ArrayList<Integer> ret = new ArrayList<>();
    if (listNode != null) {
        ret.addAll(printListFromTailToHead(listNode.next));
        ret.add(listNode.val);
    }
    return ret;
}

```

2.使用头插法

头插法顾名思义是将节点插入到头部：在遍历原始链表时，将当前节点插入新链表的头部，使其成为第一个节点。

链表的操作需要维护后继关系，例如在某个节点 node1 之后插入一个节点 node2，我们可以通过修改后继关系来实现：

```java
node3 = node1.next;
node2.next = node3;
node1.next = node2;
```

![img](https://cs-notes-1256109796.cos.ap-guangzhou.myqcloud.com/58c8e370-3bec-4c2b-bf17-c8d34345dd17.gif)

为了能将一个节点插入头部，我们引入了一个叫头结点的辅助节点，该节点不存储值，只是为了方便进行插入操作。不要将头结点与第一个节点混起来，第一个节点是链表中第一个真正存储值的节点。

![img](https://cs-notes-1256109796.cos.ap-guangzhou.myqcloud.com/0dae7e93-cfd1-4bd3-97e8-325b032b716f-1572687622947.gif)

```java
public ArrayList<Integer> printListFromTailToHead(ListNode listNode) {
    // 头插法构建逆序链表
    ListNode head = new ListNode(-1);
    while (listNode != null) {
        ListNode memo = listNode.next;
        listNode.next = head.next;
        head.next = listNode;
        listNode = memo;
    }
    // 构建 ArrayList
    ArrayList<Integer> ret = new ArrayList<>();
    head = head.next;
    while (head != null) {
        ret.add(head.val);
        head = head.next;
    }
    return ret;
}
```

3.使用栈

栈具有后进先出的特点，在遍历链表时将值按顺序放入栈中，最后出栈的顺序即为逆序。![img](https://cs-notes-1256109796.cos.ap-guangzhou.myqcloud.com/9d1deeba-4ae1-41dc-98f4-47d85b9831bc.gif)

```java
public ArrayList<Integer> printListFromTailToHead(ListNode listNode) {
    Stack<Integer> stack = new Stack<>();
    while (listNode != null) {
        stack.add(listNode.val);
        listNode = listNode.next;
    }
    ArrayList<Integer> ret = new ArrayList<>();
    while (!stack.isEmpty())
        ret.add(stack.pop());
    return ret;
}
```

[在 O(1) 时间内删除链表节点](https://cyc2018.xyz/算法/剑指 Offer 题解/18.1 在 O(1) 时间内删除链表节点.html)

**题目描述**

**解题思路**

### [删除链表中重复的结点](https://cyc2018.xyz/算法/剑指 Offer 题解/18.2 删除链表中重复的结点.html)

**题目描述**

在一个排序的链表中，存在重复的结点，请删除该链表中重复的结点，重复的结点不保留，返回链表头指针。 例如，链表1->2->3->3->4->4->5 处理后为 1->2->5

**解题思路**

1.递归解法

```java
public ListNode deleteDuplication(ListNode pHead) {
    if (pHead == null || pHead.next == null)
        return pHead;
    ListNode next = pHead.next;
    if (pHead.val == next.val) {
        while (next != null && pHead.val == next.val)
            next = next.next;
        return deleteDuplication(next);
    } else {
        pHead.next = deleteDuplication(pHead.next);
        return pHead;
    }
}
```

2.迭代解法

```java
public ListNode deleteDuplication(ListNode pHead) {
        ListNode dummy = new ListNode(-1);
        ListNode tail = dummy;
        while (pHead != null) {
            // 进入循环时，确保了 pHead 不会与上一节点相同
            if (pHead.next == null || pHead.next.val != pHead.val) {
                tail.next = pHead;
                tail = pHead;
            }
            // 如果 pHead 与下一节点相同，跳过相同节点（到达「连续相同一段」的最后一位）
            while (pHead.next != null && pHead.val == pHead.next.val)
                pHead = pHead.next;
            pHead = pHead.next;
        }
        tail.next = null;
        return dummy.next;
    }
```

### [链表中倒数第 K 个结点](https://cyc2018.xyz/算法/剑指 Offer 题解/22. 链表中倒数第 K 个结点.html)

**题目描述**

**解题思路**

设链表的长度为 N。设置两个指针 P1 和 P2，先让 P1 移动 K 个节点，则还有 N - K 个节点可以移动。此时让 P1 和 P2 同时移动，可以知道当 P1 移动到链表结尾时，P2 移动到第 N - K 个节点处，该位置就是倒数第 K 个节点。

![img](https://cs-notes-1256109796.cos.ap-guangzhou.myqcloud.com/6b504f1f-bf76-4aab-a146-a9c7a58c2029.png)

```java
public ListNode FindKthToTail(ListNode head, int k) {
    if (head == null)
        return null;
    ListNode P1 = head;
    while (P1 != null && k-- > 0)
        P1 = P1.next;
    if (k > 0)
        return null;
    ListNode P2 = head;
    while (P1 != null) {
        P1 = P1.next;
        P2 = P2.next;
    }
    return P2;
}
```

### [链表中环的入口结点(待补)](https://cyc2018.xyz/算法/剑指 Offer 题解/23. 链表中环的入口结点.html)

**题目描述**

一个链表中包含环，请找出该链表的环的入口结点。要求不能使用额外的空间。

**解题思路**

### [反转链表](https://cyc2018.xyz/算法/剑指 Offer 题解/24. 反转链表.html)

**题目描述**

**解题思路**

1.正规解法

```java
public ListNode ReverseList(ListNode head) {
        if(head == null){
            return null;
        }
        ListNode pre = null;
        ListNode next = null;
        while(head != null){
            next = head.next;
            head.next = pre;
            pre = head;
            head = next;
        }
        return pre;
    }
}
```

2.迭代（头插法）

```java
public ListNode ReverseList(ListNode head) {
    ListNode newList = new ListNode(-1);
    while (head != null) {
        ListNode next = head.next;
        head.next = newList.next;
        newList.next = head;
        head = next;
    }
    return newList.next;
}
```

### [合并两个排序的链表](https://cyc2018.xyz/算法/剑指 Offer 题解/25. 合并两个排序的链表.html)

**题目描述**

![img](https://cs-notes-1256109796.cos.ap-guangzhou.myqcloud.com/c094d2bc-ec75-444b-af77-d369dfb6b3b4.png)

**解题思路**

1.迭代

```java
public ListNode Merge(ListNode list1, ListNode list2) {
    ListNode head = new ListNode(-1);
    ListNode cur = head;
    while (list1 != null && list2 != null) {
        if (list1.val <= list2.val) {
            cur.next = list1;
            list1 = list1.next;
        } else {
            cur.next = list2;
            list2 = list2.next;
        }
        cur = cur.next;
    }
    if (list1 != null)
        cur.next = list1;
    if (list2 != null)
        cur.next = list2;
    return head.next;
}
```

2.递归

```java
public ListNode Merge(ListNode list1, ListNode list2) {
    if (list1 == null)
        return list2;
    if (list2 == null)
        return list1;
    if (list1.val <= list2.val) {
        list1.next = Merge(list1.next, list2);
        return list1;
    } else {
        list2.next = Merge(list1, list2.next);
        return list2;
    }
}
```

### [复杂链表的复制（待补）](https://cyc2018.xyz/算法/剑指 Offer 题解/35. 复杂链表的复制.html)

**题目描述**

**解题思路**

### [两个链表的第一个公共结点](https://cyc2018.xyz/算法/剑指 Offer 题解/52. 两个链表的第一个公共结点.html)

**题目描述**

![img](https://cs-notes-1256109796.cos.ap-guangzhou.myqcloud.com/5f1cb999-cb9a-4f6c-a0af-d90377295ab8.png)

**解题思路**

设 A 的长度为 a + c，B 的长度为 b + c，其中 c 为尾部公共部分长度，可知 a + c + b = b + c + a。

当访问链表 A 的指针访问到链表尾部时，令它从链表 B 的头部重新开始访问链表 B；同样地，当访问链表 B 的指针访问到链表尾部时，令它从链表 A 的头部重新开始访问链表 A。这样就能控制访问 A 和 B 两个链表的指针能同时访问到交点。

链表的指针能同时访问到交点。

```java
public ListNode FindFirstCommonNode(ListNode pHead1, ListNode pHead2) {
    ListNode l1 = pHead1, l2 = pHead2;
    while (l1 != l2) {
        l1 = (l1 == null) ? pHead2 : l1.next;
        l2 = (l2 == null) ? pHead1 : l2.next;
    }
    return l1;
}
```

## 树

### [重建二叉树](https://cyc2018.xyz/算法/剑指 Offer 题解/7. 重建二叉树.html)

**题目描述**

**解题思路**

### [二叉树的下一个结点](https://cyc2018.xyz/算法/剑指 Offer 题解/8. 二叉树的下一个结点.html)

**题目描述**

**解题思路**

### [树的子结构](https://cyc2018.xyz/算法/剑指 Offer 题解/26. 树的子结构.html)

**题目描述**

**解题思路**

### [二叉树的镜像](https://cyc2018.xyz/算法/剑指 Offer 题解/27. 二叉树的镜像.html)

**题目描述**

**解题思路**

### [对称的二叉树](https://cyc2018.xyz/算法/剑指 Offer 题解/28. 对称的二叉树.html)

**题目描述**

**解题思路**

### [从上往下打印二叉树](https://cyc2018.xyz/算法/剑指 Offer 题解/32.1 从上往下打印二叉树.html)

**题目描述**

**解题思路**

### [把二叉树打印成多行](https://cyc2018.xyz/算法/剑指 Offer 题解/32.2 把二叉树打印成多行.html)

**题目描述**

**解题思路**

### [按之字形顺序打印二叉树](https://cyc2018.xyz/算法/剑指 Offer 题解/32.3 按之字形顺序打印二叉树.html)

**题目描述**

**解题思路**

### [二叉搜索树的后序遍历序列](https://cyc2018.xyz/算法/剑指 Offer 题解/33. 二叉搜索树的后序遍历序列.html)

**题目描述**

**解题思路**

### [二叉树中和为某一值的路径](https://cyc2018.xyz/算法/剑指 Offer 题解/34. 二叉树中和为某一值的路径.html)

**题目描述**

**解题思路**

### [二叉搜索树与双向链表](https://cyc2018.xyz/算法/剑指 Offer 题解/36. 二叉搜索树与双向链表.html)

**题目描述**

**解题思路**

### [序列化二叉树](https://cyc2018.xyz/算法/剑指 Offer 题解/37. 序列化二叉树.html)

**题目描述**

**解题思路**

### [二叉查找树的第 K 个结点](https://cyc2018.xyz/算法/剑指 Offer 题解/54. 二叉查找树的第 K 个结点.html)

**题目描述**

**解题思路**

### [二叉树的深度](https://cyc2018.xyz/算法/剑指 Offer 题解/55.1 二叉树的深度.html)

**题目描述**

**解题思路**

### [平衡二叉树](https://cyc2018.xyz/算法/剑指 Offer 题解/55.2 平衡二叉树.html)

**题目描述**

**解题思路**

### [树中两个节点的最低公共祖先](https://cyc2018.xyz/算法/剑指 Offer 题解/68. 树中两个节点的最低公共祖先.html)

**题目描述**

**解题思路**

## 

