# 1、简介

## 1.1、什么是MyBatis

* MyBatis 是一款优秀的**持久层框架**

* 它支持自定义 SQL、存储过程以及高级映射

* MyBatis 免除了几乎所有的 JDBC 代码以及设置参数和获取结果集的工作

* MyBatis 可以通过简单的 XML 或注解来配置和映射原始类型、接口和 Java POJO（Plain Old Java Objects，普通老式 Java 对象）为数据库中的记录
* MyBatis 本是apache的一个[开源项目](https://baike.baidu.com/item/开源项目/3406069)iBatis, 2010年这个[项目](https://baike.baidu.com/item/项目/477803)由apache software foundation 迁移到了[google code](https://baike.baidu.com/item/google code/2346604)，并且改名为MyBatis 。
* 2013年11月迁移到[Github](https://baike.baidu.com/item/Github/10145341)。

## 1.2、持久化

数据持久化

* 持久化就是将程序的数据在持久状态和瞬时状态转化的过程
* 内存：**断电即失**
* 数据库（jdbc），io文件持久化

为什么要持久化

* 有一些对象不能让他丢失
* 内存太贵

## 1.3、持久层

Dao层，Service层，Controller层....

* 完成持久化工作的代码块
* 层界限十分明显

# 2、第一个MyBatis程序

思路：搭建环境--->导入MyBatis--->编写代码--->测试

## 2.1、搭建环境

搭建数据库

```sql
create database `hang`;
use hang;

CREATE TABLE `user`(
	`id` INT(20) NOT NULL PRIMARY KEY,
	`name` VARCHAR(30) DEFAULT NULL,
	`pwd` VARCHAR(30) DEFAULT NULL
)ENGINE=INNODB DEFAULT CHARSET=utf8;

INSERT INTO `user` (`id`,`name`,`pwd`) VALUES
(1,'琉璃',123456),
(2,'张三',654321),
(3,'李四',456123)
```

新建项目

​	1.新建一个普通的maven项目

​	2.删除src目录

​	3.导入maven依赖	

~~~xml
    ```xml
    <!--导入依赖-->
        <dependencies>
            <!--mysql驱动-->
            <!-- https://mvnrepository.com/artifact/mysql/mysql-connector-java -->
            <dependency>
                <groupId>mysql</groupId>
                <artifactId>mysql-connector-java</artifactId>
                <version>8.0.25</version>
            </dependency>
            <!--mybaits-->
            <!-- https://mvnrepository.com/artifact/org.mybatis/mybatis -->
            <dependency>
                <groupId>org.mybatis</groupId>
                <artifactId>mybatis</artifactId>
                <version>3.5.7</version>
            </dependency>
            <!--junit-->
            <!-- https://mvnrepository.com/artifact/junit/junit -->
            <dependency>
                <groupId>junit</groupId>
                <artifactId>junit</artifactId>
                <version>4.12</version>
                <scope>test</scope>
            </dependency>
    
        </dependencies>
    ```
~~~

# 3、CURD

## 1、namespace

namespace中的包名要和Dao/mapper接口一致

## 2、select

选择，查询语句

* id：对应的namespace中的方法名
* resultType：SQL语句执行的返回值
* parameterType：参数类型



​	1.编写接口

```java
// 根据id查询用户
User getUserById(int id);
```

​	2.编写对应的mapper中的sql语句

```xml
<select id="getUserById" parameterType="int" resultType="com.hang.pojo.User">
    select * from hang.user where id = #{id}
</select>
```

​	3.测试

```java
@Test
public void getUserById(){
    SqlSession sqlSession = MybatisUtils.getSqlSession();
    UserMapper mapper = sqlSession.getMapper(UserMapper.class);
    User user = mapper.getUserById(1);
    System.out.println(user);
    sqlSession.close();
}
```

## 3、Insert

```xml
<insert id="addUser" parameterType="com.hang.pojo.User">
    insert into user(id, name, pwd) value (#{id}, #{name}, #{pwd})
</insert>
```

## 4、update

```xml
<update id="updateUser" parameterType="com.hang.pojo.User">
    update user set name = #{name}, pwd = #{pwd} where id = #{id}
</update>
```

## 5、delete

```xml
<delete id="deleteUser" parameterType="int">
    delete from user where id = #{id}
</delete>
```

注意：

* 增删改需要提交事务

## 6、错误分析

* 标签不要匹配错
* resource绑定mapper，需要使用路径
* 程序配置文件必须符合规范
* NullPointerException，没有注册到资源
* 输出的xml文件中存在中文乱码问题
* maven资源没有导出问题

## 7、模糊查询

1.Java代码执行的时候，传递通配符%%

```java
List<User> userList = mapper.getUserLike("%李%");
```

2.在sql拼接中使用通配符

```xml
select * from user where name like "%"#{name}"%"
```

# 4、配置解析

## 1、核心配置文件

* mabatis-config.xml

* MyBatis的配置文件包含了影响MyBatis行为的设置和属性信息

  ```xml
  configuration（配置）
  properties（属性）
  settings（设置）
  typeAliases（类型别名）
  typeHandlers（类型处理器）
  objectFactory（对象工厂）
  plugins（插件）
  environments（环境配置）
  environment（环境变量）
  transactionManager（事务管理器）
  dataSource（数据源）
  databaseIdProvider（数据库厂商标识）
  mappers（映射器）
  ```

## 2、环境配置（environments）

MyBatis 可以配置成适应多种环境

**不过要记住：尽管可以配置多个环境，但每个 SqlSessionFactory 实例只能选择一种环境。**

MyBatis默认的事务管理器是JDBC，连接池：POOLED

## 3、属性（properties）

我们可以通过properties属性来实现引用配置文件

这些属性可以在外部进行配置，并可以进行动态替换。你既可以在典型的 Java 属性文件中配置这些属性，也可以在 properties 元素的子元素中设置。【db.properties】

编写一个配置文件

db.properties

```properties
driver = com.mysql.cj.jdbc.Driver
url = jdbc:mysql://localhost:3306/hang?useSSL=true&amp;useUnicode=true&amp;characterEncoding=UTF-8
username = root
password = 0123
```

在核心配置文件中引入

```properties
    <!--引入外部配置文件-->
    <properties resource="db.properties">
        <property name="username" value="root"/>
        <property name="password" value="0123"/>
    </properties>
```

* 可以直接引入外部文件
* 可以在其中增加一些属性配置
* 如果有两个文件有同一个字段，优先使用外部配置文件

## 4、类型别名（typeAliases）

* 类型别名可为 Java 类型设置一个缩写名字

* 意在降低冗余的全限定类名书写

  ```xml
      <!--可以给实体类起别名-->
      <typeAliases>
          <typeAlias type="com.hang.pojo.User" alias="User"/>
      </typeAliases>
  ```

  也可以指定一个包名，MyBatis 会在包名下面搜索需要的 Java Bean，比如：

  扫描实体类的包，它的默认名就为这个类的 类名，首字母小写

  ```xml
      <!--可以给实体类起别名-->
      <typeAliases>
          <package name="com.hang.pojo"/>
      </typeAliases>
  ```

  在实体类比较少的时候使用第一种
  
  在实体类比较多的时候使用第二种
  
  第一种可以DIY别名，第二种则不行，如果非要改可以在实体类上加注解
  
  ```java
  @Alias("user")
  public class User{}
  ```

## 5、设置（settings）

这是 MyBatis 中极为重要的调整设置，它们会改变 MyBatis 的运行时行为

![image-20210721115721578](https://edu-hanghang.oss-cn-beijing.aliyuncs.com/MyBatis/image-20210721115721578.png)

![image-20210721115646452](https://edu-hanghang.oss-cn-beijing.aliyuncs.com/MyBatis/image-20210721115646452.png)

## 6、生命周期

不同作用域和生命周期类别是至关重要的，因为错误的使用会导致非常严重的并发问题

**SqlSessionFactoryBuilder**

* 一旦创建了 SqlSessionFactory，就不再需要它了
* 局部变量

**SqlSessionFactory**

* 说白了就是可以想象为：数据库连接池
* SqlSessionFactory一旦被创建就应该在应用的运行期间一直存在，**没有任何理由丢弃它或重新创建另一个实例**
*  SqlSessionFactory 的最佳作用域是应用作用域
* 最简单的就是使用单例模式或者静态单例模式

**SqlSession**

* 连接到连接池的一个请求
* SqlSession 的实例不是线程安全的，因此是不能被共享的，所以它的最佳的作用域是请求或方法作用域
* 用完之后需要赶紧关闭，否则占用资源

# 5、解决属性名和字段名不一致的问题

数据库表中的字段

![image-20210721121625666](https://edu-hanghang.oss-cn-beijing.aliyuncs.com/MyBatis/image-20210721121625666.png)

新建一个项目，拷贝之前的，测试实体类字段不一致问题

![image-20210721122918001](https://edu-hanghang.oss-cn-beijing.aliyuncs.com/MyBatis/image-20210721122918001.png)

测试出现问题

![image-20210721122937969](https://edu-hanghang.oss-cn-beijing.aliyuncs.com/MyBatis/image-20210721122937969.png)

解决方案

* 起别名

* resultMap

  ```xml
  id name pwd
  id name password
  ```

  ```xml
  <!--结果集映射-->
  <resultMap id="UserMap" type="user">
      <!--column数据库中的字段，property实体类中的属性-->
      <id column="id" property="id"/>
      <result column="name" property="name"/>
      <result column="pwd" property="password"/>
  </resultMap>
  
  <select id="getUserById" parameterType="int" resultMap="UserMap">
      select * from hang.user where id = #{id}
  </select>
  ```

  * resultMap 元素是 MyBatis 中最重要最强大的元素
  * ResultMap 的设计思想是，对简单的语句做到零配置，对于复杂一点的语句，只需要描述语句之间的关系就行了

# 6、日志

## 6.1、日志工厂

如果一个数据库操作出现异常，我们需要排错，日志就是最好的助手

曾今：sout、debug

![image-20210721145850260](https://edu-hanghang.oss-cn-beijing.aliyuncs.com/MyBatis/image-20210721145850260.png)

-  SLF4J
-  LOG4J
- LOG4J2
- JDK_LOGGING
- COMMONS_LOGGING
- STDOUT_LOGGING
- NO_LOGGING

在MyBatis中具体使用那个日志实现，在设置中设定

**STDOUT_LOGGING标准日志输出**

```xml
<settings>
        <setting name="logImpl" value="STDOUT_LOGGING"/>
</settings>
```

![image-20210721154026861](https://edu-hanghang.oss-cn-beijing.aliyuncs.com/MyBatis/image-20210721154026861.png)

## 6.2、Log4j

什么是Log4j

* 可以控制日志信息输送的目的地是[控制台](https://baike.baidu.com/item/控制台/2438626)、文件、[GUI](https://baike.baidu.com/item/GUI)组件
* 控制每一条日志的输出格式
* 通过定义每一条日志信息的级别，我们能够更加细致地控制日志的生成过程
* 通过定义每一条日志信息的级别，我们能够更加细致地控制日志的生成过程

1.先导入log4j的包

```xml
<!-- https://mvnrepository.com/artifact/log4j/log4j -->
<dependency>
    <groupId>log4j</groupId>
    <artifactId>log4j</artifactId>
    <version>1.2.17</version>
</dependency>
```

2.log4j.properties

```properties
#将等级为DEBUG的日志信息输出到console和file这两个目的地，console和file的定义在下面的代码
log4j.rootLogger=DEBUG,console,file
#控制台输出的相关设置
log4j.appender.console = org.apache.log4j.ConsoleAppender
log4j.appender.console.Target = System.out
log4j.appender.console.Threshold=DEBUG
log4j.appender.console.layout = org.apache.log4j.PatternLayout
log4j.appender.console.layout.ConversionPattern=[%c]-%m%n
#文件输出的相关设置
log4j.appender.file = org.apache.log4j.RollingFileAppender
log4j.appender.file.File=./log/kexing.log
log4j.appender.file.MaxFileSize=10mb
log4j.appender.file.Threshold=DEBUG
log4j.appender.file.layout=org.apache.log4j.PatternLayout
log4j.appender.file.layout.ConversionPattern=[%p][%d{yy-MM-dd}][%c]%m%n
#日志输出级别
log4j.logger.org.mybatis=DEBUG
log4j.logger.java.sql=DEBUG
log4j.logger.java.sql.Statement=DEBUG
log4j.logger.java.sql.ResultSet=DEBUG
log4j.logger.java.sql.PreparedStatement=DEBUG
```

3.配置log4j为日志的实现

```xml
<settings>
    <setting name="logImpl" value="LOG4J"/>
</settings>
```

4.Log4j的使用，直接运行测试

![image-20210721155647161](https://edu-hanghang.oss-cn-beijing.aliyuncs.com/MyBatis/image-20210721155647161.png)

**简单使用**

​	1.在使用Log4j的类中，导入包 import org.apache.log4j.Logger;

​	2.日志对象，参数为当前类的class

```java
static Logger logger = Logger.getLogger(UserMapperTest.class);
```

​	3.日志级别

```java
logger.info("info:进入了Log4jTest");
logger.debug("debug:进入了Log4jTest");
logger.error("error:进入了Log4jTest");
```

# 7、分页

**为什么分页**

* 减少数据的处理量

## 7.1、**使用Limt分页**

使用MyBatis实现分页，核心SQL

1.接口

```java
// 分页
List<User> getUserByLimit(Map<String, Integer> map);
```

2.Mapper.XML

```xml
<select id="getUserByLimit" parameterType="map" resultMap="UserMap">
    select * from user limit #{startIndex},#{pageSize}
</select>
```

3.测试

```java
public void getUserByLimit(){
    SqlSession sqlSession = MybatisUtils.getSqlSession();
    UserMapper mapper = sqlSession.getMapper(UserMapper.class);
    HashMap<String, Integer> map = new HashMap<>();
    map.put("startIndex", 0);
    map.put("pageSize", 2);
    List<User> userList = mapper.getUserByLimit(map);
    for (User user: userList) {
        System.out.println(user);
    }
    sqlSession.close();
}
```

## 7.2、RowBounds分页

不再使用SQL进行分页

1.接口

```java
// 分页2
List<User> getUserByRowBounds();
```

2.mapper.xml

```xml
<select id="getUserByRowBounds" resultMap="UserMap">
    select * from user
</select>
```

3.测试

```java
@Test
public void getUserByRowBounds(){
    SqlSession sqlSession = MybatisUtils.getSqlSession();
    // RowBounds实现
    RowBounds rowBounds = new RowBounds(1, 2);
    List<User> list = sqlSession.selectList("com.hang.dao.UserMapper.getUserByRowBounds", null, rowBounds);
    for (User user : list) {
        System.out.println(user);
    }

    sqlSession.close();
}
```

## 7.3、分页插件

![image-20210721164453545](https://edu-hanghang.oss-cn-beijing.aliyuncs.com/MyBatis/image-20210721164453545.png)

# 8、注解开发

## 8.1、使用注解开发

1.注解在接口上实现

```java
@Select("select * from user")
List<User> getUsers();
```

2.需要在核心配置文件中绑定接口

```xml
<mappers>
    <mapper class="com.hang.dao.UserMapper"/>
</mappers>
```

3.测试



本质：反射机制实现

底层：动态代理

## 8.2、CRUD

我们可以在工具类创建的时候实现自动提交事务

```java
public static SqlSession getSqlSession(){
        return sqlSessionFactory.openSession(true);
}
```

关于@param()注解

* 基本类型的参数或者String类型需要加上
* 引用类型不需要加
* 如果只有一个基本类型的话，可以忽略，但是建议加上
* 在我们sql中引用的就是@param("uid")中设定的属性名

# 9、Lombok

使用步骤：

1. 在IDEA中安装Lombok插件

2. 在项目中导入Lombok的jar包

   ```xml
   <dependency>
       <groupId>org.projectlombok</groupId>
       <artifactId>lombok</artifactId>
       <version>1.18.20</version>
   </dependency>
   ```

   

3. 在实体类上加注解

   ```java
   @Data
   @AllArgsConstructor
   @NoArgsConstructor
   public class User {
       private int id;
       private String name;
       private String password;
   
   }
   ```

   

   ```xml
   @Getter and @Setter
   @FieldNameConstants
   @ToString
   @EqualsAndHashCode
   @AllArgsConstructor, @RequiredArgsConstructor and @NoArgsConstructor
   @Log, @Log4j, @Log4j2, @Slf4j, @XSlf4j, @CommonsLog, @JBossLog, @Flogger, @CustomLog
   @Data
   @Builder
   @SuperBuilder
   @Singular
   @Delegate
   @Value
   @Accessors
   @Wither
   @With
   @SneakyThrows
   @val
   @var
   experimental @var
   @UtilityClass
   ```

   说明

   ```xml
   @Data: 无参构造，get，set，tostring，hashcode，equals
   @AllArgsConstructor
   @NoArgsConstructor
   @EqualsAndHashCode
   @ToString
   @Getter
   ```

# 10、多对一处理

多对一：

- 多个学生对应一个老师
- 对于老师而言，**集合**，一个老师，有很多学生【一对多】
- 对于学生而言，**关联**，多个学生关联一个老师【多对一】

## **测试环境搭建**

1. 导入lombok
2. 新建实体类
3. 建立Mapper接口
4. 建立Mapper.xml文件
5. 在核心配置文件中绑定注册我们的Mapper接口或者文件
6. 测试查询是否成功

## 按照查询嵌套处理

```xml
<!--
        思路：
            1.查询所有学生信息
            2.根据查询出来的学生的tid，寻找对应的老师     子查询
-->
<select id="getStudent" resultMap="StudentTeacher">
    select * from student
</select>
<resultMap id="StudentTeacher" type="Student">
    <!--复杂的属性，我们需要单独处理
            对象： association
            集合：collection
        -->
    <association property="teacher" column="tid" javaType="Teacher" select="getTeacher"/>
</resultMap>
<select id="getTeacher" resultType="Teacher">
    select * from teacher where id = #{id}
</select>
```

## 按照结果嵌套查询

```xml
<!--按照结果嵌套处理-->
<select id="getStudent2" resultMap="StudentTeacher2">
    select s.id sid,s.name sname,t.name tname
    from student s,teacher t
    where s.id=t.id
</select>

<resultMap id="StudentTeacher2" type="Student">
    <association property="teacher" javaType="Teacher">
        <result property="name" column="tname"/>
    </association>
</resultMap>
```

# 11、一对多处理

1. 搭建环境，和刚才一样

   实体类

   ```java
   @Data
   public class Student {
       private int id;
       private String name;
       private int tid;
   }
   ```

   ```java
   @Data
   public class Teacher {
       private int id;
       private String name;
       // 一个老师拥有很多学生
       private List<Student> students;
   }
   ```

## 按照结果嵌套

```xml
<!--按照结果嵌套查询-->
<select id="getTeacher" resultMap="TeacherStudent">
    select s.id sid, s.`name` sname, t.`name` tname, t.id tid
    from student s, teacher t
    where s.tid = t.id and t.id = #{tid}
</select>
<resultMap id="TeacherStudent" type="teacher">
    <result property="id" column="tid"/>
    <result property="name" column="tname"/>
    <!--复杂的属性，我们需要单独处理
            对象： association
            集合：collection
            javaType=""指定属性的集合
            集合中的泛型信息，我们使用ofType获取
        -->
    <collection property="students" ofType="student">
        <result property="id" column="sid"/>
        <result property="name" column="sname"/>
        <result property="tid" column="tid"/>
    </collection>
</resultMap>
```

## 按照查询嵌套

```xml
<select id="getTeacher2" resultMap="TeacherStudent2">
    select * from teacher where id = #{tid}
</select>

    <resultMap id="TeacherStudent2" type="teacher">
    <collection property="students" javaType="ArrayList"  ofType="student" select="getStudentByTeacherId" column="id"/>
    </resultMap>

    <select id="getStudentByTeacherId" resultType="Student">
    select * from student where tid = #{tid}
</select>
```

## 小结

1. 关联 - association 【多对一】
2. 集合 - collection 【一对多】
3. javaType    &    ofType
   1. javaType 用来指定实体类中属性的类型
   2. ofType 用来指定映射到List或者集合中的pojo类型，泛型中的约束类型

注意点：

- 保证SQL的可读性，尽量保证通俗易懂
- 注意一对多和多对一中，属性名和字段的问题
- 如果错误不好排查，可以使用日志，建议使用log4j

# 12、动态SQL

**什么是动态SQL：动态SQL就是通过不同的条件生成不同的SQL语句**

## 搭建环境

```sql
CREATE TABLE blog(
	id varchar(50) not null comment '博客id',
	title varchar(100) not null comment '博客标题',
	author varchar(30) not null comment '博客作者',
	create_time datetime not null comment '创建时间',
	views int(30) not null comment '浏览量'
)ENGINE=INNODB DEFAULT charset=utf8
```

创建一个基本工程：

1. 导包

2. 编写配置文件

3. 编写实体类

   ```java
   @Data
   public class Blog {
       private int id;
       private String title;
       private String author;
       private Date createTime;
       private int views;
   }
   ```

4. 编写实体类对应Mapper接口以及Mapper.XML文件



## IF

```xml
<select id="queryBlogIF" parameterType="map" resultType="Blog">
        select * from blog where 1=1
        <if test="title != null">
            and title = #{title}
        </if>
        <if test="author != null">
            and author = #{author}
        </if>
</select>
```

## choose、when、otherwise

```xml
<select id="queryBlogChoose" parameterType="map" resultType="Blog">
        select * from blog
        <where>
            <choose>
                <when test="title != null">
                    title = #{title}
                </when>
                <when test="author != null">
                    and author = #{author}
                </when>
                <otherwise>
                    and views = #{views}
                </otherwise>
            </choose>
        </where>
</select>
```



## trim、where、set

```xml
<select id="queryBlogIF" parameterType="map" resultType="Blog">
        select * from blog
        <where>
            <if test="title != null">
                and title = #{title}
            </if>
            <if test="author != null">
                and author = #{author}
            </if>
        </where>
</select>
```

```xml
<update id="updateBlog" parameterType="map">
        update blog
        <set>
            <if test="title != null">
                title = #{title},
            </if>
            <if test="author != null">
                author = #{author}
            </if>
        </set>
        where id = #{id}
</update>
```

## SQL片段

1. 使用SQL标签抽取公共部分
2. 在需要使用的地方使用include标签引用即可

## Foreach

```xml
<!--
        select * from blog where 1=1 and (id=1 or id=2 or id=3)
-->
<select id="queryBlogForeach" parameterType="map" resultType="blog">
    select * from blog
    <where>
        <foreach collection="ids" item="id" open="and (" close=")" separator="or">
            id = #{id}
        </foreach>
    </where>
</select>
```

# 13、缓存

## 13.1、简介

1. 什么是缓存
   - 存在内存中的临时数据
   - 将用户经常查询的数据放在缓存（内存）中，用户去查询数据就不用从磁盘上(关系型数据库
     数据文件)查询，从缓存中查询，从而提高查询效率，解决了高并发系统的性能问题。
2. 为什么使用缓存
   - 减少和数据库的交互次数，减少系统开销，提高系统效率。
3. 什么样的数据能使用缓存？
   - 经常查询并且不经常改变的数据。

## 13.2、Mybatis缓存

- MyBatis包含一个非常强大的查询缓存特性，它可以非常方便的定制和配置缓存，缓存可以极大的提高查询效率。
- MyBatis系统中默认定义了两级缓存：一级缓存和二级缓存
  - 默认情况下，只有一级缓存开启（SqlSession级别的缓存，也称为本地缓存）
  - 二级缓存需要手动开启和配置，他是基于namespace级别的缓存。
  - 为了提高可扩展性，MyBatis定义了缓存接口Cache。我们可以通过实现Cache接口来定义二级缓存。

## 13.3、一级缓存

- 一级缓存也叫本地缓存：
  - 与数据库同一次会话期间查询到的数据会放在本地缓存汇总
  - 以后如果还需要获取相同数据，直接从缓存中取，没必要再去查询数据库

测试步骤：

- 开启日志

- 测试一个session中查询两次相同的记录

- 查看日志输出

  <img src="https://edu-hanghang.oss-cn-beijing.aliyuncs.com/MyBatis/e2aa9811-5f5e-4a5b-981f-d476b46c0fbd.png" alt="img" style="zoom:67%;" />

缓存失效情况

1.查询两个不同的时，sql就会创建两次

<img src="https://edu-hanghang.oss-cn-beijing.aliyuncs.com/MyBatis/390fe8f2-2e1e-4d1c-b2cf-2ff0dca61dad.png" alt="img" style="zoom:67%;" />

2.增删改操作可能会改变原来的数据，所以必定会刷新缓存

<img src="https://edu-hanghang.oss-cn-beijing.aliyuncs.com/MyBatis/f27a5621-841a-4956-9cb2-2aced762d428.png" alt="img" style="zoom:67%;" />

3.查询不同的mapper

4.手动清理缓存

<img src="https://edu-hanghang.oss-cn-beijing.aliyuncs.com/MyBatis/88688c59-c1ec-4f10-b4a3-d0fd44422b52.png" alt="img" style="zoom:67%;" />

小结：一级缓存默认是开启的，只在一次sqlsession中有效，也就是拿到连接到关闭连接的区间段

可以把它理解成一个map。

## 13.4、二级缓存

- 二级缓存也叫全局缓存，一级缓存的作用域太低了，所以诞生了二级缓存
- 基于namespace级别的缓存，一个名称空间，对应一个二级缓存
- 工作机制
  - 一个会话查询一条数据，这个数据就会被放在当前会话的一级缓存中
  - 如果当前会话关闭了，这个会话对应的一级缓存也就没了，但是我们想要的是，会话关闭了，一级缓存的数据被保存到二级缓存中
  - 新的会话查询信息，就可以冲二级缓存中获取内容
  - 不同的mapper查出的数据会自己放进对应的缓存（map）中

<img src="https://edu-hanghang.oss-cn-beijing.aliyuncs.com/MyBatis/cb4f49dd-0202-418e-8630-e34bd66ea0d2.png" alt="img" style="zoom:67%;" />

步骤：

1. 开启全局缓存

   ```xml
       <!--开启全局缓存-->
       <setting name="cacheEnabled" value="true"/>
   </setting>
   ```

   我没了，但是我东西还在，我可以遗传 ——一级缓存

2. 在要是用二级缓存的Mapper中开启

   ```xml
    <cache />
   ```

   也可以自定义一些参数

   ```xml
      <cache eviction="FIFO"
              flushInterval="60000"
              size="512"
              readOnly="true"/>
   ```

1. 测试

   1. 问题，我们需要实体类序列化，否则就会报错！

      ![img](https://edu-hanghang.oss-cn-beijing.aliyuncs.com/MyBatis/f8b0ddd4-4812-4edd-8c0d-b6a94c77cc73.png)

小结：

- 只要开启了二级缓存，在同一个Mapper下就有效
- 所有的数据都会先放在一级缓存中
- 只有当会话提交，或者关闭的时候，才会提交到二级缓存中

## 13.5、缓存原理

![img](https://edu-hanghang.oss-cn-beijing.aliyuncs.com/MyBatis/487da9af-cee4-40f9-bbc8-da886d2dbe83.png)