# Comparison method violates its general contract!

#### 错误代码复现

```java
            Collections.sort(auditLogs, new Comparator<AuditLog>() {
                @Override
                public int compare(AuditLog o1, AuditLog o2) {
                    int i = o1.getTime().compareTo(o2.getTime());
                    if (i>0){
                        return 1;
                    }else {
                        return -1;
                    }
                }
            });
```

#### 报错提示复现

```
java.lang.IllegalArgumentException: Comparison method violates its general contract!
	at java.util.TimSort.mergeHi(TimSort.java:899) ~[na:1.8.0_311]
	at java.util.TimSort.mergeAt(TimSort.java:516) ~[na:1.8.0_311]
	at java.util.TimSort.mergeForceCollapse(TimSort.java:457) ~[na:1.8.0_311]
	at java.util.TimSort.sort(TimSort.java:254) ~[na:1.8.0_311]
	at java.util.Arrays.sort(Arrays.java:1512) ~[na:1.8.0_311]
	at java.util.ArrayList.sort(ArrayList.java:1464) ~[na:1.8.0_311]
	at java.util.Collections.sort(Collections.java:177) ~[na:1.8.0_311]
```

#### 错误分析

1、根据报错信息提示，造成的原因是由于java.util.TimSort.mergeHi造成的，我们查看源码

```java
public static <T> void sort(T[] a, Comparator<? super T> c) {
    if (c == null) {
        sort(a);
    } else {
        if (LegacyMergeSort.userRequested)
            legacyMergeSort(a, c);
        else
            TimSort.sort(a, 0, a.length, c, null, 0, 0);
    }
}
```

根据源码我们可以发现走到路径是else这个分支

2、由于jdk7及其之后走的就是TimSort.sort这个分支，在这个方法中必须满足以下三点：

​	（1）自反性：如果 x>y ，那么必然 y<x

​	（2）传递性：如果 x>y , y>z,  那么x>z

​	（3）对称性：如果x=y，那么x与z的比较结果应该和y与z的比较结果一致

3、根据以上三点，可以分析出并没有满足相等情况下，故造成报错

#### 处理方案

1、让排序时走legacyMergeSort方案，这样就可以解决问题（不推荐）

```
// java启动时修改jvm
-Djava.util.Arrays.useLegacyMergeSort=true
```

2、更改代码（推荐）

```
// 代码中并没有确定相等的情况，只需要添加上就可以了，我们也可以用compareTo帮我我们直接比较
// 修改后的代码如下
Collections.sort(auditLogs, new Comparator<AuditLog>() {
    @Override
    public int compare(AuditLog o1, AuditLog o2) {
        return o1.getTime().compareTo(o2.getTime());
    }
});
```

#### 详细分析

按照正常情况，高版本的JDK应该是可以兼容之前的代码，但是JDK6与JDK7确实存在不兼容问题，在不兼容列表中我们可以找到关于Collections.sort的不兼容说明，如下：

```
Area: API: Utilities  
Synopsis: Updated sort behavior for Arrays and Collections may throw an IllegalArgumentException  
Description: The sorting algorithm used by java.util.Arrays.sort and (indirectly) by java.util.Collections.sort has been replaced.   
The new sort implementation may throw an IllegalArgumentException if it detects a Comparable that violates the Comparable contract.   
The previous implementation silently ignored such a situation.  
If the previous behavior is desired, you can use the new system property, java.util.Arrays.useLegacyMergeSort,   
to restore previous mergesort behavior.  
Nature of Incompatibility: behavioral  
RFE: 6804124  
```

描述的意思是说，java.util.Arrays.sort(java.util.Collections.sort调用的也是此方法)方法中的排序算法在JDK7中已经被替换了。如果违法了比较的约束新的排序算法也许会抛出llegalArgumentException异常。JDK6中的实现则忽略了这种情况。那么比较的约束是什么呢？[看这里](http://docs.oracle.com/javase/7/docs/api/java/util/Comparator.html#compare(T, T))，大体如下：
$$
sgn(x):=
\begin{cases}
-1 \qquad \,   if \ x<0, \\
0 \quad \qquad if \ x=0,\\
1 \quad \qquad  if \ x>0
\end{cases}
$$

- sgn(compare(x, y)) == -sgn(compare(y, x))
- ((compare(x, y)>0) && (compare(y, z)>0)) implies compare(x, z)>0
- compare(x, y)==0 implies that sgn(compare(x, z))==sgn(compare(y, z)) for all z

再回过头来看我们开篇有问题的实现：

```java
return x > y ? 1 : -1;
```

当x == y时，sgn(compare(x, y))  = -1，-sgn(compare(y, x)) = 1，这违背了sgn(compare(x, y)) == -sgn(compare(y, x))约束，所以在JDK7中抛出了本文标题的异常。

那么现在是否可以盖棺定论了，按照上面的分析来看，使用这种比较方式（return x > y ? 1 : -1;），只要集合或数组中有相同的元素，就会抛出本文标题的异常。实则不然，什么情况下抛出异常，还取决于JDK7底层排序算法的实现，也就是大名鼎鼎的[TimSort](http://svn.python.org/projects/python/trunk/Objects/listsort.txt)。后面文章会分析TimSort。本文给出一个会引发该异常的Case，以便有心人共同研究，如下：

```java
Integer[] array =   
{0, 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,   
0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 2, 1, 0, 0, 0, 2, 30, 0, 3};  
```



本文参考：

https://www.cnblogs.com/firstdream/p/7204067.html

https://blog.csdn.net/samur2/article/details/109231250